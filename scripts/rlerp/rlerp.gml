var dir = argument0;
var targetdir = argument1;
var dirdiff = angle_difference(targetdir,dir);
return (dir+dirdiff*argument2) mod 360;