{
    "id": "ba74d262-9aec-440f-b3f5-45ccc1622d6e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGameController",
    "eventList": [
        {
            "id": "57b6c3f8-c506-46a3-b3d6-db11b9cca468",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ba74d262-9aec-440f-b3f5-45ccc1622d6e"
        },
        {
            "id": "0d22a5c4-6198-4fc3-8a08-a49a004f15e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "ba74d262-9aec-440f-b3f5-45ccc1622d6e"
        },
        {
            "id": "e73ecdc0-8477-453c-b1bc-6d6effb9571f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ba74d262-9aec-440f-b3f5-45ccc1622d6e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}