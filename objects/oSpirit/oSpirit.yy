{
    "id": "67788cad-0c60-4679-baee-679cc49de304",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSpirit",
    "eventList": [
        {
            "id": "22e24c8b-566f-4b78-bbfa-384618320c76",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "67788cad-0c60-4679-baee-679cc49de304"
        },
        {
            "id": "f0255a0f-6c41-48d6-9a89-aadb110c1bcd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "67788cad-0c60-4679-baee-679cc49de304"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0e6315d0-88f1-480e-ae64-f6da72429c94",
    "visible": true
}