image_alpha = lerp(image_alpha,1,0.1);
image_xscale = lerp(image_xscale,1+(value-1)/2,0.05);
image_yscale = image_xscale;
dir = rlerp(dir,point_direction(x,y,global.moon_x,global.moon_y),0.05);
if (point_distance(x,y,global.moon_x,global.moon_y) < 50) {
	global.spirits+= value;
	instance_destroy();
	audio_play_sound(choose(aGhost1,aGhost2,aGhost3,aGhost4,aGhost5),5,false);
}
spd += acc;
x += lengthdir_x(spd,dir);
y += lengthdir_y(spd,dir);
image_angle = dir;