dir = random_range(0,180);
image_alpha = 0;
//image_blend = c_red;
spd = 0.1;
acc = 0.05;
if (oShrine.shrine) {
	value = 2;
} else {
	value = 1;
}
if (oShrine.altar) {
	if (random(1) < 0.2) {
		instance_create_depth(x,y-16,depth,oReincarnate);
	}
}
if (oShrine.sacrifice && !instance_exists(oShrineAttack)) {
	oShrine.blood += 1;
}