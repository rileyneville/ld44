/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

upgrades = [upgrade_house,upgrade_house,upgrade_house,upgrade_house];
upgrade_descriptions = [
"Cottage:\nThe smoke from your chimney lures more humans in to investigate.",
"House:\nThe smell of enchanted cookies attracts even more humans to your woods.",
"Mansion:\nStories of the haunted mansion in the woods attract would-be ghost hunters.",
"Castle:\nThe screams from your dungeons rile the local townsfolk into action"];
upgrade_costs = [3,25,100,500];


upgrade_sprites = [sHut,sCottage,sHouse,sMansion,sCastle];

