{
    "id": "f9932a7d-88c3-4d40-b899-f92e22ede95a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHouseUpgrade",
    "eventList": [
        {
            "id": "46bac130-25a5-4e61-ba3b-e813930dc149",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f9932a7d-88c3-4d40-b899-f92e22ede95a"
        },
        {
            "id": "a27cc462-c5fa-4e9e-9c75-ca5e9f8e953d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f9932a7d-88c3-4d40-b899-f92e22ede95a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5d386555-8d32-4e91-af79-f5f6cb9bc0bb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e5420dd6-06db-4328-8885-dbc80d98dfed",
    "visible": true
}