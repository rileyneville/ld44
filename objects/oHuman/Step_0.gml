switch (state) {
	case (HumanState.recover): {
		hsp = lerp(hsp,0,0.45);
		vsp = vsp + grv;
		sprite_index = sHuman_recover;
	} break;
	case (HumanState.walk): {
		var trap = instance_place(x,y,oTrap);
		if (instance_exists(trap)) {
			if (trap.enabled && trap.active && trap.floor_trap) {
				state = HumanState.fall;
			}
		}  
		
		if (abs(hsp) > spd) {
			hsp = lerp(hsp,spd*dir,0.5);
		} else {
			hsp = lerp(hsp,spd*dir,acc);
		}
		vsp = vsp + grv;
		
		/*if (bbox_right > room_width) {
			dir = -1;
		} else if (bbox_left < 0) {
			dir = 1;
		}*/
		switch(type) {
			case HumanType.normal:
				sprite_index = sHuman_walk;
			break;
			case HumanType.fork:
				sprite_index = sHuman_walk_fork;
			break;
			case HumanType.torch:
				sprite_index = sHuman_walk_torch;
			break;
		}
		image_speed = abs(hsp)/spd;
	
		
		
	} break;
	case (HumanState.drag): {
		
		sprite_index = sHuman_flail;
		if (point_distance(x,y,mouse_x,mouse_y+8) <= oTelekenisis.radius) {
			hsp = lerp(hsp,0,0.75);
			vsp = lerp(vsp,0,0.75);
		} else {
			hsp = lerp(hsp,mouse_x-x,0.25);
			vsp = lerp(vsp,(mouse_y+8)-y,0.25);
		}
		with (oHuman) {
			if (other != id) {
				var ddist = point_distance(other.x,other.y,x,y);
				if (ddist <= 8) {
					hsp += sign(x-other.x);
				}
			}
		}
		if (mouse_check_button_released(1)) {
			hsp = lerp(hsp,0,0.75);
			vsp = lerp(vsp,0,0.75);
			max_speed(20);
			state = HumanState.fall;
		}
	} break;
	case (HumanState.fall): {
		sprite_index = sHuman_flail;
		hsp = lerp(hsp,0,0.015);
		if (hsp != 0) {
			dir = sign(hsp);
		}
		vsp = vsp + grv;
		vsp = lerp(vsp,0,0.015);
		var trap = instance_place(x,y,oTrap);
		if (instance_exists(trap)) {
			if (trap.enabled && trap.active || trap.trapped == id) {
				hsp = lerp(hsp,trap.x-x,0.25);
				vsp = lerp(vsp,trap.y-y,0.25);
				if (point_distance(x,y,trap.x,trap.y) <= 1) {
					trap.trapped = id;
				}
			} else { trap = noone; }
		}
		
		if (trap == noone && bbox_bottom >= global.floor_level-1) {
			state = HumanState.recover;	
			image_speed = 1;
			image_index = 0;
		}
		
	} break;
}

if (state != HumanState.walk && type != HumanType.normal){
	type = HumanType.normal;
	switch(type) {
		case HumanType.fork:
			//instance_create_depth(x,y,depth,oFork);
		break;
		case HumanType.torch:
			//instance_create_depth(x,y,depth,oTorch);
		break;
	}
}

image_xscale = dir;
max_speed(maxspd);


x += floor(abs(hsp))*sign(hsp);
hf += frac(hsp);
if (hf >= 1) {
	x += 1;
	hf -= 1;
} else if (hf <= -1) {
	x -= 1;
	hf += 1;
}
if (bbox_right < 0 || bbox_left > room_width) {
	if (state != HumanState.drag) {
		instance_destroy();
	}
}

y += floor(abs(vsp))*sign(vsp);
vf += frac(vsp);
if (vf >= 1) {
	y += 1;
	vf -= 1;
} else if (vf <= -1) {
	y -= 1;
	vf += 1;
}
if (y > room_height) {
	instance_destroy();
	//vsp = 0;
	//y = clamp(y,0,room_height);
}

while (bbox_bottom > global.floor_level) {
	vsp = 0;
	var diff = y-bbox_bottom;
	y = global.floor_level+diff;
}