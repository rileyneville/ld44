enum HumanType {
	normal,
	torch,
	fork,
	count
}

type = irandom_range(0,HumanType.count-1);

enum HumanState {
	walk,
	fall,
	drag,
	recover
}

state = HumanState.walk;
dir = 1;
spd = 0.35;
acc = 0.05;
grv = 0.09;
maxspd = 40;

hsp = 0;
hf = 0;
vsp = 0;
vf = 0;

light_scale = 0;