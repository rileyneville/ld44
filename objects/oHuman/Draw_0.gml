draw_self();
if (type == HumanType.torch) {
	light_scale = lerp(light_scale,random_range(0.25,0.65),0.1);
} else {
	light_scale = lerp(light_scale,0,0.2);
}
if (light_scale > 0.05) {
	gpu_set_blendmode(bm_add);
	draw_sprite_ext(sLight,0,x+dir*4,y-2,light_scale,light_scale,0,c_white,1);
	gpu_set_blendmode(bm_normal);
}