{
    "id": "db180e85-16b0-484e-b388-9a4272f90f05",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHuman",
    "eventList": [
        {
            "id": "3130c4d3-011f-428c-ba6b-25f03d74e980",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "db180e85-16b0-484e-b388-9a4272f90f05"
        },
        {
            "id": "87967040-2141-45f4-abd0-a97997a7c25d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "db180e85-16b0-484e-b388-9a4272f90f05"
        },
        {
            "id": "d334241a-5380-40c1-afa0-38c9f14beb6c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "db180e85-16b0-484e-b388-9a4272f90f05"
        },
        {
            "id": "43e59d20-5eff-482e-be86-3dc19c0c7f2f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "db180e85-16b0-484e-b388-9a4272f90f05"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9b07966a-eedb-4e70-9452-b3bff8f5cb4a",
    "visible": true
}