{
    "id": "27859f65-2ce7-4a1f-abcb-002ccc4d8d50",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMagicMirror",
    "eventList": [
        {
            "id": "d80d3e93-f97f-423c-afae-2da0cc6d338d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "27859f65-2ce7-4a1f-abcb-002ccc4d8d50"
        },
        {
            "id": "26f96330-e867-4767-a8c9-55d3fbd15c97",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "27859f65-2ce7-4a1f-abcb-002ccc4d8d50"
        },
        {
            "id": "440164cd-c8bb-459d-a1bf-256ecd034270",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "27859f65-2ce7-4a1f-abcb-002ccc4d8d50"
        },
        {
            "id": "b5169bc7-4122-4463-9667-a70c108f5ac1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "27859f65-2ce7-4a1f-abcb-002ccc4d8d50"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5d386555-8d32-4e91-af79-f5f6cb9bc0bb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4a591e4b-8676-48bc-a5bb-d7947d291700",
    "visible": true
}