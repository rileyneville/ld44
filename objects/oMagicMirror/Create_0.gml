event_inherited();
enabled = false;
timer = 0;

upgrades = [enable_self];
upgrade_descriptions = ["Magic Mirror:\nCreates a copy of anyone who glances into it."];
upgrade_costs = [30];