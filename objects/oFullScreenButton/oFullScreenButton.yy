{
    "id": "1d3c8824-ceda-44e2-9a3a-2836343f96fc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFullScreenButton",
    "eventList": [
        {
            "id": "79c1336e-eeb9-4127-957e-5ea2cf539714",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1d3c8824-ceda-44e2-9a3a-2836343f96fc"
        },
        {
            "id": "7323f175-7aaa-4de8-984c-0c1fdba04e2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1d3c8824-ceda-44e2-9a3a-2836343f96fc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b78ceddf-fd4a-4cd6-b091-e191e54c5975",
    "visible": true
}