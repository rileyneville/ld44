image_index = fullscreen;
if (mouse_check_button_pressed(1) 
	&& point_in_rectangle(mouse_x,mouse_y,bbox_left,bbox_top,bbox_right,bbox_bottom)) {
	fullscreen = !fullscreen;	
	window_set_fullscreen(fullscreen);
}