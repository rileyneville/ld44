{
    "id": "14323d14-ba24-4ac1-a9dd-c83dd2637d50",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oReincarnate",
    "eventList": [
        {
            "id": "f3e8128f-15e6-4e1d-b8d4-8b44d313f95c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "14323d14-ba24-4ac1-a9dd-c83dd2637d50"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "60b22275-d801-4ac6-8251-0a1aafec2316",
    "visible": true
}