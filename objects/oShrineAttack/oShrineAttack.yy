{
    "id": "46e2a92d-92f1-4382-83aa-0093e17baa5b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oShrineAttack",
    "eventList": [
        {
            "id": "d814fdc0-a18c-4653-9c15-a80bd7be8e77",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "46e2a92d-92f1-4382-83aa-0093e17baa5b"
        },
        {
            "id": "a548bbed-b228-4ca7-b719-c9c16573afbf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "46e2a92d-92f1-4382-83aa-0093e17baa5b"
        },
        {
            "id": "08667955-8007-441f-9a9b-14074b2aeda2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "46e2a92d-92f1-4382-83aa-0093e17baa5b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "589cfac6-f78a-45e7-a458-760d8bbdaa1f",
    "visible": true
}