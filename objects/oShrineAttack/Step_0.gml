if (image_index > 10 && image_index < 12) {
	with (oHuman) {
		if (place_meeting(x,y,other)) {
			instance_destroy();
			instance_create_depth(x,y,depth,oSpirit);
			instance_create_depth(x,y,depth,oHumanObliterated);
		}
	}
}
