{
    "id": "5bb933b9-20f6-4dfc-bae4-ad0f6a8a76f8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTrap",
    "eventList": [
        {
            "id": "8b18f067-888c-47bd-a686-7b8d4e9adfbc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5bb933b9-20f6-4dfc-bae4-ad0f6a8a76f8"
        },
        {
            "id": "0b3ac796-3a78-4d0e-9f48-7c731887cecc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5bb933b9-20f6-4dfc-bae4-ad0f6a8a76f8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5d386555-8d32-4e91-af79-f5f6cb9bc0bb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}