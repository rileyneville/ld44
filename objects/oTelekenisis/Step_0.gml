
if (mouse_check_button(1)) {
	if (mouse_check_button_pressed(1)) {
		audio_sound_gain(sound,1,200);
	}
	with (oHuman) {
		if (other.held < other.max_humans &&
			state != HumanState.drag &&
			distance_to_point(mouse_x,mouse_y) <= other.radius) {
			other.held ++;
			state = HumanState.drag;
		}
	}
	part_emitter_region(ps,pe,mouse_x-radius,mouse_x+radius,mouse_y-radius,mouse_y+radius,ps_shape_ellipse,ps_distr_invgaussian);
	switch(max_humans) {
		case 1:
			part_emitter_stream(ps,pe,pt,-4);
			break;
		case 2:
			part_emitter_stream(ps,pe2,pt,-4);
			part_emitter_stream(ps,pe,pt,-2);
			break;
		case 4:
			part_emitter_stream(ps,pe2,pt,-2);
			part_emitter_stream(ps,pe,pt,1);
			break;
	}
	image_speed = 2;
} else {
	image_speed = 1;
}

if (mouse_check_button_released(1)) {
	audio_sound_gain(sound,0,200);
	held = 0;
	part_emitter_stream(ps,pe,pt,0);
	part_emitter_stream(ps,pe2,pt,0);
}

/*if (mouse_check_button_pressed(2)) {
	with (instance_create_layer(mouse_x,mouse_y,"Humans",oHuman)) {
		state = HumanState.fall;
	}
}*/

sprite_index = upgrade_sprites[upgrade_level];