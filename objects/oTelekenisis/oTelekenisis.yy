{
    "id": "52dcf1aa-1cc5-4955-896b-2297f0a5af2b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTelekenisis",
    "eventList": [
        {
            "id": "a9fdad24-a09d-4801-b36b-ad609a3ae887",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "52dcf1aa-1cc5-4955-896b-2297f0a5af2b"
        },
        {
            "id": "c94eafc1-0723-413e-aebb-a04fb4dc4cdc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "52dcf1aa-1cc5-4955-896b-2297f0a5af2b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5d386555-8d32-4e91-af79-f5f6cb9bc0bb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "15465a26-fb49-42c4-856a-674df0b27835",
    "visible": true
}