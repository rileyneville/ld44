/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

radius = 2;
max_humans = 1;

upgrades = [upgrade_telekenisis,upgrade_telekenisis];
upgrade_descriptions = ["Scrying Stone:\nAllows you to carry two humans at once.",
"Crystal Ball:\nAllows you to carry four humans at once."];
upgrade_costs = [10,30];
upgrade_sprites = [sNone,sScryingStone,sCrystalBall];
held = 0;

ps = part_system_create();
pt = part_type_create();
part_type_blend(pt,true);
part_type_shape(pt,pt_shape_sphere);
part_type_size(pt,0.02,0.04,0,0.01);
part_type_alpha3(pt,0.5,1,0);
part_type_life(pt,30,60);
part_type_speed(pt,0.25,1,-0.02,0);
part_type_direction(pt,0,359,0,15);
pe = part_emitter_create(ps);
pe2 = part_emitter_create(ps);
part_emitter_region(ps,pe2,x-16,x+16,y-16,y+16,ps_shape_ellipse,ps_distr_invgaussian);

sound = audio_play_sound(aTelekenisis,10,true);
audio_sound_gain(sound,0,0);