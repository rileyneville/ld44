/// @description Insert description here
// You can write your code in this editor

if (instance_exists(trapped)) {
	if (sprite_index != sCauldron_large) {
		active = false;
		alarm[0] = 30;
	}
	instance_destroy(trapped);
	trapped = noone;	
	instance_create_depth(oCauldron.x,oCauldron.y,depth,oCauldron_splash);
	with (instance_create_depth(x,y,depth,oSpirit)) {
		value += other.spirit_value;
		audio_play_sound(choose(aDrown1,aDrown2,aDrown3,aDrown4,aDrown5,aDrown6),10,false);
	}
}