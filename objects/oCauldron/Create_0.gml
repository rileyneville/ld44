/// @description Insert description here
// You can write your code in this editor
event_inherited();
enabled = true;
light_scale = 1;
spirit_value = 0;
upgrades = [upgrade_cauldron_size,upgrade_spirit_value];
upgrade_costs = [5,50];
upgrade_descriptions = ["Large Cauldron:\nCan boil multiple humans at once.",
"Eye of Newt:\nCauldron extracts more life force from each victim."];