if (enabled) {
	draw_self();
	light_scale = lerp(light_scale,random_range(0.9,1.25),0.1);
	gpu_set_blendmode(bm_add);
	draw_sprite_ext(sLight,0,x,y,light_scale,light_scale,0,c_white,1);
	gpu_set_blendmode(bm_normal);
}