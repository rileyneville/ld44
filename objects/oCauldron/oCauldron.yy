{
    "id": "e547adbe-0da8-41d3-8602-5f3fcddbda12",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCauldron",
    "eventList": [
        {
            "id": "45f40785-a3f5-4b23-8eb5-7c2f4e8e81cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e547adbe-0da8-41d3-8602-5f3fcddbda12"
        },
        {
            "id": "805a621b-61c1-4c78-818e-77246ed54af8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e547adbe-0da8-41d3-8602-5f3fcddbda12"
        },
        {
            "id": "4d6ce569-4f56-44d3-a1b4-780ec92c3efd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e547adbe-0da8-41d3-8602-5f3fcddbda12"
        },
        {
            "id": "78bc7b9e-bf0a-4c86-8b60-497234c5d41e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "e547adbe-0da8-41d3-8602-5f3fcddbda12"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5bb933b9-20f6-4dfc-bae4-ad0f6a8a76f8",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d871312b-8f70-45fb-9f4e-2197c2da988c",
    "visible": true
}