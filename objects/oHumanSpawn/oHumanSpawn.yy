{
    "id": "4fea8c3e-34e5-436b-ad9d-b4b2e63a75ff",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHumanSpawn",
    "eventList": [
        {
            "id": "1a5d94f5-b98a-4e95-a992-5048da71f39a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4fea8c3e-34e5-436b-ad9d-b4b2e63a75ff"
        },
        {
            "id": "7475a32d-1db4-437f-aa33-6531d7784906",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4fea8c3e-34e5-436b-ad9d-b4b2e63a75ff"
        },
        {
            "id": "b9979342-eb66-4ae8-a8cd-f9df6c00701e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4fea8c3e-34e5-436b-ad9d-b4b2e63a75ff"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}