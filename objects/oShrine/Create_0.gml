/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
shrine = false;
altar = false;
sacrifice = false;
blood = 0;
upgrades = [upgrade_shrine,upgrade_altar,upgrade_sacrificial_altar];
upgrade_descriptions = ["Shrine:\nExtract more life force when sacrificing humans.",
"Altar:\nHumans gain a slight chance to reincarnate.",
"Sacrificial Altar:\nEach human you kill contributes its blood to a devastating spell."];
upgrade_costs = [15,30,70];
