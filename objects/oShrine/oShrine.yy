{
    "id": "3d69ab0b-265f-4483-9ce4-0f64c7e85506",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oShrine",
    "eventList": [
        {
            "id": "0b52d6b1-422a-4210-9f71-e8c65a1f18c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3d69ab0b-265f-4483-9ce4-0f64c7e85506"
        },
        {
            "id": "b58d1648-6405-4ee5-a101-12a86909ce54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3d69ab0b-265f-4483-9ce4-0f64c7e85506"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5d386555-8d32-4e91-af79-f5f6cb9bc0bb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}