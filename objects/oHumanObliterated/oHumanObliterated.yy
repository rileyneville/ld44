{
    "id": "5bb9062b-8b2a-47d8-8f1c-eb6f52b9448a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHumanObliterated",
    "eventList": [
        {
            "id": "8dc3fb10-b218-49d6-b604-e38607a4e9bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "5bb9062b-8b2a-47d8-8f1c-eb6f52b9448a"
        },
        {
            "id": "ae2bfc92-4a4e-488d-a810-18333d307d00",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5bb9062b-8b2a-47d8-8f1c-eb6f52b9448a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ed442927-17ce-4551-b540-ad9c246b9d39",
    "visible": true
}