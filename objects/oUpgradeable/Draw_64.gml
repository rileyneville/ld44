if (array_length_1d(upgrades) > upgrade_level) {
	
	
	bwidth = 50;
	var bx1 = x-bwidth/2;
	var bx2 = x+bwidth/2;
	var by1 = global.floor_level + 8;
	var by2 = global.floor_level + 32;
	var mouse_hover = point_in_rectangle(mouse_x,mouse_y,bx1,by1,bx2,by2);
	
	if (global.spirits >= upgrade_costs[upgrade_level]) {
		if (mouse_hover) {
			draw_set_color(c_white);
		} else {
			draw_set_color(c_red);
		}
	} else {
		draw_set_color(c_gray);
	}
	
	var text = upgrade_costs[upgrade_level];
	draw_set_font(fBuy);
	draw_set_halign(fa_center);
	draw_set_valign(fa_top);
	draw_text(x,global.floor_level+10,text);
	
	
	draw_set_color(c_white);
	
	if (mouse_hover) {
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);
		draw_text(50,50,upgrade_descriptions[upgrade_level]);
	}
	
	var button_clicked = mouse_hover && mouse_check_button_pressed(1);
	if (button_clicked && global.spirits >= upgrade_costs[upgrade_level]) {
		global.spirits -= upgrade_costs[upgrade_level];
		script_execute(upgrades[upgrade_level]);
		upgrade_level++;
	}
	
	
	
}