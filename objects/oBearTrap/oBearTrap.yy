{
    "id": "1caf99c6-4c8c-40c2-88d5-66394b4fced7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBearTrap",
    "eventList": [
        {
            "id": "263185cc-0f5d-4942-83d9-2d8ea3030a1e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "1caf99c6-4c8c-40c2-88d5-66394b4fced7"
        },
        {
            "id": "a3fd405b-73fe-412f-91a2-116fcb9bcb02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1caf99c6-4c8c-40c2-88d5-66394b4fced7"
        },
        {
            "id": "32bc1fe1-ade8-48d7-9644-eb843d4a0793",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1caf99c6-4c8c-40c2-88d5-66394b4fced7"
        },
        {
            "id": "42c74b65-a518-4423-b1ac-8ce3f86d7e01",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "1caf99c6-4c8c-40c2-88d5-66394b4fced7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5bb933b9-20f6-4dfc-bae4-ad0f6a8a76f8",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "224ae061-7565-49d3-999f-1f417f3055cd",
    "visible": true
}