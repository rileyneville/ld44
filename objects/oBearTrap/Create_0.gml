/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
floor_trap = true;
autoreset = false;

upgrades = [enable_self,enable_autoreset];
upgrade_descriptions = ["Bear Trap:\nAutomatically crushes intruders with its sharp jaws",
"Enchanted Pressure Plate:\nBear trap resets itself after being triggered"];
upgrade_costs = [5,15];