if (instance_exists(trapped) && active) {
	active = false;
	sprite_index = sBearTrap_snap;
	image_index = 0;
	image_speed = 1;
	with(trapped) {
		instance_destroy();
		instance_create_depth(x,y,depth,oSpirit);
	}
	audio_play_sound(aTrap1,10,false);
	audio_play_sound(choose(aDead1,aDead2,aDead3,aDead4,aDead5,aDead6,aDead7,aDead8,aDead9),10,false);
	trapped = noone;
}

if (!active && mouse_check_button_pressed(1) 
&& collision_point(mouse_x,mouse_y,id,false,false) != noone
&& image_index > image_number-1) {
	alarm[0] = 1;
}