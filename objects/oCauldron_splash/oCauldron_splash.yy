{
    "id": "47ba4bd6-f98c-45fd-a577-4597308e6c24",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCauldron_splash",
    "eventList": [
        {
            "id": "9c8b531e-2b19-4276-b480-65f081d7c1b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "47ba4bd6-f98c-45fd-a577-4597308e6c24"
        },
        {
            "id": "d0c62013-ad0b-40ba-9ea1-ee63fc30a6a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "47ba4bd6-f98c-45fd-a577-4597308e6c24"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "352df41c-19b5-4da9-8926-87f0e16cf583",
    "visible": true
}