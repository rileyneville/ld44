{
    "id": "0165c443-5089-41c9-8ffb-40cb4cc9922b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMuteButton",
    "eventList": [
        {
            "id": "c56fcdca-d4dd-4c4d-b865-1dd6a69c398f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0165c443-5089-41c9-8ffb-40cb4cc9922b"
        },
        {
            "id": "0fd3627f-a970-4640-9229-70b2f5290d9d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0165c443-5089-41c9-8ffb-40cb4cc9922b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0ebb8328-bf6d-4d55-bebc-716885ed5fdc",
    "visible": true
}