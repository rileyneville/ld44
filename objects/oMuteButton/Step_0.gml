image_index = muted;
if (mouse_check_button_pressed(1) 
	&& point_in_rectangle(mouse_x,mouse_y,bbox_left,bbox_top,bbox_right,bbox_bottom)) {
	muted = !muted;	
	if (muted) {audio_set_master_gain(0,0);}
	else {audio_set_master_gain(0,1);}
}