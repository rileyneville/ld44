{
    "id": "e4980455-92bc-4052-9726-6fb1bdc9608c",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fMoon",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 2,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Chiller",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "e315a294-e1b5-4248-9c35-7c8bd3190b3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 42,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "0c20c341-e4eb-4c9a-bdee-520afa01e568",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 42,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 504,
                "y": 46
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "2e10c909-4ce0-4bde-89a4-a6d7aab9dc5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 42,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 496,
                "y": 46
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "75066a50-5996-4764-8f6b-d0eba0dae57f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 42,
                "offset": 1,
                "shift": 37,
                "w": 35,
                "x": 459,
                "y": 46
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "bb6aabe5-6b12-479d-96f2-9a77ce9af1d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 42,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 445,
                "y": 46
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "172eb860-5501-4aac-9405-19f35361969e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 42,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 426,
                "y": 46
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "db745c73-1e06-463b-964f-65d5bc6706be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 42,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 407,
                "y": 46
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "921b5e81-342d-46c2-b2e0-217cf789e873",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 42,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 401,
                "y": 46
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "359c759e-ade6-4464-8cc1-6a22648e3cd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 42,
                "offset": 3,
                "shift": 14,
                "w": 11,
                "x": 388,
                "y": 46
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "25a58999-634a-4c3f-bcfc-a4d422b5a901",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 42,
                "offset": 0,
                "shift": 14,
                "w": 11,
                "x": 375,
                "y": 46
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "f18c8727-a52a-4c2d-9c99-7b6d830ff3f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 42,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 90
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "17dfb676-fb91-429e-95a7-4e4a5ff29c6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 42,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 354,
                "y": 46
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "9b11ea89-6425-4ab7-96fa-cef1f7ea6bd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 42,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 331,
                "y": 46
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "0c7fc532-0bbc-4813-9587-a3e648079738",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 42,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 318,
                "y": 46
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "049b426b-c045-4ee5-90e9-85bfc584e389",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 42,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 311,
                "y": 46
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "ad101d3d-9807-4de7-a5d7-d97a1f6a2906",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 42,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 292,
                "y": 46
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "03792206-5bf8-4c60-bc69-974e3af0803f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 42,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 276,
                "y": 46
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "200a35e5-8452-41f7-ad01-cbf6fc262bb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 42,
                "offset": -1,
                "shift": 11,
                "w": 10,
                "x": 264,
                "y": 46
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "f20d8ee9-85a7-4aae-97bf-e3c03f9a1f91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 42,
                "offset": -1,
                "shift": 15,
                "w": 14,
                "x": 248,
                "y": 46
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "7f70b986-27d9-43f6-9639-da1f037000da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 42,
                "offset": -2,
                "shift": 15,
                "w": 16,
                "x": 230,
                "y": 46
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "22a99e90-951b-46dd-8e6c-e46ff349a892",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 42,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 213,
                "y": 46
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "2f453cba-617b-4441-80b1-f14a9a6f740b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 42,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 338,
                "y": 46
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "3cc1c365-6e77-4324-b02f-24d893ca2b8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 42,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 16,
                "y": 90
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "a5434099-1905-476b-ac87-7b3595fe6eda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 42,
                "offset": -1,
                "shift": 15,
                "w": 15,
                "x": 30,
                "y": 90
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "64c972df-0ef2-492f-af47-b541907059a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 42,
                "offset": -1,
                "shift": 16,
                "w": 16,
                "x": 47,
                "y": 90
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "74c15958-78ea-4458-bb1b-61a5e1e06947",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 42,
                "offset": -3,
                "shift": 14,
                "w": 16,
                "x": 386,
                "y": 90
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "7071ad16-0904-407f-a06c-71cb543974fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 42,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 379,
                "y": 90
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "95649d0c-427b-4b2e-b740-c3c670e86e84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 42,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 372,
                "y": 90
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "1a089fea-8b6d-4c42-9129-a98d4c168df6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 42,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 356,
                "y": 90
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "76c34aa9-5bed-441b-969e-3d9225c935f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 42,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 342,
                "y": 90
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "661ec260-f02b-4624-b427-47ef8da915de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 42,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 327,
                "y": 90
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "9fed00f4-30dc-4691-bf91-093988a09239",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 42,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 314,
                "y": 90
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "77fe01a1-a26d-473e-b336-b45c8806e143",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 42,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 292,
                "y": 90
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "aa36c70e-00df-4252-b999-4e1c2b562bb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 42,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 272,
                "y": 90
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "16dccd0e-26ee-4719-a772-d9a1e947f26e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 42,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 253,
                "y": 90
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "4c5cc2c0-b2c8-4827-bb6f-6d15a4e9c822",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 42,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 234,
                "y": 90
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "5fc22747-ab6e-424d-8eb0-b656a8ea039b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 42,
                "offset": -1,
                "shift": 15,
                "w": 15,
                "x": 217,
                "y": 90
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "8c8a9b00-d62f-4460-af88-d1f39f2eaac1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 42,
                "offset": 1,
                "shift": 15,
                "w": 15,
                "x": 200,
                "y": 90
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "bca390b6-1e70-44e1-95a9-a34aabfa77fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 42,
                "offset": 1,
                "shift": 12,
                "w": 13,
                "x": 185,
                "y": 90
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "baa5a7aa-da08-430a-83cb-b14a1f4fd746",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 42,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 171,
                "y": 90
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "a6b02575-0bd4-47f3-acac-ff5937b0941c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 42,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 157,
                "y": 90
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "f3e20f35-7aad-4a8d-9c8f-00c2e3eb50c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 42,
                "offset": -1,
                "shift": 12,
                "w": 14,
                "x": 141,
                "y": 90
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "6302c30d-6017-4932-9e79-631ea6bef198",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 42,
                "offset": -2,
                "shift": 14,
                "w": 19,
                "x": 120,
                "y": 90
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "d717bdd3-9630-4581-8b3b-dd4f2190a1eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 42,
                "offset": 1,
                "shift": 16,
                "w": 16,
                "x": 102,
                "y": 90
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "17186735-6d64-4420-989c-293e048ce79d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 42,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 86,
                "y": 90
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "8e6216b9-ea6f-4f45-bb96-ac0a1e6bb40b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 42,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 65,
                "y": 90
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "fca61093-5c81-4593-9375-cfeb8074b768",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 42,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 194,
                "y": 46
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "892aec72-e2f8-42ab-a96c-4ff50d2a21a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 42,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 177,
                "y": 46
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "f12126c1-99e0-4eb3-8124-2d22b0d10417",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 42,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 161,
                "y": 46
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "79cab202-12e9-44f0-bfe9-241fb367fbab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 42,
                "offset": 1,
                "shift": 16,
                "w": 20,
                "x": 358,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "24ebad4f-3b65-4009-a473-63874885dd57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 42,
                "offset": 1,
                "shift": 15,
                "w": 16,
                "x": 322,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "868c9803-ace9-415a-b507-46ca800abc97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 42,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 309,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "979381ae-35f2-4e73-a212-f0d34e2a49d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 42,
                "offset": -1,
                "shift": 14,
                "w": 19,
                "x": 288,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "2de89a63-0231-4b6b-9533-6c2fda25dd54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 42,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 272,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "9524f212-0c6e-46e0-ba0a-171d4526c0a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 42,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 255,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b754dba7-6c30-4ef0-863c-85a0231822f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 42,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "cefff338-2c2f-4122-af5f-22de7808cd3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 42,
                "offset": -1,
                "shift": 16,
                "w": 17,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "81c6ef57-3b98-49c7-9c0e-b46d86e65d7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 42,
                "offset": -1,
                "shift": 14,
                "w": 17,
                "x": 194,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "a8fb1025-54c8-4ae1-a4cd-3b038bd1b39c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 42,
                "offset": -1,
                "shift": 17,
                "w": 20,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "f6d52381-e20c-4a2b-b416-ea1a10f2ad72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 42,
                "offset": 0,
                "shift": 14,
                "w": 16,
                "x": 340,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "61f3bafc-f3ad-45d7-80c4-fe6a86192185",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 42,
                "offset": -1,
                "shift": 17,
                "w": 18,
                "x": 152,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "e4c05960-31b4-43ae-878a-f2c9683e5ad2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 42,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "7612df82-79ce-4b6c-9ccd-5d589db6bf90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 42,
                "offset": 2,
                "shift": 18,
                "w": 13,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "6612d1b7-f5b5-4440-bf64-2e0a28ab6229",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 42,
                "offset": -1,
                "shift": 18,
                "w": 19,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "25f45ff2-c73d-426d-9197-82bed5d5840a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 42,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a0f9a8a1-0add-4ef5-a72e-e975b3ee2dbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 42,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "c1b564eb-c336-4927-835b-23ed5c5df46c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 42,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "5b2d822a-e7ee-4380-bbb8-bb58851b544b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 42,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "07bd57a9-3711-4d19-ba97-39879080481b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 42,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "d85235e9-2b6c-4ffe-a5b9-de0ab3a02ffc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 42,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "3840d2bb-79f8-45d2-bf1d-4e6496ab2971",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 42,
                "offset": -1,
                "shift": 7,
                "w": 10,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "02a5c202-fbd8-4974-bae7-059b988251c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 42,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 380,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "43179b0b-4df5-4b5c-bea4-3e3128abb790",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 42,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 15,
                "y": 46
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "662f7aca-fbaa-4376-b1d1-2169a801e841",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 42,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 392,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "eafbefbf-2fd1-4c3d-bf29-1781fdcb1093",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 42,
                "offset": -5,
                "shift": 8,
                "w": 12,
                "x": 136,
                "y": 46
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "34069968-00e2-4496-a9b5-6a897086e389",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 42,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 124,
                "y": 46
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "9365ea9d-b278-4aa7-b787-7f09aa982551",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 42,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 116,
                "y": 46
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "e9840aa4-23c8-4ef3-bcf0-f4b98a3bc046",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 42,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 100,
                "y": 46
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "60c93ff1-970c-4850-8ea1-42e52d20d30a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 42,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 88,
                "y": 46
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "43ccdbe8-375e-4eb8-b4f8-afeae0e5056e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 42,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 77,
                "y": 46
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "8f830448-7c1b-4c21-ac6e-6cde2ae505f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 42,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 64,
                "y": 46
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "dd30b8aa-0eb2-4042-ae92-90e010c6bf22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 42,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 51,
                "y": 46
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "651a074e-aa80-44d6-a3d1-ba8e3e4f0caa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 42,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 41,
                "y": 46
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "a739dcf9-d348-47aa-93e4-432f3d2f1683",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 42,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 150,
                "y": 46
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e1f8f242-9907-4775-8b7c-b1361f08a6dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 42,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 28,
                "y": 46
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "73e549e2-3759-4039-9f1a-31739b3a141f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 42,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "c9064045-b078-467f-b8cb-4f86b74b33e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 42,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 494,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "bd511595-856d-4179-998a-b014b385d171",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 42,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 480,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "45f8f906-0d37-4aef-9d91-ab853c90b74f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 42,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 467,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "64934225-6580-4ce2-80e5-f6a2028f6d44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 42,
                "offset": 0,
                "shift": 13,
                "w": 11,
                "x": 454,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "70de25d1-2ffb-4b19-beb8-8ea88c9ec9dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 42,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 439,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "0e0561f3-f68a-4287-86f3-8ff601bf05c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 42,
                "offset": -2,
                "shift": 12,
                "w": 14,
                "x": 423,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "1300ef52-9349-432d-9edb-b11a75e1df03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 42,
                "offset": 4,
                "shift": 12,
                "w": 9,
                "x": 412,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "5dfab677-4bc3-4cf6-a531-81d1439f9739",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 42,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 399,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c0d83e8e-b849-4b60-9974-aaee06485fcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 42,
                "offset": -1,
                "shift": 13,
                "w": 14,
                "x": 404,
                "y": 90
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "2df5c9bd-50a0-4a1d-86c4-5385cb734dec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 42,
                "offset": 7,
                "shift": 34,
                "w": 20,
                "x": 420,
                "y": 90
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 26,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}