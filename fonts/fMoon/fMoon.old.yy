{
    "id": "e4980455-92bc-4052-9726-6fb1bdc9608c",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fMoon",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Chiller",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "fe1a67e1-509c-43a7-af2a-7fa4ac1f4990",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 41,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ded73aac-7ace-4fa5-b15d-93c07f90bbf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 41,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 445,
                "y": 45
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "bbb28bdf-d8b4-49ef-b9a0-fcf1bbbfc821",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 41,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 437,
                "y": 45
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "02087eaf-c9fa-4267-820d-d2378664ffcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 41,
                "offset": 1,
                "shift": 37,
                "w": 35,
                "x": 400,
                "y": 45
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "5a91cf03-9b7e-427f-9f23-7dd6f13663f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 41,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 387,
                "y": 45
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "bfeb2f3d-44b7-4984-9791-96efdc2a5e53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 41,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 369,
                "y": 45
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "9df4971e-296c-4579-802a-7039dc744a80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 41,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 351,
                "y": 45
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "31923624-2dea-4e94-98e4-2b91be9670cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 41,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 346,
                "y": 45
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "f92e4eee-0ef4-4844-8d91-2d34f9cca4e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 41,
                "offset": 4,
                "shift": 14,
                "w": 10,
                "x": 334,
                "y": 45
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "980cda2a-6b8f-4547-81fa-ed237f670b9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 41,
                "offset": 0,
                "shift": 14,
                "w": 11,
                "x": 321,
                "y": 45
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "e89f3890-4e3b-435b-8dc0-8b6baec8c085",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 41,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 452,
                "y": 45
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "5115c5e4-2cad-4371-9073-b5b392b60c58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 41,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 300,
                "y": 45
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "20bb3a88-5bef-4d09-a40d-3c84dd7a807a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 41,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 278,
                "y": 45
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "360111c0-aca4-412a-86d8-2d0308d42e18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 41,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 265,
                "y": 45
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "5fefcf47-fb44-498f-891c-1385f2d6b644",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 41,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 259,
                "y": 45
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "11a536a5-741c-447c-a496-ff2c23a9df16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 41,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 240,
                "y": 45
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "09caf53a-3a5c-48b8-a052-ce2e836a207f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 41,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 224,
                "y": 45
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "0643369c-3620-4184-8a84-db08b1ff20ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 41,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 214,
                "y": 45
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "e759b958-4a1f-408c-98db-cd0e52c2611e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 41,
                "offset": -1,
                "shift": 15,
                "w": 14,
                "x": 198,
                "y": 45
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "8ce515bd-e21d-43f7-a1e2-7462511b2d12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 41,
                "offset": -1,
                "shift": 15,
                "w": 15,
                "x": 181,
                "y": 45
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "abcb31fa-d1ce-498f-bbf4-b4eda96eb707",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 41,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 165,
                "y": 45
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "da388626-02ca-416f-a13e-c379dbca49bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 41,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 284,
                "y": 45
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "e469c2e9-fe00-4b3e-aa89-4a99082124a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 41,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 465,
                "y": 45
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "3ee9e4e1-8170-4e21-91e7-331a8e0570d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 41,
                "offset": -1,
                "shift": 15,
                "w": 15,
                "x": 478,
                "y": 45
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "c9d56576-2414-4695-85df-303e2e980911",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 41,
                "offset": -1,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 88
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "72fde861-9fbe-4185-9e1e-1c00f38d5471",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 41,
                "offset": -2,
                "shift": 14,
                "w": 14,
                "x": 319,
                "y": 88
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e35d0fa8-d802-4c05-ab40-fbd3e4c7bb32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 41,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 313,
                "y": 88
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "bb893135-56ee-4886-95fa-ce83564c8415",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 41,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 307,
                "y": 88
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "c7f6be72-1f4e-4e21-8de3-b43e7a419142",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 41,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 292,
                "y": 88
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "9d899e0d-15a5-4b17-aab5-01beab99ad44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 41,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 279,
                "y": 88
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "61361995-a54b-45fe-91c0-d6ab0c0e8533",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 41,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 265,
                "y": 88
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "73fc5a5c-3218-457b-9b27-79d20ec7cfa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 41,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 253,
                "y": 88
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "a8c790d0-8f04-4114-a61d-26b7fb5341ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 41,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 232,
                "y": 88
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "ae73ad25-0ec4-4d26-a7e5-2a38bb89ea71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 41,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 214,
                "y": 88
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "61ddc6b9-dc29-473a-adde-fa0096eea799",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 41,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 197,
                "y": 88
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "39633c2b-1a68-4d77-a70f-6172b65fd689",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 41,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 179,
                "y": 88
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "f48ec401-72bf-4d27-b125-7be0e9ea18b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 41,
                "offset": -1,
                "shift": 15,
                "w": 14,
                "x": 163,
                "y": 88
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c09041a0-28af-4348-8405-e267db1b8e60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 41,
                "offset": 1,
                "shift": 15,
                "w": 15,
                "x": 146,
                "y": 88
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "dea3aede-8a5d-425e-87f4-5dec6e72bb5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 41,
                "offset": 2,
                "shift": 12,
                "w": 11,
                "x": 133,
                "y": 88
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "cdf9a7ea-071a-407a-99c0-72cc94ada88e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 41,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 120,
                "y": 88
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "6f8dcc85-0cc1-4185-a202-fb0dc2eaeb7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 41,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 106,
                "y": 88
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "1084b11f-eb3f-48fa-970a-aece9cf78dd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 41,
                "offset": -1,
                "shift": 12,
                "w": 13,
                "x": 91,
                "y": 88
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "4572235e-dc27-4184-af7b-dbd12f69fc60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 41,
                "offset": -1,
                "shift": 14,
                "w": 18,
                "x": 71,
                "y": 88
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "c4571ae8-f0e1-4821-8f60-bdccf8d3ef8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 41,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 55,
                "y": 88
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a953a2e7-428e-4df5-969d-3ce64d8bc9d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 41,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 40,
                "y": 88
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "ca754b7b-b3f6-460c-92ae-8fc702954b17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 41,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 20,
                "y": 88
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "fdb7e5f8-fc38-4576-9664-e8b1d52e3bf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 41,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 147,
                "y": 45
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "526d0b3e-a26c-4862-8292-faff408acf71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 41,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 131,
                "y": 45
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "9a7c80fd-652b-4e1c-8eb8-b771669bc979",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 41,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 117,
                "y": 45
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "9e0fadba-9ce2-476a-9298-48e22e5a2e27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 41,
                "offset": 1,
                "shift": 16,
                "w": 20,
                "x": 337,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "9a8cd87a-c87b-4107-9936-c9a1c4b48a10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 41,
                "offset": 2,
                "shift": 15,
                "w": 15,
                "x": 303,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "f33a2c6d-469e-4a93-a6b0-75e52864e9e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 41,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 291,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "1cc10add-7d69-430a-941a-d29587370ba1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 41,
                "offset": -1,
                "shift": 14,
                "w": 18,
                "x": 271,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "8de5857d-6378-403e-93ef-93b393ce83d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 41,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 256,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "f726c056-def9-48ba-91ef-0922a03b6e35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 41,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "f10410f3-3969-490d-9ee4-69020874d27c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 41,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "f74802eb-6684-498f-bfdf-066369023f6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 41,
                "offset": -1,
                "shift": 16,
                "w": 17,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "b348c811-4dd3-473c-8954-04fe13c712c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 41,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "5940cd25-3381-4c02-8d3c-909f7f7a0f23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 41,
                "offset": 0,
                "shift": 17,
                "w": 18,
                "x": 161,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "aa7ea067-f619-45de-8984-afa38b95e20a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 41,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 320,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "020d4fc5-950a-4bcd-8cef-0bd3dfa50c6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 41,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "94b59df0-396a-46d8-8096-5e6da77503e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 41,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f3dcb369-e943-4e37-a545-4500f0ad88f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 41,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "16cd85b2-ae5a-4dd7-a007-598f200183c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 41,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "3666a2aa-09e8-4a57-8599-e1b0917fb44b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 41,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "2e85c046-aad9-4f9f-bc41-dce9bbd520cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 41,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "feb97f18-6c51-4353-a478-7b77b9c071eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 41,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "9aec0c5b-79fa-487d-9d4f-72fd3587c00c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 41,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "71240546-c054-407e-b7c5-47a8ce1c31b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 41,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "317bcb80-1670-4208-9b70-77b120f5622b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 41,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "bcc1e202-50fb-477c-9929-7e1b65be5416",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 41,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 131,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "03e63831-cbed-472c-988b-d133d7d2d8cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 41,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 359,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "c42cc9fb-dbc8-4817-a833-9453d13e1dcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 41,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 486,
                "y": 2
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "eed67554-ef8e-48c6-a97f-768061963894",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 41,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 370,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "01ba4869-4ba0-47f3-8fbe-03f4057ca843",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 41,
                "offset": -4,
                "shift": 8,
                "w": 10,
                "x": 95,
                "y": 45
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "22f2c250-d5e7-4030-b8eb-495ea3b36ed8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 41,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 83,
                "y": 45
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "afceea5e-da2f-43b0-8778-c63ce1551b8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 41,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 76,
                "y": 45
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "769e08bd-41a2-4f9a-b79c-a7ef74729e74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 41,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 60,
                "y": 45
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "de3382d6-bd9c-426d-8e10-480fe0e83d86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 41,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 49,
                "y": 45
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "bb5c08c8-6f77-467e-a74d-deb9d507addf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 41,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 38,
                "y": 45
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "0714de77-9414-4891-bf17-2f76ce6313fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 41,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 25,
                "y": 45
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "f95b56ce-8ecd-417d-9b21-a0a99a37019b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 41,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 12,
                "y": 45
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "bbe841c0-6cae-4668-a99f-3927950636eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 41,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 45
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "cdaee21d-abf6-43f6-a72f-4ef67754aeb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 41,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 107,
                "y": 45
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "afe1ec53-cbab-451b-99bf-71454112553c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 41,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 498,
                "y": 2
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "04197a8f-27d6-46c1-94d1-d681ef8f3e82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 41,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 473,
                "y": 2
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "b8f2aabd-0e58-4278-8b23-bdad81de55c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 41,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 462,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "4d98ef32-8dd0-4c57-bf59-b299b65c27a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 41,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 449,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "edf2b262-b7b0-44ca-8c4f-8a7fe43f6a17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 41,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 437,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "eff07458-04de-4bd3-8496-c164baefe56d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 41,
                "offset": 0,
                "shift": 13,
                "w": 11,
                "x": 424,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "65c21f05-e805-432b-bdbd-c795cc00616d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 41,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 410,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "35f5561b-24bf-4810-a57c-fe70df5d1b68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 41,
                "offset": -1,
                "shift": 12,
                "w": 12,
                "x": 396,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "fb3963e9-5dca-4bb6-b598-965f6a9973b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 41,
                "offset": 5,
                "shift": 12,
                "w": 7,
                "x": 387,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "b7d42fc3-c9b0-487c-bf17-7c4fe215c7ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 41,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 375,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "f6109384-64f2-415f-8f95-191cd64e62d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 41,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 335,
                "y": 88
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "e7cd17cf-196b-4bd9-ba2a-4afa11d9d593",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 41,
                "offset": 7,
                "shift": 34,
                "w": 20,
                "x": 349,
                "y": 88
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 26,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}