{
    "id": "17fe9e5a-8d39-4fd6-80e0-fb15d2bb3059",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fBuy",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 2,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Chiller",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "13dada43-2ea7-4bb1-8ac9-eeaec89bcffb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 42,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "7833f674-de4d-4fa2-9158-4dbbd14b28e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 42,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 504,
                "y": 46
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "678a548e-4ae0-493f-891c-89b82d23640f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 42,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 496,
                "y": 46
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "01daf038-22e9-4fbd-b26e-dbec162c8bad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 42,
                "offset": 1,
                "shift": 37,
                "w": 35,
                "x": 459,
                "y": 46
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "fc07bafc-3e82-4d1c-9e80-4dfcc72b2116",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 42,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 445,
                "y": 46
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "0db088eb-ed51-4279-9541-627f8772d1f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 42,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 426,
                "y": 46
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "5a70e16d-6dad-49e4-bb6d-16d470041589",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 42,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 407,
                "y": 46
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "c2ea370f-ec7f-4769-aa51-a6744a203b27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 42,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 401,
                "y": 46
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "970e4984-deae-4ff3-b249-0b59d9e9ef25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 42,
                "offset": 3,
                "shift": 14,
                "w": 11,
                "x": 388,
                "y": 46
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "418dba55-5f63-4172-89e3-61d139d07f60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 42,
                "offset": 0,
                "shift": 14,
                "w": 11,
                "x": 375,
                "y": 46
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "5d01ce4e-c3ee-44f5-a206-7a6847dbcdc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 42,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 90
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "fb7bddb1-cb83-44ed-9034-293b15f561b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 42,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 354,
                "y": 46
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "5d95ff53-5cc0-4d25-ae90-d6f30b7e9087",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 42,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 331,
                "y": 46
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "260516e9-9ba8-4217-9e92-b37b135234f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 42,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 318,
                "y": 46
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "cf63a0e3-9b5d-495c-af79-cf3ddf2d6b2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 42,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 311,
                "y": 46
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "6049ce97-0714-4151-a13e-1a68569342b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 42,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 292,
                "y": 46
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "2cd1f57c-0013-4f0d-b64b-11afb96f55fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 42,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 276,
                "y": 46
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "2de79566-5ad8-4439-8c46-58064869e599",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 42,
                "offset": -1,
                "shift": 11,
                "w": 10,
                "x": 264,
                "y": 46
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "cff5ba82-5ffa-4b57-944a-0a009399dbea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 42,
                "offset": -1,
                "shift": 15,
                "w": 14,
                "x": 248,
                "y": 46
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "72b65dcc-ef38-46e1-a831-61f790078391",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 42,
                "offset": -2,
                "shift": 15,
                "w": 16,
                "x": 230,
                "y": 46
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "f37400b9-386e-4479-9c01-562ce580a752",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 42,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 213,
                "y": 46
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "f0a361d7-8bf9-49ea-b29b-d770aa173a29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 42,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 338,
                "y": 46
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "d8fb650b-feb0-4004-90d6-d6156708df9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 42,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 16,
                "y": 90
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "05f2fc78-4970-4e8d-a4ae-7cdfa11dc027",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 42,
                "offset": -1,
                "shift": 15,
                "w": 15,
                "x": 30,
                "y": 90
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "728142c1-5918-44c5-9d61-ef7860449002",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 42,
                "offset": -1,
                "shift": 16,
                "w": 16,
                "x": 47,
                "y": 90
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "c8fec4cd-62eb-4b03-901d-9fa11d8892db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 42,
                "offset": -3,
                "shift": 14,
                "w": 16,
                "x": 386,
                "y": 90
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "55d30b48-3836-481c-9ecf-10e38b6a856e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 42,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 379,
                "y": 90
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "c3ed4afc-7e29-476a-9394-f8a8619cda61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 42,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 372,
                "y": 90
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "f0525117-723b-45e6-bc5e-6b2552110ac4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 42,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 356,
                "y": 90
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "3b17005b-e966-40aa-b813-9a01b098b717",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 42,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 342,
                "y": 90
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "29507e8f-ab99-4f6b-ae82-4d3b51d656be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 42,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 327,
                "y": 90
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "67c7f0e6-11d7-4cd0-b98e-3e5e74153652",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 42,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 314,
                "y": 90
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "6d6d3a3b-5061-4cc2-b3d4-369787f057f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 42,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 292,
                "y": 90
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "5bbca3a0-9b26-41e8-b87e-78ae5bbf5f9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 42,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 272,
                "y": 90
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e8cc06b0-fb2d-48c3-af09-ac6f5a0c25ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 42,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 253,
                "y": 90
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "32ddbd23-5b85-4e6b-a5e2-b6ab433a53d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 42,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 234,
                "y": 90
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b0f6b00f-1275-4692-be34-be21e8b0ff63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 42,
                "offset": -1,
                "shift": 15,
                "w": 15,
                "x": 217,
                "y": 90
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "5bead60d-2b98-40d5-8457-01bf5425a673",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 42,
                "offset": 1,
                "shift": 15,
                "w": 15,
                "x": 200,
                "y": 90
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "003bda9a-0593-4b1d-a07d-055d205e0406",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 42,
                "offset": 1,
                "shift": 12,
                "w": 13,
                "x": 185,
                "y": 90
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e86f4348-fe37-47ff-8422-a687458898aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 42,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 171,
                "y": 90
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "8014dc79-095c-4e71-9df7-f100df6249b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 42,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 157,
                "y": 90
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "3779acdc-72bb-4470-a4b1-185adee52fe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 42,
                "offset": -1,
                "shift": 12,
                "w": 14,
                "x": 141,
                "y": 90
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "c7837864-929d-49de-b6e8-75f5725af4ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 42,
                "offset": -2,
                "shift": 14,
                "w": 19,
                "x": 120,
                "y": 90
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "b2056398-6394-41b7-99d3-208dc664bad9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 42,
                "offset": 1,
                "shift": 16,
                "w": 16,
                "x": 102,
                "y": 90
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "c666594a-22a5-4b0d-8340-219291762160",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 42,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 86,
                "y": 90
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "c6eea5b0-c928-486f-8fa5-e12dbb7f5a96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 42,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 65,
                "y": 90
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "1ce5b935-d6c6-4a3e-8e1e-13ee1650060f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 42,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 194,
                "y": 46
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d8e74e9a-01e2-4ffc-85ae-3392e1c01103",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 42,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 177,
                "y": 46
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "233c34b9-c2cd-492f-b12d-7ffb22740916",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 42,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 161,
                "y": 46
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "df2f98ec-35e4-4634-a0c2-6f1b2d657746",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 42,
                "offset": 1,
                "shift": 16,
                "w": 20,
                "x": 358,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "b0cd481f-fe5e-4757-a017-28a5810654eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 42,
                "offset": 1,
                "shift": 15,
                "w": 16,
                "x": 322,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "be0d56cf-2e97-40fe-bb7f-4740eda7ab76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 42,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 309,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "c0513efc-73f3-415f-86a0-c31aaf05c418",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 42,
                "offset": -1,
                "shift": 14,
                "w": 19,
                "x": 288,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "d5b5a84a-b7b2-4e1e-b217-52c5598e87f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 42,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 272,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "ea699e1f-09ce-4d42-b5cd-8ea933262736",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 42,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 255,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "f3e32f55-525f-4c73-83cd-a81d0789e015",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 42,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "459ce94e-9039-4387-9b89-fd436703c9d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 42,
                "offset": -1,
                "shift": 16,
                "w": 17,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "a28a4463-fdf2-4cf9-b7f4-feea674cd619",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 42,
                "offset": -1,
                "shift": 14,
                "w": 17,
                "x": 194,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "88fa1ce3-5197-4926-b34c-891c749de409",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 42,
                "offset": -1,
                "shift": 17,
                "w": 20,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "c0753045-5423-4822-a699-eb21abf6a878",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 42,
                "offset": 0,
                "shift": 14,
                "w": 16,
                "x": 340,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "ea275778-4ebc-4900-8fc0-821711d7f96a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 42,
                "offset": -1,
                "shift": 17,
                "w": 18,
                "x": 152,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "73238821-3728-42fd-9209-e1d4fcc2cfe0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 42,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "ed0f6711-67df-452c-9db5-498eab508575",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 42,
                "offset": 2,
                "shift": 18,
                "w": 13,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "2af0888f-1970-457d-a0ad-a1d71b447aa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 42,
                "offset": -1,
                "shift": 18,
                "w": 19,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "8abef4be-cbf6-4538-97f4-d05ccbf242d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 42,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "4c0d5034-3ba4-43c0-b547-09760be653dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 42,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "140e30bc-b003-4739-97df-dd8bd90fbc01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 42,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "178dbb62-7db8-4001-8edf-65329d64bedf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 42,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "29feeb02-0db2-46c5-b06c-f1859ef72c27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 42,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "c4ac1e8c-de1f-4570-a603-130f95a953e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 42,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "62e389cf-80bb-46c4-89a0-8dcf83e43c20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 42,
                "offset": -1,
                "shift": 7,
                "w": 10,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "191f062f-f1eb-442a-9d48-99799f50c6fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 42,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 380,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "0ce9f5f3-9fa2-4a17-96e6-0aff21cdc147",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 42,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 15,
                "y": 46
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "649ea33d-bf51-4c76-82d0-db111ea4d1cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 42,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 392,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d89be1bf-fb81-409d-9800-2ce1bc38cd87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 42,
                "offset": -5,
                "shift": 8,
                "w": 12,
                "x": 136,
                "y": 46
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "7cddb347-95a5-42c0-97fa-0bbf851bbec4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 42,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 124,
                "y": 46
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "6814b8ed-a3a7-4315-9e9d-d3ce38eaa8df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 42,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 116,
                "y": 46
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "4cb6b619-5e28-49b9-830a-d6ac2b3df3a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 42,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 100,
                "y": 46
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "26b183a7-8f27-4ef9-8437-0c09c88ab5af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 42,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 88,
                "y": 46
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "9cb09978-a91b-4f34-bd6f-8ff8d70a2661",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 42,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 77,
                "y": 46
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "9d406505-ebed-4e01-b418-f940fb8cf653",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 42,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 64,
                "y": 46
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "33945cd8-6a1e-46cc-8de7-3479a06722ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 42,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 51,
                "y": 46
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "0752b640-d638-43db-9db2-86ef336859fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 42,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 41,
                "y": 46
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "6448f193-eec9-4c0a-a797-3087029353b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 42,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 150,
                "y": 46
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "13e52a47-ae53-4bbe-828e-01e3a6e39f2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 42,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 28,
                "y": 46
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d0f00b44-0245-428b-8803-6b7498d8546a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 42,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "5d5d6d7a-85fb-4d19-b062-da3e667c04d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 42,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 494,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "fb1954bd-1933-4a72-a938-db7d4a41bdc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 42,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 480,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "090d51ba-c9de-4873-b77a-5ed2bf8c9186",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 42,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 467,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "fc06988d-1c02-4058-bac1-0af244d27b95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 42,
                "offset": 0,
                "shift": 13,
                "w": 11,
                "x": 454,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "b257af76-e7fd-438d-bfcd-8881298771cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 42,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 439,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "0471d681-d3fe-48d1-a8da-6b0cf2e520be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 42,
                "offset": -2,
                "shift": 12,
                "w": 14,
                "x": 423,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "84fbddb8-7723-4225-9b7b-5d3ccdfd9d28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 42,
                "offset": 4,
                "shift": 12,
                "w": 9,
                "x": 412,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "314b564b-08a3-4934-a1b6-8e2586c308fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 42,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 399,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "f4fd0d10-7cf6-4463-9613-d512fea15d7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 42,
                "offset": -1,
                "shift": 13,
                "w": 14,
                "x": 404,
                "y": 90
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "22aa522e-de8b-433b-b7fe-7432c44c15ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 42,
                "offset": 7,
                "shift": 34,
                "w": 20,
                "x": 420,
                "y": 90
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 26,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}