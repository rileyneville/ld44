{
    "id": "17fe9e5a-8d39-4fd6-80e0-fb15d2bb3059",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fBuy",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Chiller",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "ee16741a-77ff-4e26-a64c-e636de1aaf64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "f395a953-c0d7-49fe-9ab9-b112f60c310c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 29,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 36,
                "y": 95
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "5680139e-7968-4433-ab1e-b4af98e882da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 29,
                "y": 95
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "d3476a1d-a989-4707-b807-29b8fbe60712",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 29,
                "offset": 0,
                "shift": 26,
                "w": 25,
                "x": 2,
                "y": 95
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "2977f4cf-8e7f-491c-9338-ea0cda475039",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 219,
                "y": 64
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "04dded54-b969-4c21-beab-8c181c68840e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 29,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 205,
                "y": 64
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "a5259869-5a79-4a8f-9e51-b6342f83209c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 29,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 191,
                "y": 64
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "2a3ae6ed-236f-4a94-8594-f4fe0acdcf29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 29,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 186,
                "y": 64
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "e320bc2a-7ef8-41cc-bb74-42c16c47cdd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 29,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 176,
                "y": 64
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "4c58ef6e-59d9-4811-a49c-56550951b407",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 166,
                "y": 64
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "83a6e81d-59e2-430a-888e-6c3db153a22d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 42,
                "y": 95
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "c7e728be-a82d-494d-8770-08a142db7ae5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 29,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 151,
                "y": 64
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "26f90cc7-ef26-436c-b762-8b6d53050756",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 29,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 132,
                "y": 64
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "3ed351db-9eb1-446b-92d7-c8bddc5ddd60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 121,
                "y": 64
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "3599c64d-de9a-4a75-bdbc-ccfe82747754",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 29,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 115,
                "y": 64
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "dde8535f-0c66-4e1b-9d59-95a20121633d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 101,
                "y": 64
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "e8295990-2922-47ed-8bc8-96c18f3821bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 88,
                "y": 64
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "abaf130f-766f-4b55-965c-379e48a3653c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 29,
                "offset": -1,
                "shift": 8,
                "w": 7,
                "x": 79,
                "y": 64
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "8fd3f379-46f5-4200-8b54-5f010a027654",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 29,
                "offset": -1,
                "shift": 10,
                "w": 10,
                "x": 67,
                "y": 64
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "312b7d6d-c077-4656-807c-2016614fea0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 29,
                "offset": -1,
                "shift": 11,
                "w": 11,
                "x": 54,
                "y": 64
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "a30040d7-4175-48b2-95dc-bbf2c46def22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 41,
                "y": 64
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "358cec94-0e54-4d4e-a201-7916126599ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 138,
                "y": 64
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "70ce2e83-6322-41e7-b5bb-8ae2131fbdb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 53,
                "y": 95
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "c4964473-ad24-4ccc-b96f-294ac83cc634",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 29,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 64,
                "y": 95
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "9bb59225-e9ef-41d2-a140-5efab5afcc42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 29,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 77,
                "y": 95
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "9b108f72-6dd8-4b27-a8de-8ac447fc97c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 29,
                "offset": -2,
                "shift": 10,
                "w": 11,
                "x": 88,
                "y": 126
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "9b726d30-8463-4533-964d-95831c9a3d29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 29,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 82,
                "y": 126
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "1a9f2b87-ef11-472f-af1b-beca2ff9578b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 29,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 76,
                "y": 126
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "07947afb-57c8-4d18-bf04-8199a76e86d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 64,
                "y": 126
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "20965d50-7ceb-42ca-a2d8-b06128ad2096",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 53,
                "y": 126
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "f0e5d507-0888-49a6-96f8-69ee04a9b0b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 29,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 42,
                "y": 126
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "8e17d994-d1b8-48d5-9885-46f189c690a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 32,
                "y": 126
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "64695e18-0caa-4e06-9fa7-c8a7c482cbbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 29,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 16,
                "y": 126
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "2d073aa1-d044-4558-af3c-201b214db6d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 126
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b52b2203-f907-43a3-a8d2-2ce5a536cb8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 230,
                "y": 95
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "f9c99299-2472-4cd5-ba38-b41c66f02976",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 29,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 216,
                "y": 95
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "f434ab57-4f2d-4511-8475-9ff94a8a3639",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 29,
                "offset": -1,
                "shift": 10,
                "w": 10,
                "x": 204,
                "y": 95
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "4976abcb-fe94-41bc-96f7-3712e631b8e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 191,
                "y": 95
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "51369168-b717-4540-be8d-2c1e91126702",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 29,
                "offset": 1,
                "shift": 8,
                "w": 9,
                "x": 180,
                "y": 95
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "d6694507-f8a0-45ba-ad1d-fa9cf189e080",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 169,
                "y": 95
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "83c89ffd-6298-4e1e-85cb-892b8c0171a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 158,
                "y": 95
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "be3a35ec-0257-49c4-af09-53a7c3bec377",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 29,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 146,
                "y": 95
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "e7209f70-57bf-42e8-ba94-c59b5b838f20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 29,
                "offset": -1,
                "shift": 9,
                "w": 13,
                "x": 131,
                "y": 95
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "7bbf1f2e-df67-45be-a153-d1bbc96b36d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 29,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 118,
                "y": 95
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a340592f-2b89-468a-bd3c-65230c3de18f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 106,
                "y": 95
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "3fbd06ed-f81f-4b63-97b8-4081559b8a55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 29,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 91,
                "y": 95
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "59839786-629d-4926-945e-117ce1a08d12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 27,
                "y": 64
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "875d3307-a487-4947-b595-7cfb7d1c4903",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 14,
                "y": 64
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "a9458a86-52d4-4738-85dd-36d1b89a66e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 2,
                "y": 64
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "78ceb788-0d85-4dc2-aaad-12db6b3a8e0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 15,
                "x": 15,
                "y": 33
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "740b9a70-bcf1-41a5-ac7e-6ba3d31d9279",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 11,
                "x": 241,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "14dad5f4-a085-449e-9f74-dc80011a6a6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "2d184bb9-3fd8-4239-ab61-bd1719c8804e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 29,
                "offset": -1,
                "shift": 9,
                "w": 13,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "79f99f9c-cd1f-42ed-80d4-f8c85c8ea71c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "077a9f49-4469-4f62-a8e9-03598a8e1ab2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 29,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "3de20c2f-72e7-4f81-9370-0aefefb9e940",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 29,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "2e172391-1d6f-4eeb-81d7-30b221fbe2c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 29,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "49b4433d-b623-4165-9a10-0503fb779920",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 29,
                "offset": -1,
                "shift": 9,
                "w": 12,
                "x": 146,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "fc95e5c3-4ffa-454e-8d01-1e9d336f8178",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 29,
                "offset": -1,
                "shift": 12,
                "w": 14,
                "x": 130,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "6a45a778-aa31-46e0-b299-828bd2e0267f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 2,
                "y": 33
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "3298dc16-9e3e-4aec-9539-cafbfc7ca999",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 29,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "37433826-4951-4d36-a5e7-fb7a6aa9e804",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 29,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "ff5cf888-b5c3-4900-85ea-a49582f04c64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "8d67a7c6-433d-4102-8baa-7bdfba70f478",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 29,
                "offset": -1,
                "shift": 12,
                "w": 14,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "5a37b0da-f8eb-4bf9-b506-f73d9ac18d88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 29,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "3f1d5fa9-ccd6-462f-9e2e-671bb234d73e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "690f2e5b-7441-48cb-81c3-cb45f4b04d44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "1569283b-9e6a-4a87-99d4-ef06a66417fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "723b73bc-8099-44d7-92fe-6adaa29d388e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "e3a112ad-6181-459f-84df-ea6c74ed9ba8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ba62603b-3ae7-48ea-afdc-532d50d5f428",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 29,
                "offset": -1,
                "shift": 5,
                "w": 7,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "1d8e6ec7-db1c-4cd1-a334-6cd312b651df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 32,
                "y": 33
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "e6804a07-3a94-4aa3-9100-fd5905081d62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 135,
                "y": 33
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "49b01bc8-fd2a-4b31-8508-4de7bff170d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 29,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 41,
                "y": 33
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "231a9508-a0fd-412c-8b9f-cc7d55e621be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 29,
                "offset": -3,
                "shift": 5,
                "w": 8,
                "x": 231,
                "y": 33
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "3a004c4e-ac21-435c-a064-04c00acbdd9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 221,
                "y": 33
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "06ee23af-3acf-4ffb-8dcb-3879417de710",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 29,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 215,
                "y": 33
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "efdc4f49-0779-4163-8a76-114258fdb2a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 202,
                "y": 33
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "891ec3b6-14dd-46f5-a724-a1ba3aeebe63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 192,
                "y": 33
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "2e643b93-cf73-44ec-aec9-5e54239da3eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 183,
                "y": 33
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "0132bd95-a278-4504-80c2-2a47c367bed0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 173,
                "y": 33
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "b9a75aa1-c459-46b7-b436-835a8e4967b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 163,
                "y": 33
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "d659355c-f59b-4d9d-af91-7f1ea702f7c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 29,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 155,
                "y": 33
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "d9b9ec6f-9274-4fdd-a9ad-ae555c149c83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 29,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 241,
                "y": 33
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "89df6b4c-ebb1-4b8e-b8d9-dec4bbca7926",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 29,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 145,
                "y": 33
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "c6219922-3119-4bf3-bb4f-fbd6154db289",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 125,
                "y": 33
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "5ae9199c-1e7a-4e34-a31e-777e5e13f578",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 29,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 116,
                "y": 33
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "9f9c49b8-8f48-434a-9608-d4738b41257c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 105,
                "y": 33
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "05f4e733-8457-4089-8e93-5f9c929de98a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 95,
                "y": 33
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "8faa4749-bff5-459b-bde6-68c7c4585bb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 85,
                "y": 33
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "91894cf9-d72e-4ceb-9ea2-46e85fedb3f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 74,
                "y": 33
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "7601425d-2cc4-40a3-a2ae-36ec02195d4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 29,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 63,
                "y": 33
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "02548311-e1ce-47cf-87cf-b3ed563ccda5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 29,
                "offset": 3,
                "shift": 8,
                "w": 6,
                "x": 55,
                "y": 33
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "0eff4d6b-a24c-4941-abcf-99e74f661242",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 46,
                "y": 33
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d1dfa105-80d3-490b-95c2-1934a263318f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 29,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 101,
                "y": 126
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "4106bdd3-6386-42e7-832d-a15d2d967d5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 29,
                "offset": 4,
                "shift": 23,
                "w": 15,
                "x": 113,
                "y": 126
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 18,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}