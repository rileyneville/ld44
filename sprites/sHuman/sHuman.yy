{
    "id": "9b07966a-eedb-4e70-9452-b3bff8f5cb4a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHuman",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 7,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a94df5a-dcfb-40fc-9946-5d456e4bc2d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b07966a-eedb-4e70-9452-b3bff8f5cb4a",
            "compositeImage": {
                "id": "5787380f-329f-45fc-821c-784e4b4561a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a94df5a-dcfb-40fc-9946-5d456e4bc2d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4822a017-0ebe-4fd3-b01d-2dad2cf9b95b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a94df5a-dcfb-40fc-9946-5d456e4bc2d2",
                    "LayerId": "0ec90f4a-a5c9-4926-b4ba-e67aa21f94ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0ec90f4a-a5c9-4926-b4ba-e67aa21f94ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9b07966a-eedb-4e70-9452-b3bff8f5cb4a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}