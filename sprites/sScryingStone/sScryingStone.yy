{
    "id": "15465a26-fb49-42c4-856a-674df0b27835",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sScryingStone",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 30,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b852e74d-d9a6-40f6-b665-61030074440f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15465a26-fb49-42c4-856a-674df0b27835",
            "compositeImage": {
                "id": "cf7525ea-589d-4ae2-bd66-a4923a25159e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b852e74d-d9a6-40f6-b665-61030074440f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f42c5f2d-fbf9-4f92-a9bb-6ec80a63ae40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b852e74d-d9a6-40f6-b665-61030074440f",
                    "LayerId": "d6ba379d-b248-4add-b93c-9e313fac1d2d"
                },
                {
                    "id": "9bcce5a5-4cd5-451f-8d96-478cd4b9e880",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b852e74d-d9a6-40f6-b665-61030074440f",
                    "LayerId": "6b02b620-5aa8-4640-a6a8-c9add26b6e90"
                },
                {
                    "id": "f8ecd599-165f-40b5-922a-69e3e85ce721",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b852e74d-d9a6-40f6-b665-61030074440f",
                    "LayerId": "ca74afce-8d2b-45c5-a08a-ebfae3052175"
                }
            ]
        },
        {
            "id": "5efd16f6-30bd-40e6-80df-7f189f26bd2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15465a26-fb49-42c4-856a-674df0b27835",
            "compositeImage": {
                "id": "8d23e7c6-a0bd-4d57-8389-44e0d630e4d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5efd16f6-30bd-40e6-80df-7f189f26bd2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2204027-e4ce-40cd-898b-dbb34645b64c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5efd16f6-30bd-40e6-80df-7f189f26bd2c",
                    "LayerId": "d6ba379d-b248-4add-b93c-9e313fac1d2d"
                },
                {
                    "id": "31b9933d-26f1-4a95-8804-b4d4abad7306",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5efd16f6-30bd-40e6-80df-7f189f26bd2c",
                    "LayerId": "6b02b620-5aa8-4640-a6a8-c9add26b6e90"
                },
                {
                    "id": "a4bbc106-0b29-461f-ae81-7e7d67b81cf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5efd16f6-30bd-40e6-80df-7f189f26bd2c",
                    "LayerId": "ca74afce-8d2b-45c5-a08a-ebfae3052175"
                }
            ]
        },
        {
            "id": "8287f30c-5c7b-4050-aff6-f4ecaeca94b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15465a26-fb49-42c4-856a-674df0b27835",
            "compositeImage": {
                "id": "2cecf34c-34cc-4933-86c7-c4bd4a08c18c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8287f30c-5c7b-4050-aff6-f4ecaeca94b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14fad401-1208-4eb3-92a2-4aea28d142bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8287f30c-5c7b-4050-aff6-f4ecaeca94b3",
                    "LayerId": "d6ba379d-b248-4add-b93c-9e313fac1d2d"
                },
                {
                    "id": "a026012a-aefd-4fd3-a4d2-ef2c7fb8ad5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8287f30c-5c7b-4050-aff6-f4ecaeca94b3",
                    "LayerId": "6b02b620-5aa8-4640-a6a8-c9add26b6e90"
                },
                {
                    "id": "68da5298-32d6-4557-b4f4-529b38d2981f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8287f30c-5c7b-4050-aff6-f4ecaeca94b3",
                    "LayerId": "ca74afce-8d2b-45c5-a08a-ebfae3052175"
                }
            ]
        },
        {
            "id": "a9c6fb6d-d4a1-4f4f-b31b-49fc24364fe8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15465a26-fb49-42c4-856a-674df0b27835",
            "compositeImage": {
                "id": "a00a897a-cf87-4398-8433-43ed656d1aa1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9c6fb6d-d4a1-4f4f-b31b-49fc24364fe8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dcf7f8b-dc98-4ac0-ae72-b2c261a8703d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9c6fb6d-d4a1-4f4f-b31b-49fc24364fe8",
                    "LayerId": "d6ba379d-b248-4add-b93c-9e313fac1d2d"
                },
                {
                    "id": "0a8f81f1-5dc9-4c53-a968-c700d77a3a22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9c6fb6d-d4a1-4f4f-b31b-49fc24364fe8",
                    "LayerId": "6b02b620-5aa8-4640-a6a8-c9add26b6e90"
                },
                {
                    "id": "97714787-d8fc-48a2-90c6-f7c2b08a0b4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9c6fb6d-d4a1-4f4f-b31b-49fc24364fe8",
                    "LayerId": "ca74afce-8d2b-45c5-a08a-ebfae3052175"
                }
            ]
        },
        {
            "id": "7ca71d7a-1b14-4691-8d49-41ac4d0e06e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15465a26-fb49-42c4-856a-674df0b27835",
            "compositeImage": {
                "id": "afc447c1-01a2-4eb0-ae8e-09c7bba3fcfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ca71d7a-1b14-4691-8d49-41ac4d0e06e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f73169f2-d526-4ea9-a042-b5e77dd3993d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ca71d7a-1b14-4691-8d49-41ac4d0e06e6",
                    "LayerId": "d6ba379d-b248-4add-b93c-9e313fac1d2d"
                },
                {
                    "id": "8b2b4d3c-892c-4574-942b-d21db6aa4d49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ca71d7a-1b14-4691-8d49-41ac4d0e06e6",
                    "LayerId": "6b02b620-5aa8-4640-a6a8-c9add26b6e90"
                },
                {
                    "id": "7086cb88-27f7-4b1f-b3a0-df14c004d344",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ca71d7a-1b14-4691-8d49-41ac4d0e06e6",
                    "LayerId": "ca74afce-8d2b-45c5-a08a-ebfae3052175"
                }
            ]
        },
        {
            "id": "2bc05b1e-83a5-4659-9c68-98dea68a0881",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15465a26-fb49-42c4-856a-674df0b27835",
            "compositeImage": {
                "id": "740e554c-7b92-4c4e-9892-cbd0c5e51bbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bc05b1e-83a5-4659-9c68-98dea68a0881",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14d8215c-7ba6-420a-a43b-e40182f0b342",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bc05b1e-83a5-4659-9c68-98dea68a0881",
                    "LayerId": "d6ba379d-b248-4add-b93c-9e313fac1d2d"
                },
                {
                    "id": "ad656e52-3d44-4e73-aa5d-f0ff5f43293a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bc05b1e-83a5-4659-9c68-98dea68a0881",
                    "LayerId": "6b02b620-5aa8-4640-a6a8-c9add26b6e90"
                },
                {
                    "id": "df2b2a14-b377-4e3c-9315-270ee007d6e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bc05b1e-83a5-4659-9c68-98dea68a0881",
                    "LayerId": "ca74afce-8d2b-45c5-a08a-ebfae3052175"
                }
            ]
        },
        {
            "id": "eae08c83-7f1c-47e8-9ceb-63aace26bdcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15465a26-fb49-42c4-856a-674df0b27835",
            "compositeImage": {
                "id": "177e89dc-1f3d-45c3-b791-6aed4f12e7a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eae08c83-7f1c-47e8-9ceb-63aace26bdcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d29517c5-62ca-435b-826f-2da6f3912798",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eae08c83-7f1c-47e8-9ceb-63aace26bdcd",
                    "LayerId": "d6ba379d-b248-4add-b93c-9e313fac1d2d"
                },
                {
                    "id": "44829781-b622-43d0-801c-cdf0ef807760",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eae08c83-7f1c-47e8-9ceb-63aace26bdcd",
                    "LayerId": "6b02b620-5aa8-4640-a6a8-c9add26b6e90"
                },
                {
                    "id": "1977bbd4-6bef-41ea-8bdd-916e0f50dca9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eae08c83-7f1c-47e8-9ceb-63aace26bdcd",
                    "LayerId": "ca74afce-8d2b-45c5-a08a-ebfae3052175"
                }
            ]
        },
        {
            "id": "843bfaae-4804-400a-82fd-0d4aa1b414e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15465a26-fb49-42c4-856a-674df0b27835",
            "compositeImage": {
                "id": "5c51e8da-b907-4eb9-81a3-81919b5b8004",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "843bfaae-4804-400a-82fd-0d4aa1b414e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cd7cf35-48ec-44e0-bd4a-62bbf5555868",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "843bfaae-4804-400a-82fd-0d4aa1b414e5",
                    "LayerId": "d6ba379d-b248-4add-b93c-9e313fac1d2d"
                },
                {
                    "id": "f1b72140-d8dd-4b21-87a7-82613fb99968",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "843bfaae-4804-400a-82fd-0d4aa1b414e5",
                    "LayerId": "6b02b620-5aa8-4640-a6a8-c9add26b6e90"
                },
                {
                    "id": "6e370712-b83c-46c2-8877-776f9d995ab9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "843bfaae-4804-400a-82fd-0d4aa1b414e5",
                    "LayerId": "ca74afce-8d2b-45c5-a08a-ebfae3052175"
                }
            ]
        },
        {
            "id": "1bed44a9-cc95-4d86-9953-61c576e20c1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15465a26-fb49-42c4-856a-674df0b27835",
            "compositeImage": {
                "id": "f4c653a1-79ce-4124-83bb-e03ce3c57ca0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bed44a9-cc95-4d86-9953-61c576e20c1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f48baa16-2e98-4f9a-963a-6036d7cadffa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bed44a9-cc95-4d86-9953-61c576e20c1f",
                    "LayerId": "d6ba379d-b248-4add-b93c-9e313fac1d2d"
                },
                {
                    "id": "b4ab2079-17de-4e1c-bfd3-a17b82436d97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bed44a9-cc95-4d86-9953-61c576e20c1f",
                    "LayerId": "6b02b620-5aa8-4640-a6a8-c9add26b6e90"
                },
                {
                    "id": "23efadf6-a430-47a7-9358-398c9f9e17ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bed44a9-cc95-4d86-9953-61c576e20c1f",
                    "LayerId": "ca74afce-8d2b-45c5-a08a-ebfae3052175"
                }
            ]
        },
        {
            "id": "37a853a4-ff88-4617-8c28-4ab6d41d9311",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15465a26-fb49-42c4-856a-674df0b27835",
            "compositeImage": {
                "id": "f67f5524-dbc3-4b2e-9d66-a23ae1a42e83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37a853a4-ff88-4617-8c28-4ab6d41d9311",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03f1613f-7d57-40a4-94a5-c66e1452220c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37a853a4-ff88-4617-8c28-4ab6d41d9311",
                    "LayerId": "d6ba379d-b248-4add-b93c-9e313fac1d2d"
                },
                {
                    "id": "a63a7478-0e2d-4fb2-971d-decde7dd12ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37a853a4-ff88-4617-8c28-4ab6d41d9311",
                    "LayerId": "6b02b620-5aa8-4640-a6a8-c9add26b6e90"
                },
                {
                    "id": "d470386a-12d4-43de-bd03-300026462a5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37a853a4-ff88-4617-8c28-4ab6d41d9311",
                    "LayerId": "ca74afce-8d2b-45c5-a08a-ebfae3052175"
                }
            ]
        },
        {
            "id": "021acc50-20c4-4cc9-97a9-a0d671bab019",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15465a26-fb49-42c4-856a-674df0b27835",
            "compositeImage": {
                "id": "d581ff20-b03c-4110-b833-61f36ea3fed0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "021acc50-20c4-4cc9-97a9-a0d671bab019",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "531112e4-da7b-48bb-b6be-caedca5e43e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "021acc50-20c4-4cc9-97a9-a0d671bab019",
                    "LayerId": "d6ba379d-b248-4add-b93c-9e313fac1d2d"
                },
                {
                    "id": "8c82a5ee-9f20-4811-a5d2-aac5855b65d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "021acc50-20c4-4cc9-97a9-a0d671bab019",
                    "LayerId": "6b02b620-5aa8-4640-a6a8-c9add26b6e90"
                },
                {
                    "id": "f4fdf8b6-7c65-48d0-b7d8-9930e0260415",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "021acc50-20c4-4cc9-97a9-a0d671bab019",
                    "LayerId": "ca74afce-8d2b-45c5-a08a-ebfae3052175"
                }
            ]
        },
        {
            "id": "3626cf53-6000-42f5-92b4-03c37528b66c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15465a26-fb49-42c4-856a-674df0b27835",
            "compositeImage": {
                "id": "5ec7aa78-599d-4dac-80ff-5b302fc34560",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3626cf53-6000-42f5-92b4-03c37528b66c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25f1de69-5804-458f-8eb2-400d1bd59fb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3626cf53-6000-42f5-92b4-03c37528b66c",
                    "LayerId": "d6ba379d-b248-4add-b93c-9e313fac1d2d"
                },
                {
                    "id": "ef2f3b8b-9a7a-474d-92e2-0524f5473d19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3626cf53-6000-42f5-92b4-03c37528b66c",
                    "LayerId": "6b02b620-5aa8-4640-a6a8-c9add26b6e90"
                },
                {
                    "id": "177aa5ce-2bc8-4406-a870-e08ca058ae0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3626cf53-6000-42f5-92b4-03c37528b66c",
                    "LayerId": "ca74afce-8d2b-45c5-a08a-ebfae3052175"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6b02b620-5aa8-4640-a6a8-c9add26b6e90",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "15465a26-fb49-42c4-856a-674df0b27835",
            "blendMode": 0,
            "isLocked": false,
            "name": "default (2)",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "ca74afce-8d2b-45c5-a08a-ebfae3052175",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "15465a26-fb49-42c4-856a-674df0b27835",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "d6ba379d-b248-4add-b93c-9e313fac1d2d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "15465a26-fb49-42c4-856a-674df0b27835",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}