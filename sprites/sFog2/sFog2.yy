{
    "id": "70614904-8bd1-4eee-b47f-2a3a34d9a640",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFog2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 561,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 494,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d564f08-2e43-4121-8903-c1fa039c2e22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70614904-8bd1-4eee-b47f-2a3a34d9a640",
            "compositeImage": {
                "id": "8672e325-7f99-43f0-a565-69b3081ebf86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d564f08-2e43-4121-8903-c1fa039c2e22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4e3f1ca-bda0-48ac-9ac0-344dedd36fe4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d564f08-2e43-4121-8903-c1fa039c2e22",
                    "LayerId": "4a220ba9-21b6-4b01-95cd-e03f35b217fd"
                },
                {
                    "id": "2ddf3616-50c3-4076-ae65-ca211d8b9be9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d564f08-2e43-4121-8903-c1fa039c2e22",
                    "LayerId": "813b2c9d-dd7f-44fd-a6da-48d24ab3a2d7"
                },
                {
                    "id": "c771b251-f3a6-4f1e-873a-88ecc693e134",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d564f08-2e43-4121-8903-c1fa039c2e22",
                    "LayerId": "3b2f3cd1-586e-46be-aeea-70dc10a8cb41"
                },
                {
                    "id": "a9985ed2-c066-4c9f-83ee-82e7f638e69b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d564f08-2e43-4121-8903-c1fa039c2e22",
                    "LayerId": "0cee08e0-c061-42c9-ae59-41d7214634f6"
                },
                {
                    "id": "597fa5a3-ff8c-4f7f-9565-12dd6ffa48c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d564f08-2e43-4121-8903-c1fa039c2e22",
                    "LayerId": "71a9158f-b623-49d8-9038-9ea5f8a541c5"
                },
                {
                    "id": "b8fdc224-ed04-4206-b8a4-26fd6b9bca01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d564f08-2e43-4121-8903-c1fa039c2e22",
                    "LayerId": "52df589d-c89c-4ba3-87b3-b7312b892c7f"
                },
                {
                    "id": "6dfb7dac-e1a9-4302-86d6-b337ab72e48b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d564f08-2e43-4121-8903-c1fa039c2e22",
                    "LayerId": "69e5b6d9-791f-4347-949c-c7ce71ca4be3"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 600,
    "layers": [
        {
            "id": "4a220ba9-21b6-4b01-95cd-e03f35b217fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70614904-8bd1-4eee-b47f-2a3a34d9a640",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "813b2c9d-dd7f-44fd-a6da-48d24ab3a2d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70614904-8bd1-4eee-b47f-2a3a34d9a640",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 5",
            "opacity": 15,
            "visible": true
        },
        {
            "id": "3b2f3cd1-586e-46be-aeea-70dc10a8cb41",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70614904-8bd1-4eee-b47f-2a3a34d9a640",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "0cee08e0-c061-42c9-ae59-41d7214634f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70614904-8bd1-4eee-b47f-2a3a34d9a640",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 80,
            "visible": false
        },
        {
            "id": "71a9158f-b623-49d8-9038-9ea5f8a541c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70614904-8bd1-4eee-b47f-2a3a34d9a640",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 4 (2)",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "52df589d-c89c-4ba3-87b3-b7312b892c7f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70614904-8bd1-4eee-b47f-2a3a34d9a640",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 4",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "69e5b6d9-791f-4347-949c-c7ce71ca4be3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70614904-8bd1-4eee-b47f-2a3a34d9a640",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}