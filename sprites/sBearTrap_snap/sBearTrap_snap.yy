{
    "id": "cd84fb77-8676-405e-9e00-2add3f1f5dd1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBearTrap_snap",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f31f623-1fb0-4173-a5fb-6577e40cf4f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd84fb77-8676-405e-9e00-2add3f1f5dd1",
            "compositeImage": {
                "id": "9f1b4193-f830-42f7-8c03-3e1fdfba3f4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f31f623-1fb0-4173-a5fb-6577e40cf4f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "843714a3-08e9-4eb8-8121-261cdb9b68d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f31f623-1fb0-4173-a5fb-6577e40cf4f0",
                    "LayerId": "f394e0cd-bc3a-4594-8ffd-99ee5059eadb"
                },
                {
                    "id": "5f01639f-f694-4476-9a94-0483fbd9088f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f31f623-1fb0-4173-a5fb-6577e40cf4f0",
                    "LayerId": "9302e770-61b0-45d0-944a-eed3ab6b544f"
                },
                {
                    "id": "869eecef-26a3-4942-af53-d388229d0fb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f31f623-1fb0-4173-a5fb-6577e40cf4f0",
                    "LayerId": "17aef507-90dc-40a3-a49a-c932a816667b"
                }
            ]
        },
        {
            "id": "9ebebcc0-f333-49ae-b250-cc6478f084d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd84fb77-8676-405e-9e00-2add3f1f5dd1",
            "compositeImage": {
                "id": "b6462f7b-04f5-4843-ae06-17dac9123e89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ebebcc0-f333-49ae-b250-cc6478f084d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "257a74fc-783f-427b-a022-3924377cfa73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ebebcc0-f333-49ae-b250-cc6478f084d9",
                    "LayerId": "f394e0cd-bc3a-4594-8ffd-99ee5059eadb"
                },
                {
                    "id": "5b182e4a-2303-4f7e-a607-0a0b3aa50f05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ebebcc0-f333-49ae-b250-cc6478f084d9",
                    "LayerId": "9302e770-61b0-45d0-944a-eed3ab6b544f"
                },
                {
                    "id": "411a2ca1-1fbe-4c7d-b7c4-1a62a17809b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ebebcc0-f333-49ae-b250-cc6478f084d9",
                    "LayerId": "17aef507-90dc-40a3-a49a-c932a816667b"
                }
            ]
        },
        {
            "id": "9bc1fe7d-556e-405a-9536-69dd026bd65b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd84fb77-8676-405e-9e00-2add3f1f5dd1",
            "compositeImage": {
                "id": "3e918e90-8a05-48a8-ab4d-a95668d58cd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bc1fe7d-556e-405a-9536-69dd026bd65b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b826f304-741b-4c02-96bc-06331e93220b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bc1fe7d-556e-405a-9536-69dd026bd65b",
                    "LayerId": "f394e0cd-bc3a-4594-8ffd-99ee5059eadb"
                },
                {
                    "id": "dbfe4b84-ae46-4a2f-8100-6a5e2f414f44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bc1fe7d-556e-405a-9536-69dd026bd65b",
                    "LayerId": "9302e770-61b0-45d0-944a-eed3ab6b544f"
                },
                {
                    "id": "95050004-82cf-4f29-99f7-4e2984e3d0c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bc1fe7d-556e-405a-9536-69dd026bd65b",
                    "LayerId": "17aef507-90dc-40a3-a49a-c932a816667b"
                }
            ]
        },
        {
            "id": "ae5b9a84-98e3-4da8-a3e0-cff1c646b000",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd84fb77-8676-405e-9e00-2add3f1f5dd1",
            "compositeImage": {
                "id": "7694dd46-f0ce-47f8-95a8-f20b8e562f53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae5b9a84-98e3-4da8-a3e0-cff1c646b000",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5aecb4ee-ab2c-414c-b4e7-a4496424d8d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae5b9a84-98e3-4da8-a3e0-cff1c646b000",
                    "LayerId": "f394e0cd-bc3a-4594-8ffd-99ee5059eadb"
                },
                {
                    "id": "35b4600f-d6a0-4c12-8ad9-41b17a1111ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae5b9a84-98e3-4da8-a3e0-cff1c646b000",
                    "LayerId": "9302e770-61b0-45d0-944a-eed3ab6b544f"
                },
                {
                    "id": "54ebab7b-41eb-4228-a60a-ed286f254e94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae5b9a84-98e3-4da8-a3e0-cff1c646b000",
                    "LayerId": "17aef507-90dc-40a3-a49a-c932a816667b"
                }
            ]
        },
        {
            "id": "f84b7531-c985-41f3-83e7-6aa5e7e143e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd84fb77-8676-405e-9e00-2add3f1f5dd1",
            "compositeImage": {
                "id": "c98c4d71-5342-4d8a-850e-990904fb4e70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f84b7531-c985-41f3-83e7-6aa5e7e143e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbb6b717-bf99-4fd4-9226-db5caada5960",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f84b7531-c985-41f3-83e7-6aa5e7e143e3",
                    "LayerId": "f394e0cd-bc3a-4594-8ffd-99ee5059eadb"
                },
                {
                    "id": "830054c8-ff18-4130-ad43-eb42c7612bc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f84b7531-c985-41f3-83e7-6aa5e7e143e3",
                    "LayerId": "9302e770-61b0-45d0-944a-eed3ab6b544f"
                },
                {
                    "id": "c74eddc2-e27e-46b7-b1cb-73ccc6886beb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f84b7531-c985-41f3-83e7-6aa5e7e143e3",
                    "LayerId": "17aef507-90dc-40a3-a49a-c932a816667b"
                }
            ]
        },
        {
            "id": "2993740e-0b1b-4e32-9686-8077af589c84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd84fb77-8676-405e-9e00-2add3f1f5dd1",
            "compositeImage": {
                "id": "432044b1-d4aa-4c8a-9f23-47c4d1b1e104",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2993740e-0b1b-4e32-9686-8077af589c84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "114d6030-752e-43d6-9d0c-f624659e2d02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2993740e-0b1b-4e32-9686-8077af589c84",
                    "LayerId": "f394e0cd-bc3a-4594-8ffd-99ee5059eadb"
                },
                {
                    "id": "a09774b0-30c0-4e69-84c5-b13ecd45c3f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2993740e-0b1b-4e32-9686-8077af589c84",
                    "LayerId": "9302e770-61b0-45d0-944a-eed3ab6b544f"
                },
                {
                    "id": "d3282c97-b81f-4bd5-bcc8-41363c67e072",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2993740e-0b1b-4e32-9686-8077af589c84",
                    "LayerId": "17aef507-90dc-40a3-a49a-c932a816667b"
                }
            ]
        },
        {
            "id": "b9b944bd-802b-4ce0-8161-54f38ff285bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd84fb77-8676-405e-9e00-2add3f1f5dd1",
            "compositeImage": {
                "id": "eb3fbd32-159b-46cf-97a2-52df4b9118c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9b944bd-802b-4ce0-8161-54f38ff285bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "605e7e64-25ce-4623-ba4a-fa5aec2e0152",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9b944bd-802b-4ce0-8161-54f38ff285bb",
                    "LayerId": "9302e770-61b0-45d0-944a-eed3ab6b544f"
                },
                {
                    "id": "8c106bac-a238-4baf-b573-819bf1f792e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9b944bd-802b-4ce0-8161-54f38ff285bb",
                    "LayerId": "f394e0cd-bc3a-4594-8ffd-99ee5059eadb"
                },
                {
                    "id": "94453336-4f7c-4cfe-82c8-9ce01e793eae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9b944bd-802b-4ce0-8161-54f38ff285bb",
                    "LayerId": "17aef507-90dc-40a3-a49a-c932a816667b"
                }
            ]
        },
        {
            "id": "95d3b4bd-56d5-491c-8dc3-294eee2d4023",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd84fb77-8676-405e-9e00-2add3f1f5dd1",
            "compositeImage": {
                "id": "60bc45f3-4250-416b-8a03-23683967a6a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95d3b4bd-56d5-491c-8dc3-294eee2d4023",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99433957-ed4a-4731-8c44-e043fe114599",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95d3b4bd-56d5-491c-8dc3-294eee2d4023",
                    "LayerId": "9302e770-61b0-45d0-944a-eed3ab6b544f"
                },
                {
                    "id": "af4099f7-5f06-4518-9a91-b92a2d4f7930",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95d3b4bd-56d5-491c-8dc3-294eee2d4023",
                    "LayerId": "f394e0cd-bc3a-4594-8ffd-99ee5059eadb"
                },
                {
                    "id": "9807433c-b98e-45c4-a8c1-01b21a0b5481",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95d3b4bd-56d5-491c-8dc3-294eee2d4023",
                    "LayerId": "17aef507-90dc-40a3-a49a-c932a816667b"
                }
            ]
        },
        {
            "id": "4da5e353-d177-4784-9bf7-8e05566ddf1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd84fb77-8676-405e-9e00-2add3f1f5dd1",
            "compositeImage": {
                "id": "984bb05e-edcb-4037-8a70-b5793573be07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4da5e353-d177-4784-9bf7-8e05566ddf1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaf024e9-e92a-4ae7-8530-3704c21c933f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4da5e353-d177-4784-9bf7-8e05566ddf1d",
                    "LayerId": "9302e770-61b0-45d0-944a-eed3ab6b544f"
                },
                {
                    "id": "3b001cac-6023-4858-bd6a-bb4bc57863bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4da5e353-d177-4784-9bf7-8e05566ddf1d",
                    "LayerId": "f394e0cd-bc3a-4594-8ffd-99ee5059eadb"
                },
                {
                    "id": "8e0e5eb7-b13a-4bcf-b8bb-33246572980c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4da5e353-d177-4784-9bf7-8e05566ddf1d",
                    "LayerId": "17aef507-90dc-40a3-a49a-c932a816667b"
                }
            ]
        },
        {
            "id": "0fa06a0b-fb3a-4bdd-bccc-258a00224799",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd84fb77-8676-405e-9e00-2add3f1f5dd1",
            "compositeImage": {
                "id": "b0721c4d-5fc8-4077-934b-34fef931497b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fa06a0b-fb3a-4bdd-bccc-258a00224799",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fe6f7f7-ccbe-424e-ae58-45bc71f1430a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fa06a0b-fb3a-4bdd-bccc-258a00224799",
                    "LayerId": "9302e770-61b0-45d0-944a-eed3ab6b544f"
                },
                {
                    "id": "76ff299d-1e83-48b1-82f7-56887676044b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fa06a0b-fb3a-4bdd-bccc-258a00224799",
                    "LayerId": "f394e0cd-bc3a-4594-8ffd-99ee5059eadb"
                },
                {
                    "id": "211fc64f-906e-479c-a2b5-9e317cc9e03b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fa06a0b-fb3a-4bdd-bccc-258a00224799",
                    "LayerId": "17aef507-90dc-40a3-a49a-c932a816667b"
                }
            ]
        },
        {
            "id": "244ae37b-f94e-4169-9b59-2b28c2fde2b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd84fb77-8676-405e-9e00-2add3f1f5dd1",
            "compositeImage": {
                "id": "1a14cc1d-b6c9-4374-b759-71a966496349",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "244ae37b-f94e-4169-9b59-2b28c2fde2b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a078593a-4bbf-4227-aba3-a0904dd29930",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "244ae37b-f94e-4169-9b59-2b28c2fde2b7",
                    "LayerId": "9302e770-61b0-45d0-944a-eed3ab6b544f"
                },
                {
                    "id": "9a6dbe68-a6a0-4f0f-b1d3-73ddfb4a9371",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "244ae37b-f94e-4169-9b59-2b28c2fde2b7",
                    "LayerId": "f394e0cd-bc3a-4594-8ffd-99ee5059eadb"
                },
                {
                    "id": "4d0c2c0a-2b6e-47aa-bd53-465eb5121b4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "244ae37b-f94e-4169-9b59-2b28c2fde2b7",
                    "LayerId": "17aef507-90dc-40a3-a49a-c932a816667b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9302e770-61b0-45d0-944a-eed3ab6b544f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd84fb77-8676-405e-9e00-2add3f1f5dd1",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "f394e0cd-bc3a-4594-8ffd-99ee5059eadb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd84fb77-8676-405e-9e00-2add3f1f5dd1",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1 (2)",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "17aef507-90dc-40a3-a49a-c932a816667b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd84fb77-8676-405e-9e00-2add3f1f5dd1",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4278780159,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}