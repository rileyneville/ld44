{
    "id": "d978389c-3670-41d6-b92f-57dc3bc2dca3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHuman_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0571adb4-d6e1-424f-af8f-8918d9859a96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d978389c-3670-41d6-b92f-57dc3bc2dca3",
            "compositeImage": {
                "id": "9293f3ae-e91b-43e3-b528-2bc40df5aa47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0571adb4-d6e1-424f-af8f-8918d9859a96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ff5dea5-7986-4455-8ec6-93147dd99365",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0571adb4-d6e1-424f-af8f-8918d9859a96",
                    "LayerId": "48737878-3461-4bd3-8197-f9a7cbc7a99b"
                }
            ]
        },
        {
            "id": "ffa2aac7-8f84-4d26-9de6-55331577a0b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d978389c-3670-41d6-b92f-57dc3bc2dca3",
            "compositeImage": {
                "id": "9bf12f0c-6d85-4d7e-a54a-c237688d6f45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffa2aac7-8f84-4d26-9de6-55331577a0b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f19f047d-7137-44a6-a17f-d9e29d45ffb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffa2aac7-8f84-4d26-9de6-55331577a0b5",
                    "LayerId": "48737878-3461-4bd3-8197-f9a7cbc7a99b"
                }
            ]
        },
        {
            "id": "18c5b104-3fe3-4847-907a-5a274f3d85ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d978389c-3670-41d6-b92f-57dc3bc2dca3",
            "compositeImage": {
                "id": "dcbc2fff-9572-48a9-9ba5-5f5e5875ef9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18c5b104-3fe3-4847-907a-5a274f3d85ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d03e1bf-2fda-44c3-9ed1-627a63686063",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18c5b104-3fe3-4847-907a-5a274f3d85ed",
                    "LayerId": "48737878-3461-4bd3-8197-f9a7cbc7a99b"
                }
            ]
        },
        {
            "id": "ee45916b-6e2f-466d-a430-e2bc740bb6e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d978389c-3670-41d6-b92f-57dc3bc2dca3",
            "compositeImage": {
                "id": "82f41da1-4529-403f-b0ff-a08f3f92d776",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee45916b-6e2f-466d-a430-e2bc740bb6e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "044e0b89-f372-45cf-a663-de9d3a456207",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee45916b-6e2f-466d-a430-e2bc740bb6e6",
                    "LayerId": "48737878-3461-4bd3-8197-f9a7cbc7a99b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "48737878-3461-4bd3-8197-f9a7cbc7a99b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d978389c-3670-41d6-b92f-57dc3bc2dca3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}