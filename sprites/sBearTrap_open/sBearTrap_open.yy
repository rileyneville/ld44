{
    "id": "224ae061-7565-49d3-999f-1f417f3055cd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBearTrap_open",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 13,
    "bbox_right": 18,
    "bbox_top": 14,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b427b34f-dece-4c6a-932b-4cf1f634e20a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "224ae061-7565-49d3-999f-1f417f3055cd",
            "compositeImage": {
                "id": "f7783e8e-388d-4f61-9854-11d5a91d1970",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b427b34f-dece-4c6a-932b-4cf1f634e20a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cf284f0-cb11-46ff-b20d-15cf38446dfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b427b34f-dece-4c6a-932b-4cf1f634e20a",
                    "LayerId": "cca3b530-c510-4795-b59e-73b025a3acce"
                },
                {
                    "id": "a4cca9ba-dbc5-498b-9154-8b18fd8d4136",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b427b34f-dece-4c6a-932b-4cf1f634e20a",
                    "LayerId": "7b328f0c-5428-4991-ab46-e549fc4d9ffc"
                },
                {
                    "id": "9df3393a-dbd4-4fca-acf7-967f19772d11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b427b34f-dece-4c6a-932b-4cf1f634e20a",
                    "LayerId": "1768e454-0e62-4326-bfc7-498c110810e6"
                }
            ]
        },
        {
            "id": "3515cae9-f25b-4b2b-9c01-3521993ed221",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "224ae061-7565-49d3-999f-1f417f3055cd",
            "compositeImage": {
                "id": "c8658fd0-1af3-4250-89ac-af9f2b4124fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3515cae9-f25b-4b2b-9c01-3521993ed221",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3560ebd-97bb-4204-9d10-14906e1265cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3515cae9-f25b-4b2b-9c01-3521993ed221",
                    "LayerId": "cca3b530-c510-4795-b59e-73b025a3acce"
                },
                {
                    "id": "24afa8f4-ede3-40c5-9a80-fab59ef2bf7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3515cae9-f25b-4b2b-9c01-3521993ed221",
                    "LayerId": "7b328f0c-5428-4991-ab46-e549fc4d9ffc"
                },
                {
                    "id": "711de50a-90b3-4d89-9083-295c29dd69c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3515cae9-f25b-4b2b-9c01-3521993ed221",
                    "LayerId": "1768e454-0e62-4326-bfc7-498c110810e6"
                }
            ]
        },
        {
            "id": "128962ad-bb5c-4203-bf0f-c9dfd38dd24c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "224ae061-7565-49d3-999f-1f417f3055cd",
            "compositeImage": {
                "id": "55c56cfc-eb4e-4b1a-9fe8-d421500d6218",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "128962ad-bb5c-4203-bf0f-c9dfd38dd24c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b80b8ef2-787f-4aed-a4d4-60983d1c451a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "128962ad-bb5c-4203-bf0f-c9dfd38dd24c",
                    "LayerId": "cca3b530-c510-4795-b59e-73b025a3acce"
                },
                {
                    "id": "b60466f6-7708-403f-8e97-a19dce5b5a1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "128962ad-bb5c-4203-bf0f-c9dfd38dd24c",
                    "LayerId": "7b328f0c-5428-4991-ab46-e549fc4d9ffc"
                },
                {
                    "id": "147a9105-a6e2-4561-9bf1-4f08fe01d0af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "128962ad-bb5c-4203-bf0f-c9dfd38dd24c",
                    "LayerId": "1768e454-0e62-4326-bfc7-498c110810e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cca3b530-c510-4795-b59e-73b025a3acce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "224ae061-7565-49d3-999f-1f417f3055cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1 (2)",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "7b328f0c-5428-4991-ab46-e549fc4d9ffc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "224ae061-7565-49d3-999f-1f417f3055cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "1768e454-0e62-4326-bfc7-498c110810e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "224ae061-7565-49d3-999f-1f417f3055cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}