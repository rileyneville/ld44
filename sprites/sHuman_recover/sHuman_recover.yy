{
    "id": "5ed72cad-74d0-4f62-bfbb-1d759a86ecc6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHuman_recover",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a821f92-567e-4424-ba3c-de54319c985e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ed72cad-74d0-4f62-bfbb-1d759a86ecc6",
            "compositeImage": {
                "id": "0cf7d0e3-8af1-4e98-9ae7-92dc90df8a50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a821f92-567e-4424-ba3c-de54319c985e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7b13ba2-b664-44ba-97ba-63c4a87b2090",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a821f92-567e-4424-ba3c-de54319c985e",
                    "LayerId": "483fee6a-df3d-4e37-8abf-045ab53e39b9"
                }
            ]
        },
        {
            "id": "97b552e9-02c6-468d-8147-b2c5db2b122e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ed72cad-74d0-4f62-bfbb-1d759a86ecc6",
            "compositeImage": {
                "id": "360b8c2e-13cc-48cb-aba6-fc22d78a7967",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97b552e9-02c6-468d-8147-b2c5db2b122e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24df9f57-aeca-45f8-971a-dc726d20e6a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97b552e9-02c6-468d-8147-b2c5db2b122e",
                    "LayerId": "483fee6a-df3d-4e37-8abf-045ab53e39b9"
                }
            ]
        },
        {
            "id": "9b2896a8-d340-4143-b2be-382bd702b5d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ed72cad-74d0-4f62-bfbb-1d759a86ecc6",
            "compositeImage": {
                "id": "d55ce88c-8a53-4b50-bc34-9ae9262fef71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b2896a8-d340-4143-b2be-382bd702b5d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92a77688-eaae-4f54-b621-c5c125fec69c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b2896a8-d340-4143-b2be-382bd702b5d5",
                    "LayerId": "483fee6a-df3d-4e37-8abf-045ab53e39b9"
                }
            ]
        },
        {
            "id": "e8bd5654-b2c0-4e8d-b86f-fb3dfb036247",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ed72cad-74d0-4f62-bfbb-1d759a86ecc6",
            "compositeImage": {
                "id": "3e0f25a8-9175-41f7-91de-73996d494551",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8bd5654-b2c0-4e8d-b86f-fb3dfb036247",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f47c585-60f3-456d-a53c-e18cb4aeef5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8bd5654-b2c0-4e8d-b86f-fb3dfb036247",
                    "LayerId": "483fee6a-df3d-4e37-8abf-045ab53e39b9"
                }
            ]
        },
        {
            "id": "d339f797-fac3-41d6-993e-15c8aac6dbbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ed72cad-74d0-4f62-bfbb-1d759a86ecc6",
            "compositeImage": {
                "id": "17234177-703b-45a0-b147-8dffdf3b64c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d339f797-fac3-41d6-993e-15c8aac6dbbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbf6d59c-cf12-4a74-a54c-1b0e38dd824e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d339f797-fac3-41d6-993e-15c8aac6dbbd",
                    "LayerId": "483fee6a-df3d-4e37-8abf-045ab53e39b9"
                }
            ]
        },
        {
            "id": "5649f699-c206-408b-9d47-e533a084287a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ed72cad-74d0-4f62-bfbb-1d759a86ecc6",
            "compositeImage": {
                "id": "00b807f1-283b-45d1-a5d1-bbb8e1a984b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5649f699-c206-408b-9d47-e533a084287a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38eb672b-b26c-44ea-b3e7-917222707d0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5649f699-c206-408b-9d47-e533a084287a",
                    "LayerId": "483fee6a-df3d-4e37-8abf-045ab53e39b9"
                }
            ]
        },
        {
            "id": "f4884b44-3f1e-46e3-a93e-cd1d6c5eab70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ed72cad-74d0-4f62-bfbb-1d759a86ecc6",
            "compositeImage": {
                "id": "46f79f62-3f1c-4baf-9b75-5d0a48a4491e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4884b44-3f1e-46e3-a93e-cd1d6c5eab70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbb85f5c-f753-4946-8786-b89b6240db53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4884b44-3f1e-46e3-a93e-cd1d6c5eab70",
                    "LayerId": "483fee6a-df3d-4e37-8abf-045ab53e39b9"
                }
            ]
        },
        {
            "id": "f5d876a6-6323-4622-b305-06451962a6d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ed72cad-74d0-4f62-bfbb-1d759a86ecc6",
            "compositeImage": {
                "id": "26bce1ee-5b63-4560-80e4-00b085fdd36f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5d876a6-6323-4622-b305-06451962a6d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44515069-288b-4a60-8dba-055351380e08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5d876a6-6323-4622-b305-06451962a6d3",
                    "LayerId": "483fee6a-df3d-4e37-8abf-045ab53e39b9"
                }
            ]
        },
        {
            "id": "89be3558-ff52-4d2b-a089-5dc7f92e0eff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ed72cad-74d0-4f62-bfbb-1d759a86ecc6",
            "compositeImage": {
                "id": "f1a61c1a-1c35-4ab4-aa91-4cf4e1ffbb9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89be3558-ff52-4d2b-a089-5dc7f92e0eff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "deaee6b3-8b39-4714-b024-333645c2aba4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89be3558-ff52-4d2b-a089-5dc7f92e0eff",
                    "LayerId": "483fee6a-df3d-4e37-8abf-045ab53e39b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "483fee6a-df3d-4e37-8abf-045ab53e39b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ed72cad-74d0-4f62-bfbb-1d759a86ecc6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}