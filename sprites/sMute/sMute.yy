{
    "id": "0ebb8328-bf6d-4d55-bebc-716885ed5fdc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMute",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 3,
    "bbox_right": 30,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c977e71-8520-403c-b84e-48a6de798126",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ebb8328-bf6d-4d55-bebc-716885ed5fdc",
            "compositeImage": {
                "id": "f19f2f4f-6acf-4391-9bf3-1351537322d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c977e71-8520-403c-b84e-48a6de798126",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84c6407a-08ba-4cbc-a091-2cb20977f81b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c977e71-8520-403c-b84e-48a6de798126",
                    "LayerId": "17c134a3-e3c4-4beb-b57d-5767108190e0"
                }
            ]
        },
        {
            "id": "3a22b5a1-bb73-459b-b9e3-b7da4872ff0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ebb8328-bf6d-4d55-bebc-716885ed5fdc",
            "compositeImage": {
                "id": "6d93cf13-98e2-43a1-bd6f-0e8f95323c6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a22b5a1-bb73-459b-b9e3-b7da4872ff0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f85f73ea-46e8-48e1-a2ab-5195715abe09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a22b5a1-bb73-459b-b9e3-b7da4872ff0e",
                    "LayerId": "17c134a3-e3c4-4beb-b57d-5767108190e0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "17c134a3-e3c4-4beb-b57d-5767108190e0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ebb8328-bf6d-4d55-bebc-716885ed5fdc",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}