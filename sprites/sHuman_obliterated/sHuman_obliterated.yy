{
    "id": "ed442927-17ce-4551-b540-ad9c246b9d39",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHuman_obliterated",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 7,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fa82a3d7-bda6-4f56-b830-48e8d2061560",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed442927-17ce-4551-b540-ad9c246b9d39",
            "compositeImage": {
                "id": "aac91de5-1589-47b5-989d-2c5fff80a46b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa82a3d7-bda6-4f56-b830-48e8d2061560",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c5b5937-29b1-476c-9fd8-6e3b040b5644",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa82a3d7-bda6-4f56-b830-48e8d2061560",
                    "LayerId": "b4606d3e-bc3a-451f-bf62-d45d28ba9ed7"
                },
                {
                    "id": "c37f826b-2825-4dc9-9c63-20a7f421fd66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa82a3d7-bda6-4f56-b830-48e8d2061560",
                    "LayerId": "d8800fbb-b374-464c-84e7-e6ba0fdf0bd6"
                }
            ]
        },
        {
            "id": "78949ddf-bc66-46ee-8174-a769d6df9fce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed442927-17ce-4551-b540-ad9c246b9d39",
            "compositeImage": {
                "id": "7312daa7-5559-43a7-8369-ac62ef6b2fd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78949ddf-bc66-46ee-8174-a769d6df9fce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06046769-712f-4f17-9268-910372ee45c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78949ddf-bc66-46ee-8174-a769d6df9fce",
                    "LayerId": "b4606d3e-bc3a-451f-bf62-d45d28ba9ed7"
                },
                {
                    "id": "7c7d31ac-e0cb-4a4d-ac5d-2c47c3d1721e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78949ddf-bc66-46ee-8174-a769d6df9fce",
                    "LayerId": "d8800fbb-b374-464c-84e7-e6ba0fdf0bd6"
                }
            ]
        },
        {
            "id": "881c5595-f7ef-4d58-8b7c-29ce89cafab6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed442927-17ce-4551-b540-ad9c246b9d39",
            "compositeImage": {
                "id": "0812bf0d-5142-4fb4-9188-def5daf57471",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "881c5595-f7ef-4d58-8b7c-29ce89cafab6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddb4dfb6-3db2-4d45-a154-1d1b6df199c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "881c5595-f7ef-4d58-8b7c-29ce89cafab6",
                    "LayerId": "d8800fbb-b374-464c-84e7-e6ba0fdf0bd6"
                },
                {
                    "id": "91ebd78a-9f5d-4fec-9fec-45ad37e755f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "881c5595-f7ef-4d58-8b7c-29ce89cafab6",
                    "LayerId": "b4606d3e-bc3a-451f-bf62-d45d28ba9ed7"
                }
            ]
        },
        {
            "id": "ee9582fe-26de-4752-9f4e-efa68d34c79c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed442927-17ce-4551-b540-ad9c246b9d39",
            "compositeImage": {
                "id": "29482d83-ab53-4164-9c76-d1512288c1cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee9582fe-26de-4752-9f4e-efa68d34c79c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "830b612b-ff3d-4a00-bc74-4736f23067b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee9582fe-26de-4752-9f4e-efa68d34c79c",
                    "LayerId": "d8800fbb-b374-464c-84e7-e6ba0fdf0bd6"
                },
                {
                    "id": "7abbb2c9-a129-4adb-b9bb-28d2c4b60813",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee9582fe-26de-4752-9f4e-efa68d34c79c",
                    "LayerId": "b4606d3e-bc3a-451f-bf62-d45d28ba9ed7"
                }
            ]
        },
        {
            "id": "e99805ec-72a4-42c1-864c-5dfc4ebfbf27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed442927-17ce-4551-b540-ad9c246b9d39",
            "compositeImage": {
                "id": "72a098a9-41ad-44ce-a097-ba086dc78767",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e99805ec-72a4-42c1-864c-5dfc4ebfbf27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e3570bc-d668-45cd-b577-e5a40203b171",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e99805ec-72a4-42c1-864c-5dfc4ebfbf27",
                    "LayerId": "d8800fbb-b374-464c-84e7-e6ba0fdf0bd6"
                },
                {
                    "id": "91a8450d-e55b-4635-bce5-2d80684593b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e99805ec-72a4-42c1-864c-5dfc4ebfbf27",
                    "LayerId": "b4606d3e-bc3a-451f-bf62-d45d28ba9ed7"
                }
            ]
        },
        {
            "id": "19a5c4da-1ae5-4706-b6f0-3f867b2ae05e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed442927-17ce-4551-b540-ad9c246b9d39",
            "compositeImage": {
                "id": "b5663c5b-896e-423b-989c-520722534532",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19a5c4da-1ae5-4706-b6f0-3f867b2ae05e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40fb5c66-c727-4202-8be5-ae61bd386d93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19a5c4da-1ae5-4706-b6f0-3f867b2ae05e",
                    "LayerId": "d8800fbb-b374-464c-84e7-e6ba0fdf0bd6"
                },
                {
                    "id": "4c40421b-686c-41d6-8702-94b8ff2789e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19a5c4da-1ae5-4706-b6f0-3f867b2ae05e",
                    "LayerId": "b4606d3e-bc3a-451f-bf62-d45d28ba9ed7"
                }
            ]
        },
        {
            "id": "45a764f6-dd5b-41c0-bfb1-fc1c32c71cdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed442927-17ce-4551-b540-ad9c246b9d39",
            "compositeImage": {
                "id": "de7bfc34-dc5d-4442-837a-752dda5502f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45a764f6-dd5b-41c0-bfb1-fc1c32c71cdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1189041-5b13-4d78-91ae-9d5533fe7015",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45a764f6-dd5b-41c0-bfb1-fc1c32c71cdd",
                    "LayerId": "d8800fbb-b374-464c-84e7-e6ba0fdf0bd6"
                },
                {
                    "id": "ccfe46dc-6a61-4ea0-94cd-8673e450af63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45a764f6-dd5b-41c0-bfb1-fc1c32c71cdd",
                    "LayerId": "b4606d3e-bc3a-451f-bf62-d45d28ba9ed7"
                }
            ]
        },
        {
            "id": "47775af8-dff9-4bed-bd41-a69bb3b2b254",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed442927-17ce-4551-b540-ad9c246b9d39",
            "compositeImage": {
                "id": "3f32153b-46a6-40fc-bce1-fee90bd89602",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47775af8-dff9-4bed-bd41-a69bb3b2b254",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13dd3036-43b6-4de8-bb7c-f2519e364028",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47775af8-dff9-4bed-bd41-a69bb3b2b254",
                    "LayerId": "d8800fbb-b374-464c-84e7-e6ba0fdf0bd6"
                },
                {
                    "id": "b31659e7-06db-4849-87f7-fca7a0f776ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47775af8-dff9-4bed-bd41-a69bb3b2b254",
                    "LayerId": "b4606d3e-bc3a-451f-bf62-d45d28ba9ed7"
                }
            ]
        },
        {
            "id": "83cfedba-cc13-4613-94d0-b214ca714310",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed442927-17ce-4551-b540-ad9c246b9d39",
            "compositeImage": {
                "id": "d0f84364-b675-470c-88fa-c06eb77d67cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83cfedba-cc13-4613-94d0-b214ca714310",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7c25f3e-11ef-4fed-94e6-ca623e272798",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83cfedba-cc13-4613-94d0-b214ca714310",
                    "LayerId": "d8800fbb-b374-464c-84e7-e6ba0fdf0bd6"
                },
                {
                    "id": "4d5c6549-74ee-4efd-a2a9-bd0322da2700",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83cfedba-cc13-4613-94d0-b214ca714310",
                    "LayerId": "b4606d3e-bc3a-451f-bf62-d45d28ba9ed7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d8800fbb-b374-464c-84e7-e6ba0fdf0bd6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed442927-17ce-4551-b540-ad9c246b9d39",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "b4606d3e-bc3a-451f-bf62-d45d28ba9ed7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed442927-17ce-4551-b540-ad9c246b9d39",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 16
}