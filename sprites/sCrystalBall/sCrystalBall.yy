{
    "id": "7172a306-aa67-4f3e-9eab-e86cb9cbdc80",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCrystalBall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 8,
    "bbox_right": 23,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19c33653-90b6-4eca-bbd0-c2497ffb7358",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7172a306-aa67-4f3e-9eab-e86cb9cbdc80",
            "compositeImage": {
                "id": "033f579f-133f-4d04-89ad-42bbdeac74b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19c33653-90b6-4eca-bbd0-c2497ffb7358",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97fb6e53-f6e7-484f-aee3-da7d91c83354",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19c33653-90b6-4eca-bbd0-c2497ffb7358",
                    "LayerId": "12059dec-117f-4dd7-8c00-30c00db1b6db"
                },
                {
                    "id": "e4f3f15b-9408-48c2-b86b-0357487ad50e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19c33653-90b6-4eca-bbd0-c2497ffb7358",
                    "LayerId": "d808a1d0-b77f-4bdd-b657-0f0c557e5a96"
                }
            ]
        },
        {
            "id": "5f59be85-06d5-451c-b36f-0f31840a12ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7172a306-aa67-4f3e-9eab-e86cb9cbdc80",
            "compositeImage": {
                "id": "56fee066-2036-43d1-9408-bfa5afec7e59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f59be85-06d5-451c-b36f-0f31840a12ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37d914b8-0421-41e8-b55f-906fdf1e8308",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f59be85-06d5-451c-b36f-0f31840a12ab",
                    "LayerId": "12059dec-117f-4dd7-8c00-30c00db1b6db"
                },
                {
                    "id": "eee281cc-9c17-4d3c-baa4-c6235a9491c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f59be85-06d5-451c-b36f-0f31840a12ab",
                    "LayerId": "d808a1d0-b77f-4bdd-b657-0f0c557e5a96"
                }
            ]
        },
        {
            "id": "fe4d8752-d9d5-4d30-a1fa-5bb3e7577c79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7172a306-aa67-4f3e-9eab-e86cb9cbdc80",
            "compositeImage": {
                "id": "3f32ff7d-7c0e-4f5c-82ab-5864615c9c9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe4d8752-d9d5-4d30-a1fa-5bb3e7577c79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9170e94-d578-4844-a8f5-fc4578da9b67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe4d8752-d9d5-4d30-a1fa-5bb3e7577c79",
                    "LayerId": "12059dec-117f-4dd7-8c00-30c00db1b6db"
                },
                {
                    "id": "e1270d8d-bcb7-47d3-9d52-4746e89cf1a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe4d8752-d9d5-4d30-a1fa-5bb3e7577c79",
                    "LayerId": "d808a1d0-b77f-4bdd-b657-0f0c557e5a96"
                }
            ]
        },
        {
            "id": "e7d1b249-2785-4f07-9fd2-0bb09fdf488d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7172a306-aa67-4f3e-9eab-e86cb9cbdc80",
            "compositeImage": {
                "id": "6f247e0a-f2dd-43f7-afcb-c2f680c2db36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7d1b249-2785-4f07-9fd2-0bb09fdf488d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ae0f0d8-2bb1-4019-922f-e637717ba7d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7d1b249-2785-4f07-9fd2-0bb09fdf488d",
                    "LayerId": "12059dec-117f-4dd7-8c00-30c00db1b6db"
                },
                {
                    "id": "f03f3dd3-b0e3-4f93-b24c-0facf6495771",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7d1b249-2785-4f07-9fd2-0bb09fdf488d",
                    "LayerId": "d808a1d0-b77f-4bdd-b657-0f0c557e5a96"
                }
            ]
        },
        {
            "id": "49edc34a-a2c9-4440-9804-0d57632e1128",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7172a306-aa67-4f3e-9eab-e86cb9cbdc80",
            "compositeImage": {
                "id": "40f05a63-6819-42bd-a466-599f56881479",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49edc34a-a2c9-4440-9804-0d57632e1128",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e5029be-7320-4157-a3f5-97f55513207f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49edc34a-a2c9-4440-9804-0d57632e1128",
                    "LayerId": "12059dec-117f-4dd7-8c00-30c00db1b6db"
                },
                {
                    "id": "cdce1458-4f80-44ce-bfed-77064f0e08cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49edc34a-a2c9-4440-9804-0d57632e1128",
                    "LayerId": "d808a1d0-b77f-4bdd-b657-0f0c557e5a96"
                }
            ]
        },
        {
            "id": "9e41e665-7687-409a-abf2-dc404d8e21f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7172a306-aa67-4f3e-9eab-e86cb9cbdc80",
            "compositeImage": {
                "id": "f97d5253-88e0-4b7f-b449-ffa70419b0de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e41e665-7687-409a-abf2-dc404d8e21f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28ef5bbe-f098-4e93-955c-6a0d358270b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e41e665-7687-409a-abf2-dc404d8e21f3",
                    "LayerId": "12059dec-117f-4dd7-8c00-30c00db1b6db"
                },
                {
                    "id": "ac31dd3f-7623-42e6-9f08-db918450c485",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e41e665-7687-409a-abf2-dc404d8e21f3",
                    "LayerId": "d808a1d0-b77f-4bdd-b657-0f0c557e5a96"
                }
            ]
        },
        {
            "id": "e98444b5-0ff9-4f85-824e-91e4810a1961",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7172a306-aa67-4f3e-9eab-e86cb9cbdc80",
            "compositeImage": {
                "id": "aa841b63-c397-4a1e-9390-87ed12b5ee2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e98444b5-0ff9-4f85-824e-91e4810a1961",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bfb6591-f5b6-4e5c-b69d-27e5260b86b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e98444b5-0ff9-4f85-824e-91e4810a1961",
                    "LayerId": "12059dec-117f-4dd7-8c00-30c00db1b6db"
                },
                {
                    "id": "2bf31a58-0a47-4116-82ca-a52834e806c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e98444b5-0ff9-4f85-824e-91e4810a1961",
                    "LayerId": "d808a1d0-b77f-4bdd-b657-0f0c557e5a96"
                }
            ]
        },
        {
            "id": "c7cc32e0-3bc6-4cf5-84f9-1519a3468e1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7172a306-aa67-4f3e-9eab-e86cb9cbdc80",
            "compositeImage": {
                "id": "93b0fdce-c565-4192-ba50-e71ff8e472e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7cc32e0-3bc6-4cf5-84f9-1519a3468e1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12e72149-e3f4-4b24-b229-5cbe1a2ba04e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7cc32e0-3bc6-4cf5-84f9-1519a3468e1e",
                    "LayerId": "12059dec-117f-4dd7-8c00-30c00db1b6db"
                },
                {
                    "id": "2758a92c-4ddb-44d1-9f41-2a20e8eac7aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7cc32e0-3bc6-4cf5-84f9-1519a3468e1e",
                    "LayerId": "d808a1d0-b77f-4bdd-b657-0f0c557e5a96"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d808a1d0-b77f-4bdd-b657-0f0c557e5a96",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7172a306-aa67-4f3e-9eab-e86cb9cbdc80",
            "blendMode": 0,
            "isLocked": false,
            "name": "default (2)",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "12059dec-117f-4dd7-8c00-30c00db1b6db",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7172a306-aa67-4f3e-9eab-e86cb9cbdc80",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}