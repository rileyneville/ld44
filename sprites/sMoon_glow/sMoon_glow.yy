{
    "id": "a1e99e26-1ad0-455c-8ecf-970f1091afde",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMoon_glow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 157,
    "bbox_left": 638,
    "bbox_right": 776,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8351cb1d-76d2-4c60-adc1-d49b5fd040d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1e99e26-1ad0-455c-8ecf-970f1091afde",
            "compositeImage": {
                "id": "13e96d9b-e4bc-4efa-a687-0961aee8587b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8351cb1d-76d2-4c60-adc1-d49b5fd040d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e79237d-e16f-4ae1-8be0-dc55e8906cae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8351cb1d-76d2-4c60-adc1-d49b5fd040d1",
                    "LayerId": "82d0b095-10d4-45e0-9dc2-8d4247dd94d6"
                },
                {
                    "id": "d142655b-26de-44f6-b7ff-75dcca4a3e23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8351cb1d-76d2-4c60-adc1-d49b5fd040d1",
                    "LayerId": "2d18b0b4-33f6-4cf3-bed6-fcb02e9aaa41"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 600,
    "layers": [
        {
            "id": "82d0b095-10d4-45e0-9dc2-8d4247dd94d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1e99e26-1ad0-455c-8ecf-970f1091afde",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 4 (2)",
            "opacity": 50,
            "visible": false
        },
        {
            "id": "2d18b0b4-33f6-4cf3-bed6-fcb02e9aaa41",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1e99e26-1ad0-455c-8ecf-970f1091afde",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 4",
            "opacity": 64,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}