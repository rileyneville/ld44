{
    "id": "352df41c-19b5-4da9-8926-87f0e16cf583",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCauldron_splash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 2,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0f493f12-79eb-465d-b222-0c48a95e5e68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "352df41c-19b5-4da9-8926-87f0e16cf583",
            "compositeImage": {
                "id": "85758737-4636-47d0-be7a-8f0432a41935",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f493f12-79eb-465d-b222-0c48a95e5e68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c17213e3-a16c-4ee2-be2e-cd75815854c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f493f12-79eb-465d-b222-0c48a95e5e68",
                    "LayerId": "90c726b8-29f9-4fef-953c-596435227edf"
                },
                {
                    "id": "36ed6395-bc59-4c40-8b00-86fbf6734c02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f493f12-79eb-465d-b222-0c48a95e5e68",
                    "LayerId": "f06e13a8-b64b-4a13-9548-aaa6c7c94d8e"
                }
            ]
        },
        {
            "id": "ab51f260-41e4-459b-87b0-41d75511f298",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "352df41c-19b5-4da9-8926-87f0e16cf583",
            "compositeImage": {
                "id": "13abce81-25b0-438b-8fd7-c0d0aa887501",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab51f260-41e4-459b-87b0-41d75511f298",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f274123-8d77-41cb-9a03-df311928a940",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab51f260-41e4-459b-87b0-41d75511f298",
                    "LayerId": "f06e13a8-b64b-4a13-9548-aaa6c7c94d8e"
                },
                {
                    "id": "38d73069-5c9f-4878-a3b6-685e09c961a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab51f260-41e4-459b-87b0-41d75511f298",
                    "LayerId": "90c726b8-29f9-4fef-953c-596435227edf"
                }
            ]
        },
        {
            "id": "9713bdf3-2df1-4d61-aa81-04dadf711756",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "352df41c-19b5-4da9-8926-87f0e16cf583",
            "compositeImage": {
                "id": "7622a820-6967-4c59-912d-3b073ab286d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9713bdf3-2df1-4d61-aa81-04dadf711756",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24b151d5-31a3-4bcf-bf55-9e7beb689d8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9713bdf3-2df1-4d61-aa81-04dadf711756",
                    "LayerId": "90c726b8-29f9-4fef-953c-596435227edf"
                },
                {
                    "id": "45b0654a-ecf5-4fa2-8ac6-68bb0f41c8dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9713bdf3-2df1-4d61-aa81-04dadf711756",
                    "LayerId": "f06e13a8-b64b-4a13-9548-aaa6c7c94d8e"
                }
            ]
        },
        {
            "id": "07141b2e-0271-472d-bbf2-2c2dbf18bec7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "352df41c-19b5-4da9-8926-87f0e16cf583",
            "compositeImage": {
                "id": "a67fa5c3-b05c-4b88-9403-32af3c7aa28d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07141b2e-0271-472d-bbf2-2c2dbf18bec7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04cfbac9-3121-4e4c-9252-959606ab5a89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07141b2e-0271-472d-bbf2-2c2dbf18bec7",
                    "LayerId": "90c726b8-29f9-4fef-953c-596435227edf"
                },
                {
                    "id": "c1eb9179-154f-4644-92a5-71a261907bcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07141b2e-0271-472d-bbf2-2c2dbf18bec7",
                    "LayerId": "f06e13a8-b64b-4a13-9548-aaa6c7c94d8e"
                }
            ]
        },
        {
            "id": "d3629270-ac61-4a0a-8300-a421b7f20f9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "352df41c-19b5-4da9-8926-87f0e16cf583",
            "compositeImage": {
                "id": "48b6abe4-d726-4f29-bca2-d93e087632e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3629270-ac61-4a0a-8300-a421b7f20f9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ab2b881-55d4-4bd5-9a5d-4481f299b3a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3629270-ac61-4a0a-8300-a421b7f20f9d",
                    "LayerId": "90c726b8-29f9-4fef-953c-596435227edf"
                },
                {
                    "id": "46929a74-03d4-44ba-9e19-c563d73b77b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3629270-ac61-4a0a-8300-a421b7f20f9d",
                    "LayerId": "f06e13a8-b64b-4a13-9548-aaa6c7c94d8e"
                }
            ]
        },
        {
            "id": "e3dffbd9-cbc0-43aa-8225-6f1f57468c95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "352df41c-19b5-4da9-8926-87f0e16cf583",
            "compositeImage": {
                "id": "264a248e-959a-4eaf-98d9-b05e5e33a4e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3dffbd9-cbc0-43aa-8225-6f1f57468c95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08359300-9f6d-4143-a83f-7cc2089d9e9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3dffbd9-cbc0-43aa-8225-6f1f57468c95",
                    "LayerId": "90c726b8-29f9-4fef-953c-596435227edf"
                },
                {
                    "id": "8c4205de-2966-4b8a-9179-476ba132b63f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3dffbd9-cbc0-43aa-8225-6f1f57468c95",
                    "LayerId": "f06e13a8-b64b-4a13-9548-aaa6c7c94d8e"
                }
            ]
        },
        {
            "id": "13231885-9f6a-4eac-80d3-4c7b7b86c2ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "352df41c-19b5-4da9-8926-87f0e16cf583",
            "compositeImage": {
                "id": "9f343691-1e9f-494f-aeff-c233fdb0471d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13231885-9f6a-4eac-80d3-4c7b7b86c2ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b70fea5-9c73-44a8-98d7-40f42248b342",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13231885-9f6a-4eac-80d3-4c7b7b86c2ec",
                    "LayerId": "90c726b8-29f9-4fef-953c-596435227edf"
                },
                {
                    "id": "cfc99e71-e01d-4d94-a905-0cabb9a9672f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13231885-9f6a-4eac-80d3-4c7b7b86c2ec",
                    "LayerId": "f06e13a8-b64b-4a13-9548-aaa6c7c94d8e"
                }
            ]
        },
        {
            "id": "6aace3e0-a5e4-4b1a-b0c7-02c7a4609f64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "352df41c-19b5-4da9-8926-87f0e16cf583",
            "compositeImage": {
                "id": "5e995771-32d4-4535-9e16-dda63d930e70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6aace3e0-a5e4-4b1a-b0c7-02c7a4609f64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fe7e017-c0c0-40df-bf1a-cd972f6eeca2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6aace3e0-a5e4-4b1a-b0c7-02c7a4609f64",
                    "LayerId": "90c726b8-29f9-4fef-953c-596435227edf"
                },
                {
                    "id": "023f5008-016b-4ed9-9a70-478aac3c7f6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6aace3e0-a5e4-4b1a-b0c7-02c7a4609f64",
                    "LayerId": "f06e13a8-b64b-4a13-9548-aaa6c7c94d8e"
                }
            ]
        },
        {
            "id": "fa5a279e-4dc2-46c8-abf7-e706e029aa92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "352df41c-19b5-4da9-8926-87f0e16cf583",
            "compositeImage": {
                "id": "6aeccd1a-e48f-40cf-945f-39a3b2ddf18e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa5a279e-4dc2-46c8-abf7-e706e029aa92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56c0e06e-9f69-46a4-a70c-18e9f102efbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa5a279e-4dc2-46c8-abf7-e706e029aa92",
                    "LayerId": "90c726b8-29f9-4fef-953c-596435227edf"
                },
                {
                    "id": "01e59c82-52b5-487a-9fcc-d64805db9a5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa5a279e-4dc2-46c8-abf7-e706e029aa92",
                    "LayerId": "f06e13a8-b64b-4a13-9548-aaa6c7c94d8e"
                }
            ]
        },
        {
            "id": "8bc52cdf-1e42-40c0-a800-29731fbacb4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "352df41c-19b5-4da9-8926-87f0e16cf583",
            "compositeImage": {
                "id": "b44ccff6-9fdb-44d1-8c73-6fbdcf47a6cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bc52cdf-1e42-40c0-a800-29731fbacb4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82add31e-dd1c-45cc-ac04-1ef3f05c223a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bc52cdf-1e42-40c0-a800-29731fbacb4f",
                    "LayerId": "90c726b8-29f9-4fef-953c-596435227edf"
                },
                {
                    "id": "464d1060-985b-4a2a-9910-e6c2e1ac2930",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bc52cdf-1e42-40c0-a800-29731fbacb4f",
                    "LayerId": "f06e13a8-b64b-4a13-9548-aaa6c7c94d8e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "90c726b8-29f9-4fef-953c-596435227edf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "352df41c-19b5-4da9-8926-87f0e16cf583",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "f06e13a8-b64b-4a13-9548-aaa6c7c94d8e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "352df41c-19b5-4da9-8926-87f0e16cf583",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}