{
    "id": "b78ceddf-fd4a-4cd6-b091-e191e54c5975",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFullscreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e28b62ac-a3ca-46c0-9997-9ef413dee37b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b78ceddf-fd4a-4cd6-b091-e191e54c5975",
            "compositeImage": {
                "id": "51e78baf-0cb5-495c-8566-6ec8edcedb87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e28b62ac-a3ca-46c0-9997-9ef413dee37b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69cf5cde-c116-425a-be10-70b222bf775d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e28b62ac-a3ca-46c0-9997-9ef413dee37b",
                    "LayerId": "1115b161-2b1b-46c5-8bc2-852046ca9e26"
                }
            ]
        },
        {
            "id": "5a0bda45-ab7f-4596-9f57-c681b776b876",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b78ceddf-fd4a-4cd6-b091-e191e54c5975",
            "compositeImage": {
                "id": "af6cd21f-67e0-47c2-8d18-2a3872f53031",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a0bda45-ab7f-4596-9f57-c681b776b876",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3752f77e-7a84-454b-8ed2-ff6ee448d4e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a0bda45-ab7f-4596-9f57-c681b776b876",
                    "LayerId": "1115b161-2b1b-46c5-8bc2-852046ca9e26"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1115b161-2b1b-46c5-8bc2-852046ca9e26",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b78ceddf-fd4a-4cd6-b091-e191e54c5975",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}