{
    "id": "0c46b2fe-6d84-4b24-b58f-25b5f5f214ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sNone",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aea1f8df-b83e-40e5-a6a9-9c4cdbcd2336",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c46b2fe-6d84-4b24-b58f-25b5f5f214ec",
            "compositeImage": {
                "id": "ad16e35b-b0e7-4876-ad44-c61c45c244b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aea1f8df-b83e-40e5-a6a9-9c4cdbcd2336",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "985f1a3f-bd1d-47ec-b8ed-c422ff564323",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aea1f8df-b83e-40e5-a6a9-9c4cdbcd2336",
                    "LayerId": "66fcf802-91ea-48e6-af91-e62b55d44e00"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "66fcf802-91ea-48e6-af91-e62b55d44e00",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0c46b2fe-6d84-4b24-b58f-25b5f5f214ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}