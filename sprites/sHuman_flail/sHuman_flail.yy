{
    "id": "4bfbb0f5-53b4-4b0b-aa95-3d755ca25f17",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHuman_flail",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d2d04298-692e-4143-b2b4-9414669035ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bfbb0f5-53b4-4b0b-aa95-3d755ca25f17",
            "compositeImage": {
                "id": "8b446209-7187-4326-adc4-53131b84a445",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2d04298-692e-4143-b2b4-9414669035ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24aaac9c-4b5f-477d-b411-1ae2594d5204",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2d04298-692e-4143-b2b4-9414669035ab",
                    "LayerId": "70e78c99-3fd4-4813-9ec4-6ca90278a871"
                }
            ]
        },
        {
            "id": "af4c6ad9-403e-44a6-a70f-449df629d3c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bfbb0f5-53b4-4b0b-aa95-3d755ca25f17",
            "compositeImage": {
                "id": "1d0a6061-8cc7-4ba0-91dc-c539de278cfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af4c6ad9-403e-44a6-a70f-449df629d3c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dc549f3-23dc-4ccc-8a67-f8b844ecd6bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af4c6ad9-403e-44a6-a70f-449df629d3c6",
                    "LayerId": "70e78c99-3fd4-4813-9ec4-6ca90278a871"
                }
            ]
        },
        {
            "id": "0882b3e4-2f1e-4592-9303-85dd1f8361a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bfbb0f5-53b4-4b0b-aa95-3d755ca25f17",
            "compositeImage": {
                "id": "d2d2e7ef-7395-4ded-b015-1ac4157100e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0882b3e4-2f1e-4592-9303-85dd1f8361a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "febab763-56d7-472b-a5e9-e410a9ad4cc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0882b3e4-2f1e-4592-9303-85dd1f8361a6",
                    "LayerId": "70e78c99-3fd4-4813-9ec4-6ca90278a871"
                }
            ]
        },
        {
            "id": "ed09a874-4d1f-4a57-8a88-f442fcd15872",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bfbb0f5-53b4-4b0b-aa95-3d755ca25f17",
            "compositeImage": {
                "id": "0fd9d169-aab5-4349-a4d8-61ac669f221a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed09a874-4d1f-4a57-8a88-f442fcd15872",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d37a73e6-b306-4de0-8ca4-2eab20ceaf50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed09a874-4d1f-4a57-8a88-f442fcd15872",
                    "LayerId": "70e78c99-3fd4-4813-9ec4-6ca90278a871"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "70e78c99-3fd4-4813-9ec4-6ca90278a871",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4bfbb0f5-53b4-4b0b-aa95-3d755ca25f17",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}