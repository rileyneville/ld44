{
    "id": "efbee90d-f5e9-4750-ba9e-d407008e3c5e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0dd8bac-79fe-4e4a-9675-860b59e5219c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efbee90d-f5e9-4750-ba9e-d407008e3c5e",
            "compositeImage": {
                "id": "e0079b7a-70ff-412e-ba9a-c59ddef843d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0dd8bac-79fe-4e4a-9675-860b59e5219c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f74264c-7e04-4eea-8a9e-ca9e13a94a13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0dd8bac-79fe-4e4a-9675-860b59e5219c",
                    "LayerId": "5d15018d-41c0-4637-9ab3-e6d8f8b8f54c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5d15018d-41c0-4637-9ab3-e6d8f8b8f54c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "efbee90d-f5e9-4750-ba9e-d407008e3c5e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 15,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}