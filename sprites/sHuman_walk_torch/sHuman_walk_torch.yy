{
    "id": "50c5dadb-01f1-4ecf-ab37-021104f72ae9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHuman_walk_torch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f13210d8-9a92-4f8c-a1eb-7c8379145af2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c5dadb-01f1-4ecf-ab37-021104f72ae9",
            "compositeImage": {
                "id": "b3267669-f95d-4381-8dd8-15cd081af12e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f13210d8-9a92-4f8c-a1eb-7c8379145af2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00170b26-f450-4bd3-b5f0-8876bbf09234",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f13210d8-9a92-4f8c-a1eb-7c8379145af2",
                    "LayerId": "214b6e33-8d97-4713-95a3-3eed708cc5e3"
                },
                {
                    "id": "607fd3e0-2038-43db-9181-499a2e397ca4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f13210d8-9a92-4f8c-a1eb-7c8379145af2",
                    "LayerId": "419502c9-c045-4890-a987-6a5462f02552"
                },
                {
                    "id": "1f5b0c2a-c4eb-4b0e-99d1-8ec4f5104521",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f13210d8-9a92-4f8c-a1eb-7c8379145af2",
                    "LayerId": "ce8a7daf-006e-49f9-ae13-fd51472a651a"
                },
                {
                    "id": "af2aac04-0e3c-40aa-9590-761a8b404f69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f13210d8-9a92-4f8c-a1eb-7c8379145af2",
                    "LayerId": "99082077-6929-4349-8c56-aeec096687fc"
                },
                {
                    "id": "c396a5ac-c26e-4366-b310-1d4165275555",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f13210d8-9a92-4f8c-a1eb-7c8379145af2",
                    "LayerId": "cced6496-5b55-4f9e-be40-74f4ca9fbab5"
                },
                {
                    "id": "5f98e07c-716a-4ae8-ae89-167673c5075a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f13210d8-9a92-4f8c-a1eb-7c8379145af2",
                    "LayerId": "939022db-c5bc-4d0b-981a-f2d56d7f2e66"
                }
            ]
        },
        {
            "id": "5c6fd19d-e26c-4393-9b7d-0013f6553ab6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c5dadb-01f1-4ecf-ab37-021104f72ae9",
            "compositeImage": {
                "id": "7c91e3ae-0238-4a2e-8975-fc022016df38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c6fd19d-e26c-4393-9b7d-0013f6553ab6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d26465f2-6203-46ab-be7a-59e65540ca09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c6fd19d-e26c-4393-9b7d-0013f6553ab6",
                    "LayerId": "214b6e33-8d97-4713-95a3-3eed708cc5e3"
                },
                {
                    "id": "cc6859e8-e79f-4d11-a27f-566421102daa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c6fd19d-e26c-4393-9b7d-0013f6553ab6",
                    "LayerId": "419502c9-c045-4890-a987-6a5462f02552"
                },
                {
                    "id": "929123a6-e39d-4d2b-a9c8-4bf521929df5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c6fd19d-e26c-4393-9b7d-0013f6553ab6",
                    "LayerId": "ce8a7daf-006e-49f9-ae13-fd51472a651a"
                },
                {
                    "id": "df00cfe7-6034-4117-beb2-7918f11dbe06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c6fd19d-e26c-4393-9b7d-0013f6553ab6",
                    "LayerId": "99082077-6929-4349-8c56-aeec096687fc"
                },
                {
                    "id": "6a3d3263-da31-4a76-9918-ade28ff2b7e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c6fd19d-e26c-4393-9b7d-0013f6553ab6",
                    "LayerId": "cced6496-5b55-4f9e-be40-74f4ca9fbab5"
                },
                {
                    "id": "309850bf-fc98-4028-846b-328ab99271f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c6fd19d-e26c-4393-9b7d-0013f6553ab6",
                    "LayerId": "939022db-c5bc-4d0b-981a-f2d56d7f2e66"
                }
            ]
        },
        {
            "id": "1d3e9141-e418-4a74-800f-00eed2556afc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c5dadb-01f1-4ecf-ab37-021104f72ae9",
            "compositeImage": {
                "id": "692958fa-1a16-4d9f-9901-509d23ce99b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d3e9141-e418-4a74-800f-00eed2556afc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df6d4b1d-ea63-4e62-9cb2-c794c6b4bd12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d3e9141-e418-4a74-800f-00eed2556afc",
                    "LayerId": "214b6e33-8d97-4713-95a3-3eed708cc5e3"
                },
                {
                    "id": "ce85233f-eaf3-432d-8252-207a54a351f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d3e9141-e418-4a74-800f-00eed2556afc",
                    "LayerId": "419502c9-c045-4890-a987-6a5462f02552"
                },
                {
                    "id": "f2320fb9-2770-4cce-ab59-4110b9afe15e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d3e9141-e418-4a74-800f-00eed2556afc",
                    "LayerId": "ce8a7daf-006e-49f9-ae13-fd51472a651a"
                },
                {
                    "id": "703355f0-e441-4c32-b9ce-81189770c19e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d3e9141-e418-4a74-800f-00eed2556afc",
                    "LayerId": "99082077-6929-4349-8c56-aeec096687fc"
                },
                {
                    "id": "50722b0d-bd9b-4fb8-920f-9989d2a0acde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d3e9141-e418-4a74-800f-00eed2556afc",
                    "LayerId": "cced6496-5b55-4f9e-be40-74f4ca9fbab5"
                },
                {
                    "id": "8cf25f65-9d06-43b0-bbbf-36d3f2d75d1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d3e9141-e418-4a74-800f-00eed2556afc",
                    "LayerId": "939022db-c5bc-4d0b-981a-f2d56d7f2e66"
                }
            ]
        },
        {
            "id": "c9bf4bb9-aeaa-4736-8490-54882212ea9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c5dadb-01f1-4ecf-ab37-021104f72ae9",
            "compositeImage": {
                "id": "3977e88f-5c82-4330-8c7a-efb6b26bb51a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9bf4bb9-aeaa-4736-8490-54882212ea9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6d23a13-3d4e-4370-88e6-202cd4834df8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9bf4bb9-aeaa-4736-8490-54882212ea9f",
                    "LayerId": "214b6e33-8d97-4713-95a3-3eed708cc5e3"
                },
                {
                    "id": "5a18e3af-f60e-4b5e-9232-6ba4a78a9ac7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9bf4bb9-aeaa-4736-8490-54882212ea9f",
                    "LayerId": "419502c9-c045-4890-a987-6a5462f02552"
                },
                {
                    "id": "28d192a3-1d55-4539-b8ab-8ece7be69de5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9bf4bb9-aeaa-4736-8490-54882212ea9f",
                    "LayerId": "ce8a7daf-006e-49f9-ae13-fd51472a651a"
                },
                {
                    "id": "66759033-27ff-416a-8970-31331dc993c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9bf4bb9-aeaa-4736-8490-54882212ea9f",
                    "LayerId": "99082077-6929-4349-8c56-aeec096687fc"
                },
                {
                    "id": "8848cfcc-8b5f-4f60-a696-a97c824685aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9bf4bb9-aeaa-4736-8490-54882212ea9f",
                    "LayerId": "cced6496-5b55-4f9e-be40-74f4ca9fbab5"
                },
                {
                    "id": "d37cbcf1-1866-4c76-b363-617cae9e603e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9bf4bb9-aeaa-4736-8490-54882212ea9f",
                    "LayerId": "939022db-c5bc-4d0b-981a-f2d56d7f2e66"
                }
            ]
        },
        {
            "id": "3ee34cf1-34b3-4b15-b5b1-73c2f1e243cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c5dadb-01f1-4ecf-ab37-021104f72ae9",
            "compositeImage": {
                "id": "7d6bcaf8-d32a-486e-a6a7-4b284cbf0bd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ee34cf1-34b3-4b15-b5b1-73c2f1e243cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e85820c7-e57e-4ce2-b781-b453c4d8ed4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ee34cf1-34b3-4b15-b5b1-73c2f1e243cd",
                    "LayerId": "214b6e33-8d97-4713-95a3-3eed708cc5e3"
                },
                {
                    "id": "8dfa0e93-47ad-4d53-8262-80973de6991c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ee34cf1-34b3-4b15-b5b1-73c2f1e243cd",
                    "LayerId": "419502c9-c045-4890-a987-6a5462f02552"
                },
                {
                    "id": "95010fef-ef4d-4e01-93d7-6771d2f32f8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ee34cf1-34b3-4b15-b5b1-73c2f1e243cd",
                    "LayerId": "ce8a7daf-006e-49f9-ae13-fd51472a651a"
                },
                {
                    "id": "aba79d82-38f5-4a89-b82b-1fc894e4a198",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ee34cf1-34b3-4b15-b5b1-73c2f1e243cd",
                    "LayerId": "99082077-6929-4349-8c56-aeec096687fc"
                },
                {
                    "id": "a2630926-f599-4ce7-9c4e-b95ea53f6d00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ee34cf1-34b3-4b15-b5b1-73c2f1e243cd",
                    "LayerId": "cced6496-5b55-4f9e-be40-74f4ca9fbab5"
                },
                {
                    "id": "b751e1c4-63a8-4b3e-8a9c-e21e65f04af3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ee34cf1-34b3-4b15-b5b1-73c2f1e243cd",
                    "LayerId": "939022db-c5bc-4d0b-981a-f2d56d7f2e66"
                }
            ]
        },
        {
            "id": "86f4ec33-addb-44c6-b979-b9453520b90b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c5dadb-01f1-4ecf-ab37-021104f72ae9",
            "compositeImage": {
                "id": "32e85aba-9806-4e99-ba20-4955c2f069c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86f4ec33-addb-44c6-b979-b9453520b90b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdaf189b-e209-4084-bf07-18df70840fcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86f4ec33-addb-44c6-b979-b9453520b90b",
                    "LayerId": "214b6e33-8d97-4713-95a3-3eed708cc5e3"
                },
                {
                    "id": "3302cb51-24cb-4c13-99d6-63bfb6c36df7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86f4ec33-addb-44c6-b979-b9453520b90b",
                    "LayerId": "419502c9-c045-4890-a987-6a5462f02552"
                },
                {
                    "id": "b4b5fd22-9f35-4e2c-9571-4d7ad292c1dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86f4ec33-addb-44c6-b979-b9453520b90b",
                    "LayerId": "ce8a7daf-006e-49f9-ae13-fd51472a651a"
                },
                {
                    "id": "8fea3fd8-9e79-411b-938a-b98cbda9676f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86f4ec33-addb-44c6-b979-b9453520b90b",
                    "LayerId": "99082077-6929-4349-8c56-aeec096687fc"
                },
                {
                    "id": "9dd5f0f6-c746-42a3-b819-4056e5da6e53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86f4ec33-addb-44c6-b979-b9453520b90b",
                    "LayerId": "cced6496-5b55-4f9e-be40-74f4ca9fbab5"
                },
                {
                    "id": "124a9453-d7de-4fa3-972a-54c85999b65b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86f4ec33-addb-44c6-b979-b9453520b90b",
                    "LayerId": "939022db-c5bc-4d0b-981a-f2d56d7f2e66"
                }
            ]
        },
        {
            "id": "b80755b1-33c6-4c82-8d46-eb41587a3138",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c5dadb-01f1-4ecf-ab37-021104f72ae9",
            "compositeImage": {
                "id": "18dddfc5-1827-4d78-a42c-4983b9964f98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b80755b1-33c6-4c82-8d46-eb41587a3138",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "289edd5e-8e5c-4c75-8633-c768e3e77f34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b80755b1-33c6-4c82-8d46-eb41587a3138",
                    "LayerId": "214b6e33-8d97-4713-95a3-3eed708cc5e3"
                },
                {
                    "id": "ae5cde6c-6a08-4518-aa73-9df2c223c8e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b80755b1-33c6-4c82-8d46-eb41587a3138",
                    "LayerId": "419502c9-c045-4890-a987-6a5462f02552"
                },
                {
                    "id": "ccf68dc7-b752-4436-94b8-66797a1aaaa7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b80755b1-33c6-4c82-8d46-eb41587a3138",
                    "LayerId": "ce8a7daf-006e-49f9-ae13-fd51472a651a"
                },
                {
                    "id": "f87184ba-f5ff-4392-8e2e-5fac3a03ba77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b80755b1-33c6-4c82-8d46-eb41587a3138",
                    "LayerId": "99082077-6929-4349-8c56-aeec096687fc"
                },
                {
                    "id": "89c22b0d-e220-48cc-9022-1cb066932428",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b80755b1-33c6-4c82-8d46-eb41587a3138",
                    "LayerId": "cced6496-5b55-4f9e-be40-74f4ca9fbab5"
                },
                {
                    "id": "8865d83f-140c-4f91-8b3f-b9af44272796",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b80755b1-33c6-4c82-8d46-eb41587a3138",
                    "LayerId": "939022db-c5bc-4d0b-981a-f2d56d7f2e66"
                }
            ]
        },
        {
            "id": "9c8aa295-6255-4e18-9136-9dc64a6468ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c5dadb-01f1-4ecf-ab37-021104f72ae9",
            "compositeImage": {
                "id": "1122db7e-8a3f-4caa-a2b5-d10415054654",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c8aa295-6255-4e18-9136-9dc64a6468ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c48a775-e18e-494d-95c0-7f399a5595f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c8aa295-6255-4e18-9136-9dc64a6468ba",
                    "LayerId": "214b6e33-8d97-4713-95a3-3eed708cc5e3"
                },
                {
                    "id": "10df635d-15b5-4941-8811-aa5d42f650af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c8aa295-6255-4e18-9136-9dc64a6468ba",
                    "LayerId": "419502c9-c045-4890-a987-6a5462f02552"
                },
                {
                    "id": "ac8b0340-fe11-443f-97bf-5087facfd512",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c8aa295-6255-4e18-9136-9dc64a6468ba",
                    "LayerId": "ce8a7daf-006e-49f9-ae13-fd51472a651a"
                },
                {
                    "id": "35253844-caa2-4d56-a259-7996244ee892",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c8aa295-6255-4e18-9136-9dc64a6468ba",
                    "LayerId": "99082077-6929-4349-8c56-aeec096687fc"
                },
                {
                    "id": "89b19fe0-49c7-4607-beb6-3a2cb9798a2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c8aa295-6255-4e18-9136-9dc64a6468ba",
                    "LayerId": "cced6496-5b55-4f9e-be40-74f4ca9fbab5"
                },
                {
                    "id": "e0716407-3aec-447f-b22a-459da6288159",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c8aa295-6255-4e18-9136-9dc64a6468ba",
                    "LayerId": "939022db-c5bc-4d0b-981a-f2d56d7f2e66"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cced6496-5b55-4f9e-be40-74f4ca9fbab5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50c5dadb-01f1-4ecf-ab37-021104f72ae9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3 (2) (2)",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "99082077-6929-4349-8c56-aeec096687fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50c5dadb-01f1-4ecf-ab37-021104f72ae9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3 (2)",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "939022db-c5bc-4d0b-981a-f2d56d7f2e66",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50c5dadb-01f1-4ecf-ab37-021104f72ae9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3 (2)",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "ce8a7daf-006e-49f9-ae13-fd51472a651a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50c5dadb-01f1-4ecf-ab37-021104f72ae9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "214b6e33-8d97-4713-95a3-3eed708cc5e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50c5dadb-01f1-4ecf-ab37-021104f72ae9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "419502c9-c045-4890-a987-6a5462f02552",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50c5dadb-01f1-4ecf-ab37-021104f72ae9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}