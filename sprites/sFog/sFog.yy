{
    "id": "bc9c3677-e03f-4c43-bb83-d9fad4cb05a3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFog",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 561,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 494,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ac2cf69-496d-48ea-b4d6-117631cc27a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc9c3677-e03f-4c43-bb83-d9fad4cb05a3",
            "compositeImage": {
                "id": "7d347d1b-062b-4b0c-9d6e-82bea62180f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ac2cf69-496d-48ea-b4d6-117631cc27a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92c9a28c-06e0-4f34-a611-4c698b7cd13d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ac2cf69-496d-48ea-b4d6-117631cc27a0",
                    "LayerId": "62f2af92-fcb9-4dab-93ef-32584c91d742"
                },
                {
                    "id": "98927003-46dd-492b-8325-394e184c0d83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ac2cf69-496d-48ea-b4d6-117631cc27a0",
                    "LayerId": "4e364d4a-4c16-4ae6-a707-b6d163fc4ce5"
                },
                {
                    "id": "b97a06d7-f7b1-426e-881d-46d0c71f9dab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ac2cf69-496d-48ea-b4d6-117631cc27a0",
                    "LayerId": "23eb6053-45a2-4525-9540-9e2a6b7f4063"
                },
                {
                    "id": "f6540a0a-e786-4ec3-b1d9-e24cc0467821",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ac2cf69-496d-48ea-b4d6-117631cc27a0",
                    "LayerId": "51523565-b4bd-4f07-bb02-aceb4a09a8d2"
                },
                {
                    "id": "98a51aef-e2bb-487d-9967-a3b4f71e7f20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ac2cf69-496d-48ea-b4d6-117631cc27a0",
                    "LayerId": "8184da60-e37f-4768-93f6-2fbcac040481"
                },
                {
                    "id": "8659175c-2c79-476f-b6ea-c6b156c7ba9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ac2cf69-496d-48ea-b4d6-117631cc27a0",
                    "LayerId": "ea2075ba-7f07-444b-95f9-1e57e4008ae0"
                },
                {
                    "id": "2b4c8362-c3b1-47fc-bd61-7014a03f8627",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ac2cf69-496d-48ea-b4d6-117631cc27a0",
                    "LayerId": "17649aac-dffd-47bc-acf0-3cc4c5a7bcb8"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 600,
    "layers": [
        {
            "id": "62f2af92-fcb9-4dab-93ef-32584c91d742",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc9c3677-e03f-4c43-bb83-d9fad4cb05a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "17649aac-dffd-47bc-acf0-3cc4c5a7bcb8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc9c3677-e03f-4c43-bb83-d9fad4cb05a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 5",
            "opacity": 15,
            "visible": true
        },
        {
            "id": "4e364d4a-4c16-4ae6-a707-b6d163fc4ce5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc9c3677-e03f-4c43-bb83-d9fad4cb05a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "23eb6053-45a2-4525-9540-9e2a6b7f4063",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc9c3677-e03f-4c43-bb83-d9fad4cb05a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 80,
            "visible": false
        },
        {
            "id": "51523565-b4bd-4f07-bb02-aceb4a09a8d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc9c3677-e03f-4c43-bb83-d9fad4cb05a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 4 (2)",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "8184da60-e37f-4768-93f6-2fbcac040481",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc9c3677-e03f-4c43-bb83-d9fad4cb05a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 4",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "ea2075ba-7f07-444b-95f9-1e57e4008ae0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc9c3677-e03f-4c43-bb83-d9fad4cb05a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}