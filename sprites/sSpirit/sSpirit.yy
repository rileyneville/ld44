{
    "id": "0e6315d0-88f1-480e-ae64-f6da72429c94",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpirit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 7,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "11854d44-ee00-4d38-b324-dd9bf4f24737",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e6315d0-88f1-480e-ae64-f6da72429c94",
            "compositeImage": {
                "id": "e2ca3dd5-a38a-41d2-8dec-d17bbb1cdcdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11854d44-ee00-4d38-b324-dd9bf4f24737",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4ff9801-9b09-4658-b6de-5dc828b8aa8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11854d44-ee00-4d38-b324-dd9bf4f24737",
                    "LayerId": "1fe4b74a-8f6d-49c7-8fbc-9ef6a8fd242d"
                },
                {
                    "id": "bd5ed7c1-c8a8-4cd2-b0c1-a2bcb354c1ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11854d44-ee00-4d38-b324-dd9bf4f24737",
                    "LayerId": "892aeb13-861a-4905-a30d-f954af8bdff2"
                }
            ]
        },
        {
            "id": "ffead0f2-1c0b-4ccd-a5b4-a301a93775a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e6315d0-88f1-480e-ae64-f6da72429c94",
            "compositeImage": {
                "id": "ebc3318d-62e5-49d1-8b0d-aa6b120938c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffead0f2-1c0b-4ccd-a5b4-a301a93775a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "203ad5d1-5864-40ca-868d-37aae99cece5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffead0f2-1c0b-4ccd-a5b4-a301a93775a7",
                    "LayerId": "1fe4b74a-8f6d-49c7-8fbc-9ef6a8fd242d"
                },
                {
                    "id": "a39a034e-f20b-4887-9e52-58bd56ac0945",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffead0f2-1c0b-4ccd-a5b4-a301a93775a7",
                    "LayerId": "892aeb13-861a-4905-a30d-f954af8bdff2"
                }
            ]
        },
        {
            "id": "48b44e99-7888-4128-a2ad-2dac19650a96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e6315d0-88f1-480e-ae64-f6da72429c94",
            "compositeImage": {
                "id": "895d6d76-c16c-48e2-9e42-f7fa8800dcae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48b44e99-7888-4128-a2ad-2dac19650a96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c084986-4528-441d-ae82-a4b0ef758808",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48b44e99-7888-4128-a2ad-2dac19650a96",
                    "LayerId": "1fe4b74a-8f6d-49c7-8fbc-9ef6a8fd242d"
                },
                {
                    "id": "ab1446ec-f546-468d-afdd-ef437c51af97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48b44e99-7888-4128-a2ad-2dac19650a96",
                    "LayerId": "892aeb13-861a-4905-a30d-f954af8bdff2"
                }
            ]
        },
        {
            "id": "f632e997-ea3a-4ecf-9aaa-50067faeda9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e6315d0-88f1-480e-ae64-f6da72429c94",
            "compositeImage": {
                "id": "d1fc1755-80f0-492b-93d0-453657d3bf36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f632e997-ea3a-4ecf-9aaa-50067faeda9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdbb5e0c-9c01-4337-a0ca-c8f02fc02275",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f632e997-ea3a-4ecf-9aaa-50067faeda9c",
                    "LayerId": "1fe4b74a-8f6d-49c7-8fbc-9ef6a8fd242d"
                },
                {
                    "id": "3f89c41f-91c0-4e8b-b064-caa959751114",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f632e997-ea3a-4ecf-9aaa-50067faeda9c",
                    "LayerId": "892aeb13-861a-4905-a30d-f954af8bdff2"
                }
            ]
        },
        {
            "id": "cad2e2c3-a998-4886-873f-30dbd9f8db2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e6315d0-88f1-480e-ae64-f6da72429c94",
            "compositeImage": {
                "id": "5b9d9273-a42d-496e-8b24-cb4ee9db0696",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cad2e2c3-a998-4886-873f-30dbd9f8db2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e9be531-f71d-4c7c-8df6-ef5ffbccdbba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cad2e2c3-a998-4886-873f-30dbd9f8db2a",
                    "LayerId": "1fe4b74a-8f6d-49c7-8fbc-9ef6a8fd242d"
                },
                {
                    "id": "7cf0f893-382e-492b-9b4f-c531021710b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cad2e2c3-a998-4886-873f-30dbd9f8db2a",
                    "LayerId": "892aeb13-861a-4905-a30d-f954af8bdff2"
                }
            ]
        },
        {
            "id": "8d73b38c-bfe1-4b73-90eb-2fc5ab047170",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e6315d0-88f1-480e-ae64-f6da72429c94",
            "compositeImage": {
                "id": "43400329-d531-4e89-a741-2e9d675cf65a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d73b38c-bfe1-4b73-90eb-2fc5ab047170",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01309386-918f-422c-8b08-1f98673f15da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d73b38c-bfe1-4b73-90eb-2fc5ab047170",
                    "LayerId": "1fe4b74a-8f6d-49c7-8fbc-9ef6a8fd242d"
                },
                {
                    "id": "404def27-63e5-48b2-9b97-51742f449cb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d73b38c-bfe1-4b73-90eb-2fc5ab047170",
                    "LayerId": "892aeb13-861a-4905-a30d-f954af8bdff2"
                }
            ]
        },
        {
            "id": "db42e185-a4a6-4d3a-9ff1-350095623fa3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e6315d0-88f1-480e-ae64-f6da72429c94",
            "compositeImage": {
                "id": "7c70e4f8-2e40-48d6-9111-57f7aa2b9b64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db42e185-a4a6-4d3a-9ff1-350095623fa3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26751080-8e43-4302-a41e-90be2c063818",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db42e185-a4a6-4d3a-9ff1-350095623fa3",
                    "LayerId": "1fe4b74a-8f6d-49c7-8fbc-9ef6a8fd242d"
                },
                {
                    "id": "f9fba75c-e887-48e8-bae2-b58b07f0f38d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db42e185-a4a6-4d3a-9ff1-350095623fa3",
                    "LayerId": "892aeb13-861a-4905-a30d-f954af8bdff2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "892aeb13-861a-4905-a30d-f954af8bdff2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e6315d0-88f1-480e-ae64-f6da72429c94",
            "blendMode": 0,
            "isLocked": false,
            "name": "default (2)",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "1fe4b74a-8f6d-49c7-8fbc-9ef6a8fd242d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e6315d0-88f1-480e-ae64-f6da72429c94",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}