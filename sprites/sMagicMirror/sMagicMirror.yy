{
    "id": "4a591e4b-8676-48bc-a5bb-d7947d291700",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMagicMirror",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 7,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54d4facd-cd39-4688-ad8d-b53f5f1ef2cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a591e4b-8676-48bc-a5bb-d7947d291700",
            "compositeImage": {
                "id": "2e04489f-9e3a-4238-a797-67f2629e8ebf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54d4facd-cd39-4688-ad8d-b53f5f1ef2cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f72e3ac-348a-4a75-ba6d-0968c303f296",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54d4facd-cd39-4688-ad8d-b53f5f1ef2cc",
                    "LayerId": "21724882-1256-4557-85f0-57dd64c63c50"
                },
                {
                    "id": "7153206f-3868-4aa4-9bbb-260769999dca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54d4facd-cd39-4688-ad8d-b53f5f1ef2cc",
                    "LayerId": "ded39d87-8cbf-4f07-8cc2-ebf2714f8e6d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "21724882-1256-4557-85f0-57dd64c63c50",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4a591e4b-8676-48bc-a5bb-d7947d291700",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "ded39d87-8cbf-4f07-8cc2-ebf2714f8e6d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4a591e4b-8676-48bc-a5bb-d7947d291700",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}