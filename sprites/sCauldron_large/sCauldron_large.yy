{
    "id": "77524f9a-30c9-42ab-a154-e833d259c782",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCauldron_large",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 14,
    "bbox_right": 49,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f803eadc-9a82-4d4d-b78d-63bdd5c8fdbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77524f9a-30c9-42ab-a154-e833d259c782",
            "compositeImage": {
                "id": "6b015453-384d-4ce6-a083-34aaf1573b55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f803eadc-9a82-4d4d-b78d-63bdd5c8fdbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27019915-d143-48e5-90c6-76c2cd2dfc24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f803eadc-9a82-4d4d-b78d-63bdd5c8fdbb",
                    "LayerId": "f19581da-9e39-4d77-bb29-31cff9c26872"
                },
                {
                    "id": "8a6d1203-7223-430c-9727-562456f59122",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f803eadc-9a82-4d4d-b78d-63bdd5c8fdbb",
                    "LayerId": "bb693f44-15a7-42da-9186-70d7ae1e48f4"
                },
                {
                    "id": "1ad8c2ee-a453-408f-bd7b-132e5e63288b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f803eadc-9a82-4d4d-b78d-63bdd5c8fdbb",
                    "LayerId": "05619971-ef6d-44a3-9559-5e0ec12f578d"
                }
            ]
        },
        {
            "id": "9094fcc8-2806-4cb6-8728-806be7b9328f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77524f9a-30c9-42ab-a154-e833d259c782",
            "compositeImage": {
                "id": "7c143aee-2bf5-4fc1-9873-be7e8bb63341",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9094fcc8-2806-4cb6-8728-806be7b9328f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99806cb8-5cd5-442a-a2fe-e973b2b1fabf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9094fcc8-2806-4cb6-8728-806be7b9328f",
                    "LayerId": "f19581da-9e39-4d77-bb29-31cff9c26872"
                },
                {
                    "id": "15385343-9f9c-4a7d-aec3-ac61dc975195",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9094fcc8-2806-4cb6-8728-806be7b9328f",
                    "LayerId": "bb693f44-15a7-42da-9186-70d7ae1e48f4"
                },
                {
                    "id": "93ef89e3-ea4b-494b-9f4d-4a34d353018b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9094fcc8-2806-4cb6-8728-806be7b9328f",
                    "LayerId": "05619971-ef6d-44a3-9559-5e0ec12f578d"
                }
            ]
        },
        {
            "id": "db5d1091-cea1-4373-9d67-1e91dc8dd480",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77524f9a-30c9-42ab-a154-e833d259c782",
            "compositeImage": {
                "id": "30016ec7-bf1f-453b-a4d5-08c2dfa9c553",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db5d1091-cea1-4373-9d67-1e91dc8dd480",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9b322f9-c4b9-456d-b65d-1d382fdf7fc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db5d1091-cea1-4373-9d67-1e91dc8dd480",
                    "LayerId": "f19581da-9e39-4d77-bb29-31cff9c26872"
                },
                {
                    "id": "aa5879a3-78bc-47e2-85cc-92b6a6595f22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db5d1091-cea1-4373-9d67-1e91dc8dd480",
                    "LayerId": "bb693f44-15a7-42da-9186-70d7ae1e48f4"
                },
                {
                    "id": "d17ff6d6-898e-4b43-90e9-c967e652bb16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db5d1091-cea1-4373-9d67-1e91dc8dd480",
                    "LayerId": "05619971-ef6d-44a3-9559-5e0ec12f578d"
                }
            ]
        },
        {
            "id": "8e360851-ffbf-47e4-9391-34d45093f748",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77524f9a-30c9-42ab-a154-e833d259c782",
            "compositeImage": {
                "id": "1f8eaebf-7818-474e-9fce-07856696eb7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e360851-ffbf-47e4-9391-34d45093f748",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78018a56-1123-4ad8-8250-9f1de3d86b30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e360851-ffbf-47e4-9391-34d45093f748",
                    "LayerId": "f19581da-9e39-4d77-bb29-31cff9c26872"
                },
                {
                    "id": "4af320b2-68cb-4aa5-9fa8-d04a82f079fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e360851-ffbf-47e4-9391-34d45093f748",
                    "LayerId": "bb693f44-15a7-42da-9186-70d7ae1e48f4"
                },
                {
                    "id": "7430f62b-7f52-47df-b9a9-9a789e1dc2c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e360851-ffbf-47e4-9391-34d45093f748",
                    "LayerId": "05619971-ef6d-44a3-9559-5e0ec12f578d"
                }
            ]
        },
        {
            "id": "bb8c680b-efd6-4f51-a3ba-e3fc01a2e626",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77524f9a-30c9-42ab-a154-e833d259c782",
            "compositeImage": {
                "id": "891a0098-1a28-4bc1-991d-f4a72e3e3312",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb8c680b-efd6-4f51-a3ba-e3fc01a2e626",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d2439f8-d6c4-4919-b01e-5568959f4e4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb8c680b-efd6-4f51-a3ba-e3fc01a2e626",
                    "LayerId": "05619971-ef6d-44a3-9559-5e0ec12f578d"
                },
                {
                    "id": "5499d3ac-80ac-41db-8fd1-d393ac72c62d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb8c680b-efd6-4f51-a3ba-e3fc01a2e626",
                    "LayerId": "bb693f44-15a7-42da-9186-70d7ae1e48f4"
                },
                {
                    "id": "eb267867-359a-4f69-b744-add52725cc8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb8c680b-efd6-4f51-a3ba-e3fc01a2e626",
                    "LayerId": "f19581da-9e39-4d77-bb29-31cff9c26872"
                }
            ]
        },
        {
            "id": "dd5226a5-23b4-466a-9c69-5f59cded00fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77524f9a-30c9-42ab-a154-e833d259c782",
            "compositeImage": {
                "id": "3bc822a4-c739-4a3b-b44c-f65a3a762d5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd5226a5-23b4-466a-9c69-5f59cded00fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fa797ae-deca-41cb-8fd2-550821de3d45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd5226a5-23b4-466a-9c69-5f59cded00fb",
                    "LayerId": "05619971-ef6d-44a3-9559-5e0ec12f578d"
                },
                {
                    "id": "0270dd53-2c26-4d2a-b6f6-baa430d56c53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd5226a5-23b4-466a-9c69-5f59cded00fb",
                    "LayerId": "bb693f44-15a7-42da-9186-70d7ae1e48f4"
                },
                {
                    "id": "da8be8d2-5abe-4ec3-a9f7-ed31127f516d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd5226a5-23b4-466a-9c69-5f59cded00fb",
                    "LayerId": "f19581da-9e39-4d77-bb29-31cff9c26872"
                }
            ]
        },
        {
            "id": "99f6ad0d-ff25-4d41-9d00-f5ebfcba8b5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77524f9a-30c9-42ab-a154-e833d259c782",
            "compositeImage": {
                "id": "0a2496cf-6ac4-45bd-b42c-f3b78ef330f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99f6ad0d-ff25-4d41-9d00-f5ebfcba8b5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca63b084-3fdd-4130-8fb3-90eb043426e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99f6ad0d-ff25-4d41-9d00-f5ebfcba8b5d",
                    "LayerId": "05619971-ef6d-44a3-9559-5e0ec12f578d"
                },
                {
                    "id": "f3d4641b-2075-4aa5-9d54-5533d2a632af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99f6ad0d-ff25-4d41-9d00-f5ebfcba8b5d",
                    "LayerId": "bb693f44-15a7-42da-9186-70d7ae1e48f4"
                },
                {
                    "id": "fc422159-8813-41d0-a532-437c1bb4c2e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99f6ad0d-ff25-4d41-9d00-f5ebfcba8b5d",
                    "LayerId": "f19581da-9e39-4d77-bb29-31cff9c26872"
                }
            ]
        },
        {
            "id": "9e6f997b-a2e6-4f77-a005-33d17eb459f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77524f9a-30c9-42ab-a154-e833d259c782",
            "compositeImage": {
                "id": "f4d6764e-3f4a-48f8-b1d0-7e8445ee80cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e6f997b-a2e6-4f77-a005-33d17eb459f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e852e07-bcee-4518-a455-2752ef5fee87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e6f997b-a2e6-4f77-a005-33d17eb459f9",
                    "LayerId": "05619971-ef6d-44a3-9559-5e0ec12f578d"
                },
                {
                    "id": "7053328d-2997-4a4c-b79f-730d8b3b88d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e6f997b-a2e6-4f77-a005-33d17eb459f9",
                    "LayerId": "bb693f44-15a7-42da-9186-70d7ae1e48f4"
                },
                {
                    "id": "c61c59a8-159a-4881-84f3-5f29d8cd6b8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e6f997b-a2e6-4f77-a005-33d17eb459f9",
                    "LayerId": "f19581da-9e39-4d77-bb29-31cff9c26872"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "05619971-ef6d-44a3-9559-5e0ec12f578d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77524f9a-30c9-42ab-a154-e833d259c782",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1 (2)",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "bb693f44-15a7-42da-9186-70d7ae1e48f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77524f9a-30c9-42ab-a154-e833d259c782",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "f19581da-9e39-4d77-bb29-31cff9c26872",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77524f9a-30c9-42ab-a154-e833d259c782",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 16
}