{
    "id": "ea4114c5-fc7c-4533-bab6-904c59f3d344",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSky",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3ccd78b5-309d-4cdc-b264-ebfb38815d75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea4114c5-fc7c-4533-bab6-904c59f3d344",
            "compositeImage": {
                "id": "8b79441b-cc1d-4230-bfa5-2b34e6c6ddfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ccd78b5-309d-4cdc-b264-ebfb38815d75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf01a492-02a3-48a5-a7d9-d90a9a7ebda5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ccd78b5-309d-4cdc-b264-ebfb38815d75",
                    "LayerId": "7b7be030-646e-4e18-8415-e8c4c857b28f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "7b7be030-646e-4e18-8415-e8c4c857b28f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea4114c5-fc7c-4533-bab6-904c59f3d344",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}