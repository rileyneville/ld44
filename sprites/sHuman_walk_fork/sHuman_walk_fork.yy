{
    "id": "b8c0a8b3-bc72-457d-8d78-917deebdc57b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHuman_walk_fork",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "951675df-85c9-46f6-a6cf-eb0c8914628f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8c0a8b3-bc72-457d-8d78-917deebdc57b",
            "compositeImage": {
                "id": "2ccc3fb2-e021-4cfa-a066-f0dec7a0ebac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "951675df-85c9-46f6-a6cf-eb0c8914628f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3144741-8f9a-4644-916f-d708d310854f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "951675df-85c9-46f6-a6cf-eb0c8914628f",
                    "LayerId": "316d0f13-7373-45a0-8a93-0b53efd32cfd"
                },
                {
                    "id": "9b5e58e0-dd69-49d7-a09a-5c262d08d44e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "951675df-85c9-46f6-a6cf-eb0c8914628f",
                    "LayerId": "789cd0e3-367f-47fc-badb-e39efa195a63"
                }
            ]
        },
        {
            "id": "bbb39b2a-2519-4d58-9b1d-ae2ca4390ed0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8c0a8b3-bc72-457d-8d78-917deebdc57b",
            "compositeImage": {
                "id": "59965c1a-3ffc-4682-a25a-b693a751b089",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbb39b2a-2519-4d58-9b1d-ae2ca4390ed0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "243dc71c-fda7-49f6-b6ef-d6614571203d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbb39b2a-2519-4d58-9b1d-ae2ca4390ed0",
                    "LayerId": "316d0f13-7373-45a0-8a93-0b53efd32cfd"
                },
                {
                    "id": "81cd2f3b-8d6f-4f9d-8c61-f1bdf4b1c43f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbb39b2a-2519-4d58-9b1d-ae2ca4390ed0",
                    "LayerId": "789cd0e3-367f-47fc-badb-e39efa195a63"
                }
            ]
        },
        {
            "id": "1f87dbf2-3c26-4aa8-9364-a403bccc8e58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8c0a8b3-bc72-457d-8d78-917deebdc57b",
            "compositeImage": {
                "id": "f1e610f9-26e4-45e9-a215-1edbdffb8cdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f87dbf2-3c26-4aa8-9364-a403bccc8e58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2e6da42-88a6-4581-8f19-8ac3150254e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f87dbf2-3c26-4aa8-9364-a403bccc8e58",
                    "LayerId": "316d0f13-7373-45a0-8a93-0b53efd32cfd"
                },
                {
                    "id": "e084cf19-eda8-4c59-8633-fc44f0ab64f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f87dbf2-3c26-4aa8-9364-a403bccc8e58",
                    "LayerId": "789cd0e3-367f-47fc-badb-e39efa195a63"
                }
            ]
        },
        {
            "id": "d101bd84-5139-4b4b-856b-fd88d4f42083",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8c0a8b3-bc72-457d-8d78-917deebdc57b",
            "compositeImage": {
                "id": "345ef358-4183-4138-81dd-dff0581740ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d101bd84-5139-4b4b-856b-fd88d4f42083",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0636aaf-41cf-4cb7-86c6-cafef31b8aaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d101bd84-5139-4b4b-856b-fd88d4f42083",
                    "LayerId": "316d0f13-7373-45a0-8a93-0b53efd32cfd"
                },
                {
                    "id": "b565a2dc-3ec8-4490-adfe-876662033a19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d101bd84-5139-4b4b-856b-fd88d4f42083",
                    "LayerId": "789cd0e3-367f-47fc-badb-e39efa195a63"
                }
            ]
        },
        {
            "id": "52b9ad43-dea2-49c8-9c9b-eb1ce3ea7c23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8c0a8b3-bc72-457d-8d78-917deebdc57b",
            "compositeImage": {
                "id": "193c5cba-be06-4f4a-a7fb-93b1e34c6f79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52b9ad43-dea2-49c8-9c9b-eb1ce3ea7c23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10468873-048d-4d09-a8aa-e396c08a85f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52b9ad43-dea2-49c8-9c9b-eb1ce3ea7c23",
                    "LayerId": "316d0f13-7373-45a0-8a93-0b53efd32cfd"
                },
                {
                    "id": "1c6bc32e-8827-491d-bba5-5a04b347c4e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52b9ad43-dea2-49c8-9c9b-eb1ce3ea7c23",
                    "LayerId": "789cd0e3-367f-47fc-badb-e39efa195a63"
                }
            ]
        },
        {
            "id": "9a88d338-d1be-4f76-8be7-3f55df577282",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8c0a8b3-bc72-457d-8d78-917deebdc57b",
            "compositeImage": {
                "id": "2d91cf00-3394-4e82-9acf-0a79833419bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a88d338-d1be-4f76-8be7-3f55df577282",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce932346-410b-4cf5-907d-33a8542a3941",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a88d338-d1be-4f76-8be7-3f55df577282",
                    "LayerId": "316d0f13-7373-45a0-8a93-0b53efd32cfd"
                },
                {
                    "id": "12c029cf-fc28-4207-aa09-a19010b05528",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a88d338-d1be-4f76-8be7-3f55df577282",
                    "LayerId": "789cd0e3-367f-47fc-badb-e39efa195a63"
                }
            ]
        },
        {
            "id": "1bcf8236-04d2-4146-a2ab-13ea3c337194",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8c0a8b3-bc72-457d-8d78-917deebdc57b",
            "compositeImage": {
                "id": "1d4d1396-ec07-4afb-b1c1-aeaecbf8aece",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bcf8236-04d2-4146-a2ab-13ea3c337194",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca6bba91-ba48-4e1c-a475-389200e77dbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bcf8236-04d2-4146-a2ab-13ea3c337194",
                    "LayerId": "316d0f13-7373-45a0-8a93-0b53efd32cfd"
                },
                {
                    "id": "673b81eb-c753-435e-811b-27852e1e88e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bcf8236-04d2-4146-a2ab-13ea3c337194",
                    "LayerId": "789cd0e3-367f-47fc-badb-e39efa195a63"
                }
            ]
        },
        {
            "id": "16c7e8f5-a75c-453c-ad93-eb9c88628c66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8c0a8b3-bc72-457d-8d78-917deebdc57b",
            "compositeImage": {
                "id": "1887d886-28a9-4a55-87fb-e90514be347a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16c7e8f5-a75c-453c-ad93-eb9c88628c66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bf1379b-92e6-4265-9ec2-f30d12cb8d4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16c7e8f5-a75c-453c-ad93-eb9c88628c66",
                    "LayerId": "316d0f13-7373-45a0-8a93-0b53efd32cfd"
                },
                {
                    "id": "2788316d-ce9a-4d98-a1a9-69da0de0100f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16c7e8f5-a75c-453c-ad93-eb9c88628c66",
                    "LayerId": "789cd0e3-367f-47fc-badb-e39efa195a63"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "789cd0e3-367f-47fc-badb-e39efa195a63",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b8c0a8b3-bc72-457d-8d78-917deebdc57b",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "316d0f13-7373-45a0-8a93-0b53efd32cfd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b8c0a8b3-bc72-457d-8d78-917deebdc57b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}