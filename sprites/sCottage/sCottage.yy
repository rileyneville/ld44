{
    "id": "5d1bd3d9-c69f-4609-9c9e-1a7d858062ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCottage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 560,
    "bbox_left": 575,
    "bbox_right": 799,
    "bbox_top": 404,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b366f0e8-899d-4517-82ff-b04d9573aee5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d1bd3d9-c69f-4609-9c9e-1a7d858062ab",
            "compositeImage": {
                "id": "ad8c796a-657b-4ff4-95cb-021bb09ac3c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b366f0e8-899d-4517-82ff-b04d9573aee5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6e8554a-25de-4ee6-b827-e0845a1fa1cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b366f0e8-899d-4517-82ff-b04d9573aee5",
                    "LayerId": "6460169d-fe49-4fb3-9365-1940d8bf1655"
                },
                {
                    "id": "0325837b-26dc-4b1a-81ab-5470a1fb99c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b366f0e8-899d-4517-82ff-b04d9573aee5",
                    "LayerId": "e6b1c254-439c-4c2a-99ba-8526853d74e2"
                }
            ]
        },
        {
            "id": "b6d31892-25f9-45da-aea4-750f4d180c37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d1bd3d9-c69f-4609-9c9e-1a7d858062ab",
            "compositeImage": {
                "id": "7f8f4a8c-62d4-4ea4-a45f-d2be3fd308d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6d31892-25f9-45da-aea4-750f4d180c37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcd18f7f-32fa-4373-a26b-297e7ebc4218",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6d31892-25f9-45da-aea4-750f4d180c37",
                    "LayerId": "e6b1c254-439c-4c2a-99ba-8526853d74e2"
                },
                {
                    "id": "4a12f493-b009-46c0-af26-ba7ec8a9c2d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6d31892-25f9-45da-aea4-750f4d180c37",
                    "LayerId": "6460169d-fe49-4fb3-9365-1940d8bf1655"
                }
            ]
        },
        {
            "id": "8add42e7-ae1a-45fa-86cb-4b49d9f48b1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d1bd3d9-c69f-4609-9c9e-1a7d858062ab",
            "compositeImage": {
                "id": "350c4cd1-579b-4b3a-bb5c-382c15acabed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8add42e7-ae1a-45fa-86cb-4b49d9f48b1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aac4a9d3-17a3-4459-987d-b8b6158f2a3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8add42e7-ae1a-45fa-86cb-4b49d9f48b1b",
                    "LayerId": "e6b1c254-439c-4c2a-99ba-8526853d74e2"
                },
                {
                    "id": "0b1fa396-728a-4e00-a906-b8941458355d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8add42e7-ae1a-45fa-86cb-4b49d9f48b1b",
                    "LayerId": "6460169d-fe49-4fb3-9365-1940d8bf1655"
                }
            ]
        },
        {
            "id": "6a86bb7f-e391-4c90-bb4c-938e6c3aaaac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d1bd3d9-c69f-4609-9c9e-1a7d858062ab",
            "compositeImage": {
                "id": "4809a939-f2c5-4723-b867-f8bc29825464",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a86bb7f-e391-4c90-bb4c-938e6c3aaaac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bb248f0-054c-4ad1-8919-ac45d7a141ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a86bb7f-e391-4c90-bb4c-938e6c3aaaac",
                    "LayerId": "e6b1c254-439c-4c2a-99ba-8526853d74e2"
                },
                {
                    "id": "658f80dc-e80c-4ff3-83b3-890e49ac6936",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a86bb7f-e391-4c90-bb4c-938e6c3aaaac",
                    "LayerId": "6460169d-fe49-4fb3-9365-1940d8bf1655"
                }
            ]
        },
        {
            "id": "8d6c1a20-7884-4e72-a4e2-115a1d4667cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d1bd3d9-c69f-4609-9c9e-1a7d858062ab",
            "compositeImage": {
                "id": "83ea51af-56f6-477a-8a50-9d3a62689779",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d6c1a20-7884-4e72-a4e2-115a1d4667cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d462a9b-7f1c-43df-a79f-01fc79aedb3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d6c1a20-7884-4e72-a4e2-115a1d4667cf",
                    "LayerId": "e6b1c254-439c-4c2a-99ba-8526853d74e2"
                },
                {
                    "id": "01ce3c57-6461-447f-8993-868c29af1dfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d6c1a20-7884-4e72-a4e2-115a1d4667cf",
                    "LayerId": "6460169d-fe49-4fb3-9365-1940d8bf1655"
                }
            ]
        },
        {
            "id": "ef947d72-b286-4918-a7aa-96f2eb99ff4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d1bd3d9-c69f-4609-9c9e-1a7d858062ab",
            "compositeImage": {
                "id": "1e39b8c9-78fa-414a-9b62-d7572cd3f72e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef947d72-b286-4918-a7aa-96f2eb99ff4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee75a98a-3d66-4364-b623-425c06cfac52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef947d72-b286-4918-a7aa-96f2eb99ff4b",
                    "LayerId": "e6b1c254-439c-4c2a-99ba-8526853d74e2"
                },
                {
                    "id": "1cf5b0c2-ea3c-4e7b-869d-590c4f8b013f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef947d72-b286-4918-a7aa-96f2eb99ff4b",
                    "LayerId": "6460169d-fe49-4fb3-9365-1940d8bf1655"
                }
            ]
        },
        {
            "id": "d1571ded-64d4-488d-a0f4-3e0194f1ad84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d1bd3d9-c69f-4609-9c9e-1a7d858062ab",
            "compositeImage": {
                "id": "10cdefa7-f5d7-4895-b0e2-490685871f1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1571ded-64d4-488d-a0f4-3e0194f1ad84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42b332b9-4777-407b-b4cf-8f9383374365",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1571ded-64d4-488d-a0f4-3e0194f1ad84",
                    "LayerId": "e6b1c254-439c-4c2a-99ba-8526853d74e2"
                },
                {
                    "id": "62896cce-3e36-4b22-bc5a-cd9925a407a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1571ded-64d4-488d-a0f4-3e0194f1ad84",
                    "LayerId": "6460169d-fe49-4fb3-9365-1940d8bf1655"
                }
            ]
        },
        {
            "id": "91d3e0de-fdc1-40df-9b4a-8c4324659a47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d1bd3d9-c69f-4609-9c9e-1a7d858062ab",
            "compositeImage": {
                "id": "ebe4bdf0-423f-4b81-a599-943a3a201ded",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91d3e0de-fdc1-40df-9b4a-8c4324659a47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "566b79fb-275c-4761-b319-cd68e86f203f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91d3e0de-fdc1-40df-9b4a-8c4324659a47",
                    "LayerId": "e6b1c254-439c-4c2a-99ba-8526853d74e2"
                },
                {
                    "id": "50c99277-5d0f-494a-bb0d-1930943caf77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91d3e0de-fdc1-40df-9b4a-8c4324659a47",
                    "LayerId": "6460169d-fe49-4fb3-9365-1940d8bf1655"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 600,
    "layers": [
        {
            "id": "6460169d-fe49-4fb3-9365-1940d8bf1655",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d1bd3d9-c69f-4609-9c9e-1a7d858062ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "e6b1c254-439c-4c2a-99ba-8526853d74e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d1bd3d9-c69f-4609-9c9e-1a7d858062ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 70,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278979596,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 632,
    "yorig": 544
}