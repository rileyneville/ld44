{
    "id": "3422b727-5ac6-449c-8d38-e6cb8a35b9db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAltar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 9,
    "bbox_right": 55,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bbf6b726-fe93-450f-a91f-3f1719f696d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3422b727-5ac6-449c-8d38-e6cb8a35b9db",
            "compositeImage": {
                "id": "fd85d150-53b0-4d0d-91ff-3ae56ec0ceb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbf6b726-fe93-450f-a91f-3f1719f696d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5bf3d77-7f9c-4f56-9771-6ec2b41c3a8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbf6b726-fe93-450f-a91f-3f1719f696d3",
                    "LayerId": "4eb6d1ff-354f-4483-acf6-bdc76a1f81bc"
                },
                {
                    "id": "7ad69c99-595a-480d-b18a-746687957129",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbf6b726-fe93-450f-a91f-3f1719f696d3",
                    "LayerId": "3b13dc3a-c119-4dbb-8ae1-acecee75354b"
                },
                {
                    "id": "feb38db2-4b9a-487b-ade5-21c3666528de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbf6b726-fe93-450f-a91f-3f1719f696d3",
                    "LayerId": "221a6751-2e08-4fba-94bc-1a3840ce28c6"
                },
                {
                    "id": "c1087078-def7-4a6e-bbcf-d16c6c5ae35d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbf6b726-fe93-450f-a91f-3f1719f696d3",
                    "LayerId": "bf98d5a1-f80a-42c4-b7c1-178a7ac4673f"
                }
            ]
        },
        {
            "id": "b50e87dc-e3a6-4754-b59e-8c2820c24a01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3422b727-5ac6-449c-8d38-e6cb8a35b9db",
            "compositeImage": {
                "id": "469df12a-cfda-4939-8a3e-b549279ce6b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b50e87dc-e3a6-4754-b59e-8c2820c24a01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b664ffb6-51fe-41ca-8822-23807bf5a764",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b50e87dc-e3a6-4754-b59e-8c2820c24a01",
                    "LayerId": "4eb6d1ff-354f-4483-acf6-bdc76a1f81bc"
                },
                {
                    "id": "87f941d1-087c-4a10-98dc-d475a53c4c34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b50e87dc-e3a6-4754-b59e-8c2820c24a01",
                    "LayerId": "3b13dc3a-c119-4dbb-8ae1-acecee75354b"
                },
                {
                    "id": "4f748d2b-90dc-4f6e-9877-c50b595259b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b50e87dc-e3a6-4754-b59e-8c2820c24a01",
                    "LayerId": "221a6751-2e08-4fba-94bc-1a3840ce28c6"
                },
                {
                    "id": "5038a700-5e8f-408b-bd89-0f980b3c6cfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b50e87dc-e3a6-4754-b59e-8c2820c24a01",
                    "LayerId": "bf98d5a1-f80a-42c4-b7c1-178a7ac4673f"
                }
            ]
        },
        {
            "id": "efe6e6e2-6b4f-43fb-aba4-8180e78f0845",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3422b727-5ac6-449c-8d38-e6cb8a35b9db",
            "compositeImage": {
                "id": "41b04f42-e4b5-4b39-9eda-d3e6dafde651",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efe6e6e2-6b4f-43fb-aba4-8180e78f0845",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08f77849-6116-4eec-a4cb-de25b3d20ba3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efe6e6e2-6b4f-43fb-aba4-8180e78f0845",
                    "LayerId": "4eb6d1ff-354f-4483-acf6-bdc76a1f81bc"
                },
                {
                    "id": "e273fa76-dc59-474c-a89c-355ff6745c7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efe6e6e2-6b4f-43fb-aba4-8180e78f0845",
                    "LayerId": "3b13dc3a-c119-4dbb-8ae1-acecee75354b"
                },
                {
                    "id": "6394c47e-ce9d-4a06-816a-1371993ceb06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efe6e6e2-6b4f-43fb-aba4-8180e78f0845",
                    "LayerId": "221a6751-2e08-4fba-94bc-1a3840ce28c6"
                },
                {
                    "id": "bc7c2504-b169-465a-9fe0-8ec60c3bd687",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efe6e6e2-6b4f-43fb-aba4-8180e78f0845",
                    "LayerId": "bf98d5a1-f80a-42c4-b7c1-178a7ac4673f"
                }
            ]
        },
        {
            "id": "d5043b6f-7ab0-4b19-8e1a-f7ae9ea4996e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3422b727-5ac6-449c-8d38-e6cb8a35b9db",
            "compositeImage": {
                "id": "9e25c983-a29b-4a6d-96f5-955abb236873",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5043b6f-7ab0-4b19-8e1a-f7ae9ea4996e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1687b7c5-d69c-4d6d-8add-49f2d6cdcb89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5043b6f-7ab0-4b19-8e1a-f7ae9ea4996e",
                    "LayerId": "4eb6d1ff-354f-4483-acf6-bdc76a1f81bc"
                },
                {
                    "id": "5c359160-5231-423f-9e42-0a44fdd9925b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5043b6f-7ab0-4b19-8e1a-f7ae9ea4996e",
                    "LayerId": "3b13dc3a-c119-4dbb-8ae1-acecee75354b"
                },
                {
                    "id": "5f59fa40-2db3-4e1d-8573-fc0ebb7549b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5043b6f-7ab0-4b19-8e1a-f7ae9ea4996e",
                    "LayerId": "221a6751-2e08-4fba-94bc-1a3840ce28c6"
                },
                {
                    "id": "5890609c-e68d-47ea-9fc3-d8aabbb252d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5043b6f-7ab0-4b19-8e1a-f7ae9ea4996e",
                    "LayerId": "bf98d5a1-f80a-42c4-b7c1-178a7ac4673f"
                }
            ]
        },
        {
            "id": "99fc1b4a-0c9a-4bc0-8673-db43d6c3a972",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3422b727-5ac6-449c-8d38-e6cb8a35b9db",
            "compositeImage": {
                "id": "653debf4-dc8f-48ca-8fa1-5d48ccc9bab3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99fc1b4a-0c9a-4bc0-8673-db43d6c3a972",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa19d4ff-a061-4737-8c30-c9cc52c8bed3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99fc1b4a-0c9a-4bc0-8673-db43d6c3a972",
                    "LayerId": "4eb6d1ff-354f-4483-acf6-bdc76a1f81bc"
                },
                {
                    "id": "f807215c-62cd-4f4c-ae1c-d371167281d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99fc1b4a-0c9a-4bc0-8673-db43d6c3a972",
                    "LayerId": "3b13dc3a-c119-4dbb-8ae1-acecee75354b"
                },
                {
                    "id": "e13743e0-436e-484c-bb2e-13190bd5658c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99fc1b4a-0c9a-4bc0-8673-db43d6c3a972",
                    "LayerId": "221a6751-2e08-4fba-94bc-1a3840ce28c6"
                },
                {
                    "id": "ec04bad1-35dc-4c8e-a46a-25e1775d7095",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99fc1b4a-0c9a-4bc0-8673-db43d6c3a972",
                    "LayerId": "bf98d5a1-f80a-42c4-b7c1-178a7ac4673f"
                }
            ]
        },
        {
            "id": "80b620ce-7561-4af7-b8f5-3d7d887b64b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3422b727-5ac6-449c-8d38-e6cb8a35b9db",
            "compositeImage": {
                "id": "c22eecfb-5a00-4689-a0bc-47ce0d9dc84b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80b620ce-7561-4af7-b8f5-3d7d887b64b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ba7e620-fe12-4db9-8be3-cab2866a0448",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80b620ce-7561-4af7-b8f5-3d7d887b64b6",
                    "LayerId": "4eb6d1ff-354f-4483-acf6-bdc76a1f81bc"
                },
                {
                    "id": "e3fad102-b44d-4d15-bc27-90b4bcb0686c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80b620ce-7561-4af7-b8f5-3d7d887b64b6",
                    "LayerId": "3b13dc3a-c119-4dbb-8ae1-acecee75354b"
                },
                {
                    "id": "50c0fbc5-6f58-4eb2-9558-67c7a63aa7ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80b620ce-7561-4af7-b8f5-3d7d887b64b6",
                    "LayerId": "221a6751-2e08-4fba-94bc-1a3840ce28c6"
                },
                {
                    "id": "6a4f1c27-1121-4ccf-9676-1c5eda0c395b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80b620ce-7561-4af7-b8f5-3d7d887b64b6",
                    "LayerId": "bf98d5a1-f80a-42c4-b7c1-178a7ac4673f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4eb6d1ff-354f-4483-acf6-bdc76a1f81bc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3422b727-5ac6-449c-8d38-e6cb8a35b9db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "bf98d5a1-f80a-42c4-b7c1-178a7ac4673f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3422b727-5ac6-449c-8d38-e6cb8a35b9db",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2 (2) (2)",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "221a6751-2e08-4fba-94bc-1a3840ce28c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3422b727-5ac6-449c-8d38-e6cb8a35b9db",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2 (2)",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "3b13dc3a-c119-4dbb-8ae1-acecee75354b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3422b727-5ac6-449c-8d38-e6cb8a35b9db",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}