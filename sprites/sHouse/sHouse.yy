{
    "id": "ae66f9c6-24b2-42ac-8a10-170948965e97",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHouse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 560,
    "bbox_left": 561,
    "bbox_right": 799,
    "bbox_top": 372,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "086771d0-2837-479a-ac55-d5f0047be1ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae66f9c6-24b2-42ac-8a10-170948965e97",
            "compositeImage": {
                "id": "78b19f10-1fde-484d-a326-481b7575f13e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "086771d0-2837-479a-ac55-d5f0047be1ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "302ea3f7-0247-4763-b4fc-b7411c92e87a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "086771d0-2837-479a-ac55-d5f0047be1ef",
                    "LayerId": "a94cc847-3e54-4825-834c-ba2047ccc322"
                },
                {
                    "id": "236be939-ef61-4de5-aab7-2b436c5001f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "086771d0-2837-479a-ac55-d5f0047be1ef",
                    "LayerId": "154885b8-fe80-4822-ae0f-5834e28b5a54"
                },
                {
                    "id": "99ef698c-0b35-4d8a-a727-9ebfc29e6073",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "086771d0-2837-479a-ac55-d5f0047be1ef",
                    "LayerId": "74d758af-bc39-42f7-a9fb-680a87bce6bf"
                }
            ]
        },
        {
            "id": "7024f50f-0cba-4ddf-ac3b-bc76a17a688f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae66f9c6-24b2-42ac-8a10-170948965e97",
            "compositeImage": {
                "id": "f74e8d3f-651c-49ad-9a1e-5e9e5662d37e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7024f50f-0cba-4ddf-ac3b-bc76a17a688f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c68a6b5d-a147-4de0-bcd3-fe6cdaebbae9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7024f50f-0cba-4ddf-ac3b-bc76a17a688f",
                    "LayerId": "a94cc847-3e54-4825-834c-ba2047ccc322"
                },
                {
                    "id": "16a0ece2-d124-42b6-8d05-0103e08a01b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7024f50f-0cba-4ddf-ac3b-bc76a17a688f",
                    "LayerId": "154885b8-fe80-4822-ae0f-5834e28b5a54"
                },
                {
                    "id": "5d011199-b7a2-4205-9ae4-d69644097cd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7024f50f-0cba-4ddf-ac3b-bc76a17a688f",
                    "LayerId": "74d758af-bc39-42f7-a9fb-680a87bce6bf"
                }
            ]
        },
        {
            "id": "abf1bc1f-72e4-4e6c-8690-2f47ea3810ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae66f9c6-24b2-42ac-8a10-170948965e97",
            "compositeImage": {
                "id": "e22b3e69-0f9c-4ee3-b2c4-9b939d98be0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abf1bc1f-72e4-4e6c-8690-2f47ea3810ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d37367a0-c4f6-4729-8dce-9d5f969cb82a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abf1bc1f-72e4-4e6c-8690-2f47ea3810ba",
                    "LayerId": "a94cc847-3e54-4825-834c-ba2047ccc322"
                },
                {
                    "id": "c4d5cbd2-e6c0-43ff-83a7-9e5a94cb8003",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abf1bc1f-72e4-4e6c-8690-2f47ea3810ba",
                    "LayerId": "154885b8-fe80-4822-ae0f-5834e28b5a54"
                },
                {
                    "id": "de287147-da4f-4fee-812e-040236ffc909",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abf1bc1f-72e4-4e6c-8690-2f47ea3810ba",
                    "LayerId": "74d758af-bc39-42f7-a9fb-680a87bce6bf"
                }
            ]
        },
        {
            "id": "3a89ed59-e78a-4c3a-a5af-3adec42d632f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae66f9c6-24b2-42ac-8a10-170948965e97",
            "compositeImage": {
                "id": "ab345da3-383f-4dab-b084-2fe458d287c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a89ed59-e78a-4c3a-a5af-3adec42d632f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2344522-0e48-4b85-b461-3ca2d4f538db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a89ed59-e78a-4c3a-a5af-3adec42d632f",
                    "LayerId": "a94cc847-3e54-4825-834c-ba2047ccc322"
                },
                {
                    "id": "c4ff77a9-b3df-4f16-9df5-7d88e596a64a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a89ed59-e78a-4c3a-a5af-3adec42d632f",
                    "LayerId": "154885b8-fe80-4822-ae0f-5834e28b5a54"
                },
                {
                    "id": "abebacb8-10a5-4a42-a04a-6a700097512e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a89ed59-e78a-4c3a-a5af-3adec42d632f",
                    "LayerId": "74d758af-bc39-42f7-a9fb-680a87bce6bf"
                }
            ]
        },
        {
            "id": "ac841704-421f-4fe5-ba98-bebfcab995e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae66f9c6-24b2-42ac-8a10-170948965e97",
            "compositeImage": {
                "id": "452e9817-590e-4e09-8575-50e9bf204ac7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac841704-421f-4fe5-ba98-bebfcab995e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c98cf29-8a1e-41dc-b92e-86d0aa42a9d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac841704-421f-4fe5-ba98-bebfcab995e7",
                    "LayerId": "a94cc847-3e54-4825-834c-ba2047ccc322"
                },
                {
                    "id": "214ec48c-ad84-4fa8-bcf9-1185fb1b4ae3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac841704-421f-4fe5-ba98-bebfcab995e7",
                    "LayerId": "154885b8-fe80-4822-ae0f-5834e28b5a54"
                },
                {
                    "id": "c22023f9-880e-4312-8a06-0051a97bd9a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac841704-421f-4fe5-ba98-bebfcab995e7",
                    "LayerId": "74d758af-bc39-42f7-a9fb-680a87bce6bf"
                }
            ]
        },
        {
            "id": "7f857033-9970-40fa-983e-0c90005bdb1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae66f9c6-24b2-42ac-8a10-170948965e97",
            "compositeImage": {
                "id": "a565e326-9cfb-4c13-ac7b-47ab684665e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f857033-9970-40fa-983e-0c90005bdb1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ee570e5-2bbf-4df0-ae11-153d728e9e66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f857033-9970-40fa-983e-0c90005bdb1f",
                    "LayerId": "a94cc847-3e54-4825-834c-ba2047ccc322"
                },
                {
                    "id": "dfb90b03-3c63-4838-9431-22a6704d5737",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f857033-9970-40fa-983e-0c90005bdb1f",
                    "LayerId": "154885b8-fe80-4822-ae0f-5834e28b5a54"
                },
                {
                    "id": "82774c13-6d10-4d53-a084-7a5ab8a24241",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f857033-9970-40fa-983e-0c90005bdb1f",
                    "LayerId": "74d758af-bc39-42f7-a9fb-680a87bce6bf"
                }
            ]
        },
        {
            "id": "2382616b-68d5-478f-a97e-c878c11839e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae66f9c6-24b2-42ac-8a10-170948965e97",
            "compositeImage": {
                "id": "b57985a5-a07a-44ac-9827-5c00d7910b3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2382616b-68d5-478f-a97e-c878c11839e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17708ab5-3e96-42fd-909b-2247a7946c9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2382616b-68d5-478f-a97e-c878c11839e1",
                    "LayerId": "a94cc847-3e54-4825-834c-ba2047ccc322"
                },
                {
                    "id": "f1b38b36-2cef-454d-9481-39ba2f6beec1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2382616b-68d5-478f-a97e-c878c11839e1",
                    "LayerId": "154885b8-fe80-4822-ae0f-5834e28b5a54"
                },
                {
                    "id": "ab85beeb-ee75-46f1-a0f5-f0f24e7405c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2382616b-68d5-478f-a97e-c878c11839e1",
                    "LayerId": "74d758af-bc39-42f7-a9fb-680a87bce6bf"
                }
            ]
        },
        {
            "id": "b6e30244-8a57-4df7-81f6-2625b2863e69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae66f9c6-24b2-42ac-8a10-170948965e97",
            "compositeImage": {
                "id": "c5054980-5ede-4d6f-8083-30dae4681636",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6e30244-8a57-4df7-81f6-2625b2863e69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd1bdcf8-fb25-4547-b03e-437b623b2ac0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6e30244-8a57-4df7-81f6-2625b2863e69",
                    "LayerId": "a94cc847-3e54-4825-834c-ba2047ccc322"
                },
                {
                    "id": "b024355e-b234-49d6-bc67-cc04a4c287d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6e30244-8a57-4df7-81f6-2625b2863e69",
                    "LayerId": "154885b8-fe80-4822-ae0f-5834e28b5a54"
                },
                {
                    "id": "264113ad-8e0e-4d0a-9a5a-b8b290968292",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6e30244-8a57-4df7-81f6-2625b2863e69",
                    "LayerId": "74d758af-bc39-42f7-a9fb-680a87bce6bf"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 600,
    "layers": [
        {
            "id": "74d758af-bc39-42f7-a9fb-680a87bce6bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae66f9c6-24b2-42ac-8a10-170948965e97",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1 (2)",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "154885b8-fe80-4822-ae0f-5834e28b5a54",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae66f9c6-24b2-42ac-8a10-170948965e97",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 70,
            "visible": true
        },
        {
            "id": "a94cc847-3e54-4825-834c-ba2047ccc322",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae66f9c6-24b2-42ac-8a10-170948965e97",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278979596,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 632,
    "yorig": 544
}