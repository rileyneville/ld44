{
    "id": "904e4f94-d7da-4067-855e-65032bf64132",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCauldron_large_splash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 8,
    "bbox_right": 55,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "970b63d9-6c8d-41ee-ad1a-ff37ea609eb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "904e4f94-d7da-4067-855e-65032bf64132",
            "compositeImage": {
                "id": "a6f9471c-f8c5-4a33-b45c-5ebb87fa90f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "970b63d9-6c8d-41ee-ad1a-ff37ea609eb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0d267cc-b6ad-4f21-b675-8ccc61d26bf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "970b63d9-6c8d-41ee-ad1a-ff37ea609eb4",
                    "LayerId": "37891e90-fd70-43ec-a6ad-6357fbf8afa6"
                }
            ]
        },
        {
            "id": "27b1130b-daa2-48b0-8853-3ec29519b10b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "904e4f94-d7da-4067-855e-65032bf64132",
            "compositeImage": {
                "id": "009d7d95-c5a9-4c0c-81cc-b4423ec59242",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27b1130b-daa2-48b0-8853-3ec29519b10b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a92a32c2-5a96-4bf6-84f1-e1c1acbeaa42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27b1130b-daa2-48b0-8853-3ec29519b10b",
                    "LayerId": "37891e90-fd70-43ec-a6ad-6357fbf8afa6"
                }
            ]
        },
        {
            "id": "b058d335-9598-43d1-afd8-a17f614825de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "904e4f94-d7da-4067-855e-65032bf64132",
            "compositeImage": {
                "id": "6173aa95-94d0-43a0-bba3-afab9f62c67c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b058d335-9598-43d1-afd8-a17f614825de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c07c99a3-2059-4713-810f-4f0d2e8612c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b058d335-9598-43d1-afd8-a17f614825de",
                    "LayerId": "37891e90-fd70-43ec-a6ad-6357fbf8afa6"
                }
            ]
        },
        {
            "id": "a3b3adee-163f-4897-a334-f7dc222d3743",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "904e4f94-d7da-4067-855e-65032bf64132",
            "compositeImage": {
                "id": "3c55f97f-a923-48f2-a49f-d7ebc299e0a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3b3adee-163f-4897-a334-f7dc222d3743",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d452e02a-f32f-44d1-af33-cf22a833244a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3b3adee-163f-4897-a334-f7dc222d3743",
                    "LayerId": "37891e90-fd70-43ec-a6ad-6357fbf8afa6"
                }
            ]
        },
        {
            "id": "cbd27627-b6de-40f1-a110-90cd9872dea0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "904e4f94-d7da-4067-855e-65032bf64132",
            "compositeImage": {
                "id": "beb0c53b-eb8e-40f8-8183-872184f7578d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbd27627-b6de-40f1-a110-90cd9872dea0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be5441dc-f52d-4593-93ec-32790f93a270",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbd27627-b6de-40f1-a110-90cd9872dea0",
                    "LayerId": "37891e90-fd70-43ec-a6ad-6357fbf8afa6"
                }
            ]
        },
        {
            "id": "d1811876-ed00-44f7-9b4a-20849720a12b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "904e4f94-d7da-4067-855e-65032bf64132",
            "compositeImage": {
                "id": "6e122144-77d1-440b-b21d-c349f8b7fc99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1811876-ed00-44f7-9b4a-20849720a12b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bbd8e19-44e3-4d0b-aed7-1dfa2a35d7d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1811876-ed00-44f7-9b4a-20849720a12b",
                    "LayerId": "37891e90-fd70-43ec-a6ad-6357fbf8afa6"
                }
            ]
        },
        {
            "id": "eff8d7f2-4b5f-4486-a7a2-ea4f3935ce79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "904e4f94-d7da-4067-855e-65032bf64132",
            "compositeImage": {
                "id": "ac009a48-fbab-49e3-8514-2fa7b256658b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eff8d7f2-4b5f-4486-a7a2-ea4f3935ce79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c1accc3-7bcc-45c5-bfd4-45cfa48efba6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eff8d7f2-4b5f-4486-a7a2-ea4f3935ce79",
                    "LayerId": "37891e90-fd70-43ec-a6ad-6357fbf8afa6"
                }
            ]
        },
        {
            "id": "b48456e2-6c9a-4684-8856-9d64c7cb4aac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "904e4f94-d7da-4067-855e-65032bf64132",
            "compositeImage": {
                "id": "9171f100-4711-4a3b-b579-d8afaed14137",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b48456e2-6c9a-4684-8856-9d64c7cb4aac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61be7a3b-30c7-4d05-8c16-75a47bd5f71a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b48456e2-6c9a-4684-8856-9d64c7cb4aac",
                    "LayerId": "37891e90-fd70-43ec-a6ad-6357fbf8afa6"
                }
            ]
        },
        {
            "id": "468c6838-8eda-4da8-9401-7973b81e07c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "904e4f94-d7da-4067-855e-65032bf64132",
            "compositeImage": {
                "id": "c66a4225-876d-4ed7-8a41-94e5cf7d7c96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "468c6838-8eda-4da8-9401-7973b81e07c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "367559b1-a490-4949-ba9b-797b8f814c67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "468c6838-8eda-4da8-9401-7973b81e07c2",
                    "LayerId": "37891e90-fd70-43ec-a6ad-6357fbf8afa6"
                }
            ]
        },
        {
            "id": "227f026a-e7d0-4865-a7c6-f0432b88d52f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "904e4f94-d7da-4067-855e-65032bf64132",
            "compositeImage": {
                "id": "11133731-d02e-4b22-8e7d-1f51b582f93f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "227f026a-e7d0-4865-a7c6-f0432b88d52f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0e63a62-88e3-44e7-942a-a3911669f8f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "227f026a-e7d0-4865-a7c6-f0432b88d52f",
                    "LayerId": "37891e90-fd70-43ec-a6ad-6357fbf8afa6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "37891e90-fd70-43ec-a6ad-6357fbf8afa6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "904e4f94-d7da-4067-855e-65032bf64132",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 16
}