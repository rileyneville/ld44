{
    "id": "60b22275-d801-4ac6-8251-0a1aafec2316",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHuman_reincarnate",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 7,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "786f4e0a-fe2d-424c-b69d-dc2903150fa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60b22275-d801-4ac6-8251-0a1aafec2316",
            "compositeImage": {
                "id": "6abdfaf4-9f95-4a8d-8eeb-5bae244bb283",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "786f4e0a-fe2d-424c-b69d-dc2903150fa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7424ef7b-8b93-46fd-a927-c9acf2f8dac8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "786f4e0a-fe2d-424c-b69d-dc2903150fa5",
                    "LayerId": "bc973c4b-8122-4ca3-9fe8-bebde9a91bfc"
                }
            ]
        },
        {
            "id": "441803c7-1052-45d9-9aeb-8f5934681703",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60b22275-d801-4ac6-8251-0a1aafec2316",
            "compositeImage": {
                "id": "dea4f8fd-20f5-4537-bf71-23b45c029564",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "441803c7-1052-45d9-9aeb-8f5934681703",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd8d42f6-de0c-40ca-844d-e3994a8c2256",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "441803c7-1052-45d9-9aeb-8f5934681703",
                    "LayerId": "bc973c4b-8122-4ca3-9fe8-bebde9a91bfc"
                }
            ]
        },
        {
            "id": "9d97b14c-d67c-4096-972b-e909515fcdc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60b22275-d801-4ac6-8251-0a1aafec2316",
            "compositeImage": {
                "id": "8a7312e5-cef3-48da-83aa-650bb14b0a21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d97b14c-d67c-4096-972b-e909515fcdc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bf0ac2c-6968-4005-b5e3-ba36f7405de8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d97b14c-d67c-4096-972b-e909515fcdc3",
                    "LayerId": "bc973c4b-8122-4ca3-9fe8-bebde9a91bfc"
                }
            ]
        },
        {
            "id": "9deefc89-3304-4cb5-a628-0ac847fda129",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60b22275-d801-4ac6-8251-0a1aafec2316",
            "compositeImage": {
                "id": "03b19419-0601-439f-8757-d24f0d74c23b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9deefc89-3304-4cb5-a628-0ac847fda129",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd6f46e7-659d-4816-94de-a428d4aaed14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9deefc89-3304-4cb5-a628-0ac847fda129",
                    "LayerId": "bc973c4b-8122-4ca3-9fe8-bebde9a91bfc"
                }
            ]
        },
        {
            "id": "fc41fcab-4d34-4f1f-82d9-b379c95ce443",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60b22275-d801-4ac6-8251-0a1aafec2316",
            "compositeImage": {
                "id": "828d2165-1373-459d-b47a-d869465fcfc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc41fcab-4d34-4f1f-82d9-b379c95ce443",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3a32379-765c-4c18-918b-f3e425992a56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc41fcab-4d34-4f1f-82d9-b379c95ce443",
                    "LayerId": "bc973c4b-8122-4ca3-9fe8-bebde9a91bfc"
                }
            ]
        },
        {
            "id": "2ed2d7ce-5bdc-47fa-997a-d6d59b1ad81e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60b22275-d801-4ac6-8251-0a1aafec2316",
            "compositeImage": {
                "id": "aab6f1fa-7261-4905-8a00-a6417251490f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ed2d7ce-5bdc-47fa-997a-d6d59b1ad81e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f99db42-b239-4690-ae92-bb7a89ba59e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ed2d7ce-5bdc-47fa-997a-d6d59b1ad81e",
                    "LayerId": "bc973c4b-8122-4ca3-9fe8-bebde9a91bfc"
                }
            ]
        },
        {
            "id": "2d6196e6-74b7-4d53-9be9-f8009c3ac9f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60b22275-d801-4ac6-8251-0a1aafec2316",
            "compositeImage": {
                "id": "e6c22f6a-96b3-4448-aa2a-4dd304da3109",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d6196e6-74b7-4d53-9be9-f8009c3ac9f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71f9fbea-7198-4199-9cde-10ed91fb3cdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d6196e6-74b7-4d53-9be9-f8009c3ac9f5",
                    "LayerId": "bc973c4b-8122-4ca3-9fe8-bebde9a91bfc"
                }
            ]
        },
        {
            "id": "3867c18a-c87d-4a79-b804-38ac75b9f4eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60b22275-d801-4ac6-8251-0a1aafec2316",
            "compositeImage": {
                "id": "27a400c5-6114-41bb-801d-ec05a1b06fc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3867c18a-c87d-4a79-b804-38ac75b9f4eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63f2f166-98e5-4005-b602-feea0c7d28ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3867c18a-c87d-4a79-b804-38ac75b9f4eb",
                    "LayerId": "bc973c4b-8122-4ca3-9fe8-bebde9a91bfc"
                }
            ]
        },
        {
            "id": "8e5d48be-31d1-491f-9977-43f77c296517",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60b22275-d801-4ac6-8251-0a1aafec2316",
            "compositeImage": {
                "id": "0589c322-9efd-45e3-a762-737b7eeeb82d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e5d48be-31d1-491f-9977-43f77c296517",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae0ae073-75d1-46a7-b06b-2f4944339a65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e5d48be-31d1-491f-9977-43f77c296517",
                    "LayerId": "bc973c4b-8122-4ca3-9fe8-bebde9a91bfc"
                }
            ]
        },
        {
            "id": "3c7cf732-dc4d-4c2a-9aaa-9bebba5d8c21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60b22275-d801-4ac6-8251-0a1aafec2316",
            "compositeImage": {
                "id": "f4250129-716e-4d93-b9f9-dbad7a012a76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c7cf732-dc4d-4c2a-9aaa-9bebba5d8c21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5f556f3-c611-47a1-97dd-e3a670392d11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c7cf732-dc4d-4c2a-9aaa-9bebba5d8c21",
                    "LayerId": "bc973c4b-8122-4ca3-9fe8-bebde9a91bfc"
                }
            ]
        },
        {
            "id": "149e5a45-6668-4b85-adfb-ee096c017587",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60b22275-d801-4ac6-8251-0a1aafec2316",
            "compositeImage": {
                "id": "ee5b2d42-35aa-4659-8a93-82a97b2ca0fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "149e5a45-6668-4b85-adfb-ee096c017587",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fab47ef3-0816-4483-bf16-27bb81dff478",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "149e5a45-6668-4b85-adfb-ee096c017587",
                    "LayerId": "bc973c4b-8122-4ca3-9fe8-bebde9a91bfc"
                }
            ]
        },
        {
            "id": "6ea29f55-6607-42f9-abde-87fabff88864",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60b22275-d801-4ac6-8251-0a1aafec2316",
            "compositeImage": {
                "id": "d8f06627-f3df-41c3-ba10-638e0fa215b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ea29f55-6607-42f9-abde-87fabff88864",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e525ac30-70d4-4a40-aca8-cc5f038f4001",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ea29f55-6607-42f9-abde-87fabff88864",
                    "LayerId": "bc973c4b-8122-4ca3-9fe8-bebde9a91bfc"
                }
            ]
        },
        {
            "id": "72887bd2-7868-4ef8-9aeb-4ae9545a85be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60b22275-d801-4ac6-8251-0a1aafec2316",
            "compositeImage": {
                "id": "895e17ff-96f8-4adc-a7cc-d840b8b034ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72887bd2-7868-4ef8-9aeb-4ae9545a85be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "907b886b-9d60-48f6-99f9-5d0fcba58f7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72887bd2-7868-4ef8-9aeb-4ae9545a85be",
                    "LayerId": "bc973c4b-8122-4ca3-9fe8-bebde9a91bfc"
                }
            ]
        },
        {
            "id": "22afc283-389f-4db4-aec0-5265a0b1a103",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60b22275-d801-4ac6-8251-0a1aafec2316",
            "compositeImage": {
                "id": "e4e924f9-9d61-446e-8a61-83daeeccfdb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22afc283-389f-4db4-aec0-5265a0b1a103",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "763be8f8-6d55-45cf-a5f6-ca41cbc6ee0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22afc283-389f-4db4-aec0-5265a0b1a103",
                    "LayerId": "bc973c4b-8122-4ca3-9fe8-bebde9a91bfc"
                }
            ]
        },
        {
            "id": "946fd71a-665b-44f1-bf94-b15921dfd105",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60b22275-d801-4ac6-8251-0a1aafec2316",
            "compositeImage": {
                "id": "b3353a71-4b48-4d53-b6c0-ec54b5fbca93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "946fd71a-665b-44f1-bf94-b15921dfd105",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0f6322f-39ca-438b-abfd-840808d9c1b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "946fd71a-665b-44f1-bf94-b15921dfd105",
                    "LayerId": "bc973c4b-8122-4ca3-9fe8-bebde9a91bfc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bc973c4b-8122-4ca3-9fe8-bebde9a91bfc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60b22275-d801-4ac6-8251-0a1aafec2316",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}