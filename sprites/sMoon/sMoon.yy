{
    "id": "53e3bcd1-3962-4453-abb6-f0ffdcc36c3b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMoon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 157,
    "bbox_left": 638,
    "bbox_right": 776,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b613b0b2-d4c1-4cbf-807f-fd52dc1eedbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53e3bcd1-3962-4453-abb6-f0ffdcc36c3b",
            "compositeImage": {
                "id": "7e234dce-9672-46a4-88ad-eb5bc0322553",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b613b0b2-d4c1-4cbf-807f-fd52dc1eedbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65f3ed05-fb12-4e2b-8192-116c178eb26f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b613b0b2-d4c1-4cbf-807f-fd52dc1eedbd",
                    "LayerId": "3ac5a022-01bf-4103-aa21-2417c45bedd8"
                },
                {
                    "id": "204decff-1dbf-49e1-a302-33530b4181a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b613b0b2-d4c1-4cbf-807f-fd52dc1eedbd",
                    "LayerId": "7f74093f-41c4-46a9-8b3f-4446734f4ff4"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 600,
    "layers": [
        {
            "id": "3ac5a022-01bf-4103-aa21-2417c45bedd8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53e3bcd1-3962-4453-abb6-f0ffdcc36c3b",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 4 (2)",
            "opacity": 50,
            "visible": true
        },
        {
            "id": "7f74093f-41c4-46a9-8b3f-4446734f4ff4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53e3bcd1-3962-4453-abb6-f0ffdcc36c3b",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 4",
            "opacity": 78,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}