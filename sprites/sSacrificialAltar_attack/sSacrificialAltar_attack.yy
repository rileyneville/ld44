{
    "id": "589cfac6-f78a-45e7-a458-760d8bbdaa1f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSacrificialAltar_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 37,
    "bbox_right": 483,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "73e38956-797d-4ad8-b1fe-157e1bd60d5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "589cfac6-f78a-45e7-a458-760d8bbdaa1f",
            "compositeImage": {
                "id": "abf2e492-edc9-47c0-8235-9c283c7b868a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73e38956-797d-4ad8-b1fe-157e1bd60d5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9766afa7-e4f1-4cfb-85ad-874575d64909",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73e38956-797d-4ad8-b1fe-157e1bd60d5a",
                    "LayerId": "a34f0620-584c-4375-a1b5-a6afc7a9dd71"
                },
                {
                    "id": "db89c3e0-0b1d-4969-8734-3135b0cc3a7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73e38956-797d-4ad8-b1fe-157e1bd60d5a",
                    "LayerId": "5ee00ec0-7ce5-48e3-b1dd-8968169ffb23"
                }
            ]
        },
        {
            "id": "3df825f2-a5e3-41f8-874f-c0f8303ef854",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "589cfac6-f78a-45e7-a458-760d8bbdaa1f",
            "compositeImage": {
                "id": "4aee6750-e7ee-4d09-86fa-fe32df92e007",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3df825f2-a5e3-41f8-874f-c0f8303ef854",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ef66341-91cc-476d-99ca-70a714fd9730",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3df825f2-a5e3-41f8-874f-c0f8303ef854",
                    "LayerId": "a34f0620-584c-4375-a1b5-a6afc7a9dd71"
                },
                {
                    "id": "c1378200-85fa-4527-a0ce-bdf5c1c150d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3df825f2-a5e3-41f8-874f-c0f8303ef854",
                    "LayerId": "5ee00ec0-7ce5-48e3-b1dd-8968169ffb23"
                }
            ]
        },
        {
            "id": "237de1a6-11fa-4fdb-9fe9-5cc3b36cfacb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "589cfac6-f78a-45e7-a458-760d8bbdaa1f",
            "compositeImage": {
                "id": "07490b20-6ffc-48d7-bbaf-a4b8c2366c6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "237de1a6-11fa-4fdb-9fe9-5cc3b36cfacb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c629196-ba6c-4069-bf2a-421b81c86e91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "237de1a6-11fa-4fdb-9fe9-5cc3b36cfacb",
                    "LayerId": "a34f0620-584c-4375-a1b5-a6afc7a9dd71"
                },
                {
                    "id": "103e2c03-c95f-4211-9c0f-9047f599dd42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "237de1a6-11fa-4fdb-9fe9-5cc3b36cfacb",
                    "LayerId": "5ee00ec0-7ce5-48e3-b1dd-8968169ffb23"
                }
            ]
        },
        {
            "id": "4a403a4a-2720-4683-9234-0fdadf927b4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "589cfac6-f78a-45e7-a458-760d8bbdaa1f",
            "compositeImage": {
                "id": "daa3c439-a444-4f06-8760-66bfe47c527f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a403a4a-2720-4683-9234-0fdadf927b4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c5d09b0-15f8-4c2d-8a55-e279076167e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a403a4a-2720-4683-9234-0fdadf927b4a",
                    "LayerId": "a34f0620-584c-4375-a1b5-a6afc7a9dd71"
                },
                {
                    "id": "91122486-127c-4edf-a964-2ce3283c0b53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a403a4a-2720-4683-9234-0fdadf927b4a",
                    "LayerId": "5ee00ec0-7ce5-48e3-b1dd-8968169ffb23"
                }
            ]
        },
        {
            "id": "d87f28aa-dfd7-40f4-bffc-fb316d7e7a7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "589cfac6-f78a-45e7-a458-760d8bbdaa1f",
            "compositeImage": {
                "id": "b29beb02-48e5-4c29-a9f5-a647a3e92cec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d87f28aa-dfd7-40f4-bffc-fb316d7e7a7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e06a10f-8ddd-44fe-916e-f858ccf17641",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d87f28aa-dfd7-40f4-bffc-fb316d7e7a7d",
                    "LayerId": "a34f0620-584c-4375-a1b5-a6afc7a9dd71"
                },
                {
                    "id": "9d7372d6-6dc2-4792-8074-a0a8e59f4d38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d87f28aa-dfd7-40f4-bffc-fb316d7e7a7d",
                    "LayerId": "5ee00ec0-7ce5-48e3-b1dd-8968169ffb23"
                }
            ]
        },
        {
            "id": "1512779e-e1c5-4a8f-af0d-37f3b4dfff96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "589cfac6-f78a-45e7-a458-760d8bbdaa1f",
            "compositeImage": {
                "id": "f9c373ed-57fd-4d8b-a7e1-7f8c2e7484a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1512779e-e1c5-4a8f-af0d-37f3b4dfff96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59008d5c-bc48-4812-aa5c-436c22654011",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1512779e-e1c5-4a8f-af0d-37f3b4dfff96",
                    "LayerId": "a34f0620-584c-4375-a1b5-a6afc7a9dd71"
                },
                {
                    "id": "3d126541-dc08-41a2-9310-4ad3a02b04c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1512779e-e1c5-4a8f-af0d-37f3b4dfff96",
                    "LayerId": "5ee00ec0-7ce5-48e3-b1dd-8968169ffb23"
                }
            ]
        },
        {
            "id": "3e084938-9851-4b27-a799-dfec28ab1e1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "589cfac6-f78a-45e7-a458-760d8bbdaa1f",
            "compositeImage": {
                "id": "bc0fc17c-9d87-43c4-b809-91abfed83caf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e084938-9851-4b27-a799-dfec28ab1e1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82c9e520-96fe-4751-84f5-a1be0fe4d099",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e084938-9851-4b27-a799-dfec28ab1e1e",
                    "LayerId": "a34f0620-584c-4375-a1b5-a6afc7a9dd71"
                },
                {
                    "id": "458d8b36-f011-4378-9085-097b7cb1c879",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e084938-9851-4b27-a799-dfec28ab1e1e",
                    "LayerId": "5ee00ec0-7ce5-48e3-b1dd-8968169ffb23"
                }
            ]
        },
        {
            "id": "e4f72bd0-b878-4744-bda1-ef30f89f2000",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "589cfac6-f78a-45e7-a458-760d8bbdaa1f",
            "compositeImage": {
                "id": "ef004d3d-f9f1-495b-9133-f257a0857aeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4f72bd0-b878-4744-bda1-ef30f89f2000",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13cc11df-8986-449e-aa69-6a08cfb535b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4f72bd0-b878-4744-bda1-ef30f89f2000",
                    "LayerId": "a34f0620-584c-4375-a1b5-a6afc7a9dd71"
                },
                {
                    "id": "f1c8cb1b-56ac-49de-8e83-3dc7ef115967",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4f72bd0-b878-4744-bda1-ef30f89f2000",
                    "LayerId": "5ee00ec0-7ce5-48e3-b1dd-8968169ffb23"
                }
            ]
        },
        {
            "id": "3988b2e4-e3d2-4af2-908e-99f0a1ff51cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "589cfac6-f78a-45e7-a458-760d8bbdaa1f",
            "compositeImage": {
                "id": "c63080da-d9a9-424f-a079-8f8ca87c8ad0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3988b2e4-e3d2-4af2-908e-99f0a1ff51cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8c30329-3403-4bbf-98cc-eec336bbc39b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3988b2e4-e3d2-4af2-908e-99f0a1ff51cc",
                    "LayerId": "a34f0620-584c-4375-a1b5-a6afc7a9dd71"
                },
                {
                    "id": "6e6e0b61-b0e6-48fb-a9ca-c65100231c1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3988b2e4-e3d2-4af2-908e-99f0a1ff51cc",
                    "LayerId": "5ee00ec0-7ce5-48e3-b1dd-8968169ffb23"
                }
            ]
        },
        {
            "id": "f7988788-72dc-47f0-8ce1-80dfac76b968",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "589cfac6-f78a-45e7-a458-760d8bbdaa1f",
            "compositeImage": {
                "id": "a6adfa0f-d445-4e3e-b4d1-7067ca37e170",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7988788-72dc-47f0-8ce1-80dfac76b968",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "487ae316-bec3-4015-8acd-ab63f2db70a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7988788-72dc-47f0-8ce1-80dfac76b968",
                    "LayerId": "a34f0620-584c-4375-a1b5-a6afc7a9dd71"
                },
                {
                    "id": "3be90627-134f-4518-b696-1ddd207f31c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7988788-72dc-47f0-8ce1-80dfac76b968",
                    "LayerId": "5ee00ec0-7ce5-48e3-b1dd-8968169ffb23"
                }
            ]
        },
        {
            "id": "9ee282c2-acbf-4631-8e13-64da604d25bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "589cfac6-f78a-45e7-a458-760d8bbdaa1f",
            "compositeImage": {
                "id": "7e96f703-caf6-4e87-b4ed-c434d5e2beed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ee282c2-acbf-4631-8e13-64da604d25bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61130eb2-9619-4190-97ad-350e84417ea3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ee282c2-acbf-4631-8e13-64da604d25bf",
                    "LayerId": "a34f0620-584c-4375-a1b5-a6afc7a9dd71"
                },
                {
                    "id": "ae165b97-332c-4c85-b4d4-25b10de2b15a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ee282c2-acbf-4631-8e13-64da604d25bf",
                    "LayerId": "5ee00ec0-7ce5-48e3-b1dd-8968169ffb23"
                }
            ]
        },
        {
            "id": "6a7ea2b1-ebb3-4bf2-be35-d2f5e8aebb38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "589cfac6-f78a-45e7-a458-760d8bbdaa1f",
            "compositeImage": {
                "id": "3bceed02-625d-449b-909f-e5ca4b0cebb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a7ea2b1-ebb3-4bf2-be35-d2f5e8aebb38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00bd79c5-907c-4aca-921e-7c46047cdc88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a7ea2b1-ebb3-4bf2-be35-d2f5e8aebb38",
                    "LayerId": "a34f0620-584c-4375-a1b5-a6afc7a9dd71"
                },
                {
                    "id": "8b5346af-41e0-4d20-8f99-9e629cd305cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a7ea2b1-ebb3-4bf2-be35-d2f5e8aebb38",
                    "LayerId": "5ee00ec0-7ce5-48e3-b1dd-8968169ffb23"
                }
            ]
        },
        {
            "id": "38953b5c-760b-446a-9496-5ae994ada46d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "589cfac6-f78a-45e7-a458-760d8bbdaa1f",
            "compositeImage": {
                "id": "8d1e0218-92d7-4b02-a34b-aa7339d6a9f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38953b5c-760b-446a-9496-5ae994ada46d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a24131c4-9714-4257-9a57-d7bfcd2c1f65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38953b5c-760b-446a-9496-5ae994ada46d",
                    "LayerId": "a34f0620-584c-4375-a1b5-a6afc7a9dd71"
                },
                {
                    "id": "e30733a5-174e-45ad-b1ec-ac37c2aca6b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38953b5c-760b-446a-9496-5ae994ada46d",
                    "LayerId": "5ee00ec0-7ce5-48e3-b1dd-8968169ffb23"
                }
            ]
        },
        {
            "id": "42dd08c4-909a-4b72-a807-a1f780d83c6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "589cfac6-f78a-45e7-a458-760d8bbdaa1f",
            "compositeImage": {
                "id": "c2a32741-8c63-4107-91e0-fe89642ce58e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42dd08c4-909a-4b72-a807-a1f780d83c6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4970a731-9628-4ea0-aa83-45abc29ff3a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42dd08c4-909a-4b72-a807-a1f780d83c6c",
                    "LayerId": "a34f0620-584c-4375-a1b5-a6afc7a9dd71"
                },
                {
                    "id": "88985b52-037a-4c21-a24f-11018c8973b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42dd08c4-909a-4b72-a807-a1f780d83c6c",
                    "LayerId": "5ee00ec0-7ce5-48e3-b1dd-8968169ffb23"
                }
            ]
        },
        {
            "id": "dbb7e208-1056-449a-8829-bd772b67f16e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "589cfac6-f78a-45e7-a458-760d8bbdaa1f",
            "compositeImage": {
                "id": "148ade86-b006-4234-b1bb-241043601f09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbb7e208-1056-449a-8829-bd772b67f16e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5f44df4-5e89-4add-8e8e-8e8470d51d91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbb7e208-1056-449a-8829-bd772b67f16e",
                    "LayerId": "a34f0620-584c-4375-a1b5-a6afc7a9dd71"
                },
                {
                    "id": "d4b62984-082e-4043-ba1b-449d1e7a3de6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbb7e208-1056-449a-8829-bd772b67f16e",
                    "LayerId": "5ee00ec0-7ce5-48e3-b1dd-8968169ffb23"
                }
            ]
        },
        {
            "id": "b2041024-2ba3-4328-bb9f-1ac97a0a704c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "589cfac6-f78a-45e7-a458-760d8bbdaa1f",
            "compositeImage": {
                "id": "a9ef43c0-9b1d-4152-a067-d9eb62beafd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2041024-2ba3-4328-bb9f-1ac97a0a704c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edd3aa9c-2ecc-45ae-ad65-89f3fc0379b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2041024-2ba3-4328-bb9f-1ac97a0a704c",
                    "LayerId": "a34f0620-584c-4375-a1b5-a6afc7a9dd71"
                },
                {
                    "id": "3dd2fdcb-f447-4a48-bc8b-14e638ac1a2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2041024-2ba3-4328-bb9f-1ac97a0a704c",
                    "LayerId": "5ee00ec0-7ce5-48e3-b1dd-8968169ffb23"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a34f0620-584c-4375-a1b5-a6afc7a9dd71",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "589cfac6-f78a-45e7-a458-760d8bbdaa1f",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "5ee00ec0-7ce5-48e3-b1dd-8968169ffb23",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "589cfac6-f78a-45e7-a458-760d8bbdaa1f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 256,
    "yorig": 32
}