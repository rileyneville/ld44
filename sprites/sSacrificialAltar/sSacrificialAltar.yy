{
    "id": "74d6902d-3a81-4170-aa3e-819ae65032ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSacrificialAltar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 16,
    "bbox_right": 47,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d908a8f5-7d20-45ad-9281-3dfba4f11896",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d6902d-3a81-4170-aa3e-819ae65032ec",
            "compositeImage": {
                "id": "49b601a7-364e-4697-a878-ebf932aadf7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d908a8f5-7d20-45ad-9281-3dfba4f11896",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0e2173b-ce7a-4dcd-a880-e22a5bdd571d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d908a8f5-7d20-45ad-9281-3dfba4f11896",
                    "LayerId": "0d4485a1-947c-4bf3-aa88-9acd79bcea5a"
                },
                {
                    "id": "2d2f5fe3-6d95-44f6-a742-a79f5e07074f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d908a8f5-7d20-45ad-9281-3dfba4f11896",
                    "LayerId": "0ec3648d-0808-4caa-beba-9bbcca7243f9"
                }
            ]
        },
        {
            "id": "d81407dd-e3c4-4af0-9c1e-a56d55852fdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d6902d-3a81-4170-aa3e-819ae65032ec",
            "compositeImage": {
                "id": "f99a7166-192c-48d9-bf83-ce91971710fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d81407dd-e3c4-4af0-9c1e-a56d55852fdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5081136-db7d-44f8-bbec-6de0991a4ed0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d81407dd-e3c4-4af0-9c1e-a56d55852fdb",
                    "LayerId": "0d4485a1-947c-4bf3-aa88-9acd79bcea5a"
                },
                {
                    "id": "8f161e91-0cc7-47b9-8d7f-ef1bc2da6242",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d81407dd-e3c4-4af0-9c1e-a56d55852fdb",
                    "LayerId": "0ec3648d-0808-4caa-beba-9bbcca7243f9"
                }
            ]
        },
        {
            "id": "27025652-d3e1-4ae0-9384-81a5657f7a8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d6902d-3a81-4170-aa3e-819ae65032ec",
            "compositeImage": {
                "id": "4c0c1d3c-22ae-4bb5-9bb2-51b7fdb0a26a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27025652-d3e1-4ae0-9384-81a5657f7a8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01750c1a-88c9-438a-8a10-2d324947dbb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27025652-d3e1-4ae0-9384-81a5657f7a8d",
                    "LayerId": "0d4485a1-947c-4bf3-aa88-9acd79bcea5a"
                },
                {
                    "id": "3d00c2a5-cc99-4d84-be58-ba12be3e459a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27025652-d3e1-4ae0-9384-81a5657f7a8d",
                    "LayerId": "0ec3648d-0808-4caa-beba-9bbcca7243f9"
                }
            ]
        },
        {
            "id": "a8556841-85d4-46da-bb0f-728343458b49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d6902d-3a81-4170-aa3e-819ae65032ec",
            "compositeImage": {
                "id": "791bb7c9-6239-412c-b176-b58e2344007b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8556841-85d4-46da-bb0f-728343458b49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67d9ac32-10b0-4bc6-9c02-4dbea4997a71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8556841-85d4-46da-bb0f-728343458b49",
                    "LayerId": "0d4485a1-947c-4bf3-aa88-9acd79bcea5a"
                },
                {
                    "id": "7f80adbd-626f-4623-9c1d-d78d9b10416a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8556841-85d4-46da-bb0f-728343458b49",
                    "LayerId": "0ec3648d-0808-4caa-beba-9bbcca7243f9"
                }
            ]
        },
        {
            "id": "691ae1a3-17c9-48bf-80e6-43954cd45807",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d6902d-3a81-4170-aa3e-819ae65032ec",
            "compositeImage": {
                "id": "d89bc6f4-7c98-4219-aa90-fddfcda930f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "691ae1a3-17c9-48bf-80e6-43954cd45807",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6b9c97e-aaa3-4c5b-9117-e7d35f0101f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "691ae1a3-17c9-48bf-80e6-43954cd45807",
                    "LayerId": "0d4485a1-947c-4bf3-aa88-9acd79bcea5a"
                },
                {
                    "id": "08644b2b-96de-438d-8511-67609a8963f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "691ae1a3-17c9-48bf-80e6-43954cd45807",
                    "LayerId": "0ec3648d-0808-4caa-beba-9bbcca7243f9"
                }
            ]
        },
        {
            "id": "68d3f98b-b731-4d08-9920-8b79307793fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d6902d-3a81-4170-aa3e-819ae65032ec",
            "compositeImage": {
                "id": "4fda83c5-a918-47e4-b750-df05905ef18e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68d3f98b-b731-4d08-9920-8b79307793fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cf8bb56-0c51-49ed-afd1-df34fae6e426",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68d3f98b-b731-4d08-9920-8b79307793fa",
                    "LayerId": "0ec3648d-0808-4caa-beba-9bbcca7243f9"
                },
                {
                    "id": "797bf8c5-ca26-434d-beab-8ab020c4241c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68d3f98b-b731-4d08-9920-8b79307793fa",
                    "LayerId": "0d4485a1-947c-4bf3-aa88-9acd79bcea5a"
                }
            ]
        },
        {
            "id": "c41979e3-a075-48dd-9642-d43dac1d8796",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d6902d-3a81-4170-aa3e-819ae65032ec",
            "compositeImage": {
                "id": "976e314b-b61c-4615-a1fc-9b0fff424667",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c41979e3-a075-48dd-9642-d43dac1d8796",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fcbf27a-7c20-41fd-9c00-d08c4499d00b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c41979e3-a075-48dd-9642-d43dac1d8796",
                    "LayerId": "0ec3648d-0808-4caa-beba-9bbcca7243f9"
                },
                {
                    "id": "12536e24-07f0-46a1-9797-7068eb084fd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c41979e3-a075-48dd-9642-d43dac1d8796",
                    "LayerId": "0d4485a1-947c-4bf3-aa88-9acd79bcea5a"
                }
            ]
        },
        {
            "id": "b76efd8b-3098-412f-9ba0-c1ee0de26f2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d6902d-3a81-4170-aa3e-819ae65032ec",
            "compositeImage": {
                "id": "69f02c67-5029-42a2-840c-4cc8bb51c12b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b76efd8b-3098-412f-9ba0-c1ee0de26f2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b5afb3f-8a8d-417d-9d30-0cf208d211d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b76efd8b-3098-412f-9ba0-c1ee0de26f2f",
                    "LayerId": "0ec3648d-0808-4caa-beba-9bbcca7243f9"
                },
                {
                    "id": "2b277059-eaf3-4322-976c-145b367a9e6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b76efd8b-3098-412f-9ba0-c1ee0de26f2f",
                    "LayerId": "0d4485a1-947c-4bf3-aa88-9acd79bcea5a"
                }
            ]
        },
        {
            "id": "13115afa-e4f7-4f3e-9cc1-704b2c679170",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d6902d-3a81-4170-aa3e-819ae65032ec",
            "compositeImage": {
                "id": "cfd3740f-58b6-46cc-a92c-35436431556c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13115afa-e4f7-4f3e-9cc1-704b2c679170",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b42dff0a-d751-4f31-b71d-31c2039e343e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13115afa-e4f7-4f3e-9cc1-704b2c679170",
                    "LayerId": "0ec3648d-0808-4caa-beba-9bbcca7243f9"
                },
                {
                    "id": "4e6483ce-2bca-4cf8-8929-9a90c0c8fed8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13115afa-e4f7-4f3e-9cc1-704b2c679170",
                    "LayerId": "0d4485a1-947c-4bf3-aa88-9acd79bcea5a"
                }
            ]
        },
        {
            "id": "07991607-ee69-4eea-b76f-954d893135e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d6902d-3a81-4170-aa3e-819ae65032ec",
            "compositeImage": {
                "id": "0982487f-b973-4e83-8a4d-c55dcca9d5d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07991607-ee69-4eea-b76f-954d893135e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9197a05f-8236-4b9e-96d8-7fcd31276a63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07991607-ee69-4eea-b76f-954d893135e3",
                    "LayerId": "0ec3648d-0808-4caa-beba-9bbcca7243f9"
                },
                {
                    "id": "10e3264c-5a29-489f-a80f-758ed630ad65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07991607-ee69-4eea-b76f-954d893135e3",
                    "LayerId": "0d4485a1-947c-4bf3-aa88-9acd79bcea5a"
                }
            ]
        },
        {
            "id": "9517ac25-f6fd-4648-a229-7b68d2129b41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d6902d-3a81-4170-aa3e-819ae65032ec",
            "compositeImage": {
                "id": "2171371e-268f-4a22-b54a-9acab0025385",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9517ac25-f6fd-4648-a229-7b68d2129b41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e70db21-b82f-4809-bd8b-e002f8f3e2b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9517ac25-f6fd-4648-a229-7b68d2129b41",
                    "LayerId": "0ec3648d-0808-4caa-beba-9bbcca7243f9"
                },
                {
                    "id": "3648bf76-0643-4b0a-ae2f-7af782ebc62d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9517ac25-f6fd-4648-a229-7b68d2129b41",
                    "LayerId": "0d4485a1-947c-4bf3-aa88-9acd79bcea5a"
                }
            ]
        },
        {
            "id": "3571047f-063d-4ade-95ab-571b1160bc77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d6902d-3a81-4170-aa3e-819ae65032ec",
            "compositeImage": {
                "id": "6b37b6ff-52ba-4933-9509-351370a5b656",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3571047f-063d-4ade-95ab-571b1160bc77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c71dc1b8-175c-4195-aa69-6c5300250612",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3571047f-063d-4ade-95ab-571b1160bc77",
                    "LayerId": "0ec3648d-0808-4caa-beba-9bbcca7243f9"
                },
                {
                    "id": "cf03ae50-aba0-471e-8ca8-7cb5228a34f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3571047f-063d-4ade-95ab-571b1160bc77",
                    "LayerId": "0d4485a1-947c-4bf3-aa88-9acd79bcea5a"
                }
            ]
        },
        {
            "id": "74149643-2eab-4a21-97f4-f31b96fe3c86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d6902d-3a81-4170-aa3e-819ae65032ec",
            "compositeImage": {
                "id": "795f8134-764f-4123-91d8-df04c35c0e22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74149643-2eab-4a21-97f4-f31b96fe3c86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f1c0e84-e4ad-45b6-87fe-34fc4400fc30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74149643-2eab-4a21-97f4-f31b96fe3c86",
                    "LayerId": "0ec3648d-0808-4caa-beba-9bbcca7243f9"
                },
                {
                    "id": "5ff014e4-5d01-4338-9b32-9debfc48f3bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74149643-2eab-4a21-97f4-f31b96fe3c86",
                    "LayerId": "0d4485a1-947c-4bf3-aa88-9acd79bcea5a"
                }
            ]
        },
        {
            "id": "dfa963f0-ecda-4687-94c0-e9c557995ca1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d6902d-3a81-4170-aa3e-819ae65032ec",
            "compositeImage": {
                "id": "696176df-ff28-455c-b4ee-a993e99e5a41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfa963f0-ecda-4687-94c0-e9c557995ca1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df6feec7-8df6-4b00-91c0-f2dc69005e68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfa963f0-ecda-4687-94c0-e9c557995ca1",
                    "LayerId": "0ec3648d-0808-4caa-beba-9bbcca7243f9"
                },
                {
                    "id": "14de0403-4085-4153-86ba-da0005696417",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfa963f0-ecda-4687-94c0-e9c557995ca1",
                    "LayerId": "0d4485a1-947c-4bf3-aa88-9acd79bcea5a"
                }
            ]
        },
        {
            "id": "c3bf23f0-5a14-4b2c-991f-852f5a7348c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d6902d-3a81-4170-aa3e-819ae65032ec",
            "compositeImage": {
                "id": "eb85a3d5-e991-4df4-81a6-9f8313a10948",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3bf23f0-5a14-4b2c-991f-852f5a7348c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb730394-de53-49c5-963f-bce0dab6e7e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3bf23f0-5a14-4b2c-991f-852f5a7348c8",
                    "LayerId": "0ec3648d-0808-4caa-beba-9bbcca7243f9"
                },
                {
                    "id": "9fc00667-0619-4b7a-af8f-a220a774ddfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3bf23f0-5a14-4b2c-991f-852f5a7348c8",
                    "LayerId": "0d4485a1-947c-4bf3-aa88-9acd79bcea5a"
                }
            ]
        },
        {
            "id": "fc04e73c-d4de-400d-94d3-32a06249c390",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d6902d-3a81-4170-aa3e-819ae65032ec",
            "compositeImage": {
                "id": "28563080-83fc-4fea-9e07-021b980cca49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc04e73c-d4de-400d-94d3-32a06249c390",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cb739b7-3958-4e54-973a-a1d8f95c280c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc04e73c-d4de-400d-94d3-32a06249c390",
                    "LayerId": "0ec3648d-0808-4caa-beba-9bbcca7243f9"
                },
                {
                    "id": "fbe42c93-afc3-4630-b78f-6799698c19e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc04e73c-d4de-400d-94d3-32a06249c390",
                    "LayerId": "0d4485a1-947c-4bf3-aa88-9acd79bcea5a"
                }
            ]
        },
        {
            "id": "567ac718-7ee9-408c-aad6-e2878e1ec484",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d6902d-3a81-4170-aa3e-819ae65032ec",
            "compositeImage": {
                "id": "af279d01-2265-48b5-922f-919a1a36bdad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "567ac718-7ee9-408c-aad6-e2878e1ec484",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fc94b18-2163-4fc6-9748-e2e8914c3dbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "567ac718-7ee9-408c-aad6-e2878e1ec484",
                    "LayerId": "0ec3648d-0808-4caa-beba-9bbcca7243f9"
                },
                {
                    "id": "4863ab5e-6716-4a7b-b822-6d1b35fa526b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "567ac718-7ee9-408c-aad6-e2878e1ec484",
                    "LayerId": "0d4485a1-947c-4bf3-aa88-9acd79bcea5a"
                }
            ]
        },
        {
            "id": "bf08e3e3-13cc-47dd-9ff1-4a27da431ef0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d6902d-3a81-4170-aa3e-819ae65032ec",
            "compositeImage": {
                "id": "b7c09fe0-5c0e-4a79-b673-5fd55c319467",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf08e3e3-13cc-47dd-9ff1-4a27da431ef0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b3b05a1-b416-4b33-9323-379038f55b4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf08e3e3-13cc-47dd-9ff1-4a27da431ef0",
                    "LayerId": "0ec3648d-0808-4caa-beba-9bbcca7243f9"
                },
                {
                    "id": "d55e8263-9560-49d7-8b27-fe4c1119f5a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf08e3e3-13cc-47dd-9ff1-4a27da431ef0",
                    "LayerId": "0d4485a1-947c-4bf3-aa88-9acd79bcea5a"
                }
            ]
        },
        {
            "id": "518b3345-868c-4751-8102-93b773eaf902",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d6902d-3a81-4170-aa3e-819ae65032ec",
            "compositeImage": {
                "id": "64aab27d-0f4d-4b6f-80ea-fa4b2064dffc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "518b3345-868c-4751-8102-93b773eaf902",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "284f9c23-5f4c-464a-bc0e-cfc0edba1428",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "518b3345-868c-4751-8102-93b773eaf902",
                    "LayerId": "0ec3648d-0808-4caa-beba-9bbcca7243f9"
                },
                {
                    "id": "5e43aef3-5c32-40f0-a760-3a4f2e0437e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "518b3345-868c-4751-8102-93b773eaf902",
                    "LayerId": "0d4485a1-947c-4bf3-aa88-9acd79bcea5a"
                }
            ]
        },
        {
            "id": "d93f50f0-e314-4068-a66a-7fd68fa71daf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d6902d-3a81-4170-aa3e-819ae65032ec",
            "compositeImage": {
                "id": "8927f4ea-2765-4d17-bdbd-06296499c95c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d93f50f0-e314-4068-a66a-7fd68fa71daf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "090af411-59c5-4bcb-a878-b11c8cfedc51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d93f50f0-e314-4068-a66a-7fd68fa71daf",
                    "LayerId": "0ec3648d-0808-4caa-beba-9bbcca7243f9"
                },
                {
                    "id": "31f6ddb9-66da-42f9-a38b-117f031c9178",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d93f50f0-e314-4068-a66a-7fd68fa71daf",
                    "LayerId": "0d4485a1-947c-4bf3-aa88-9acd79bcea5a"
                }
            ]
        },
        {
            "id": "3b2ac6ec-0c1f-4801-be0d-90934e934fbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d6902d-3a81-4170-aa3e-819ae65032ec",
            "compositeImage": {
                "id": "eac97fa4-910f-4db1-b9d1-1eb8e2ccf5ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b2ac6ec-0c1f-4801-be0d-90934e934fbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbddb22b-f9a9-4a44-bb8d-9f2c4f9d41b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b2ac6ec-0c1f-4801-be0d-90934e934fbe",
                    "LayerId": "0ec3648d-0808-4caa-beba-9bbcca7243f9"
                },
                {
                    "id": "bcf987da-c74e-4f2b-ba81-369b8e15c23c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b2ac6ec-0c1f-4801-be0d-90934e934fbe",
                    "LayerId": "0d4485a1-947c-4bf3-aa88-9acd79bcea5a"
                }
            ]
        },
        {
            "id": "78df3c9c-7f1b-4df7-b632-39dfe37975e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d6902d-3a81-4170-aa3e-819ae65032ec",
            "compositeImage": {
                "id": "c51f5054-1593-4708-9861-5f5429140013",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78df3c9c-7f1b-4df7-b632-39dfe37975e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d2c4c7d-1c10-4fb9-a44e-78e0defd57c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78df3c9c-7f1b-4df7-b632-39dfe37975e7",
                    "LayerId": "0ec3648d-0808-4caa-beba-9bbcca7243f9"
                },
                {
                    "id": "b4219f3a-8114-4516-9fb0-7b7d8cf40dfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78df3c9c-7f1b-4df7-b632-39dfe37975e7",
                    "LayerId": "0d4485a1-947c-4bf3-aa88-9acd79bcea5a"
                }
            ]
        },
        {
            "id": "1a605a63-50ab-4237-aeaf-d5be64436b3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d6902d-3a81-4170-aa3e-819ae65032ec",
            "compositeImage": {
                "id": "d8c49ec8-a040-4c15-81fa-3d5e209017d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a605a63-50ab-4237-aeaf-d5be64436b3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c3322c7-9796-45fa-9b11-498301109595",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a605a63-50ab-4237-aeaf-d5be64436b3e",
                    "LayerId": "0ec3648d-0808-4caa-beba-9bbcca7243f9"
                },
                {
                    "id": "2a10af01-6b0b-4b23-a5ae-6646ca38f724",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a605a63-50ab-4237-aeaf-d5be64436b3e",
                    "LayerId": "0d4485a1-947c-4bf3-aa88-9acd79bcea5a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0ec3648d-0808-4caa-beba-9bbcca7243f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "74d6902d-3a81-4170-aa3e-819ae65032ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "0d4485a1-947c-4bf3-aa88-9acd79bcea5a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "74d6902d-3a81-4170-aa3e-819ae65032ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}