{
    "id": "ed722079-6ead-4bf5-a499-cd11cde12908",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sForeground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 490,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c0d64fc7-5f75-4e29-85e5-db18c94d2c02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed722079-6ead-4bf5-a499-cd11cde12908",
            "compositeImage": {
                "id": "832cdb05-3aff-43dd-ad3f-35a393f8d9e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0d64fc7-5f75-4e29-85e5-db18c94d2c02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce94c64a-ef88-446d-bff8-254485531e33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0d64fc7-5f75-4e29-85e5-db18c94d2c02",
                    "LayerId": "01f11ab9-4238-4f87-8d64-ab802f71052a"
                },
                {
                    "id": "8434ed8d-8a1d-4499-bfb3-8e993ccbd4f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0d64fc7-5f75-4e29-85e5-db18c94d2c02",
                    "LayerId": "8dec033e-b053-44bc-a9fc-32c643fab3f4"
                },
                {
                    "id": "14ae794d-220c-4295-a063-125971892f08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0d64fc7-5f75-4e29-85e5-db18c94d2c02",
                    "LayerId": "bcc97abd-4a70-44b5-807c-9da165c48c8b"
                },
                {
                    "id": "aa1dd0c8-fe5a-48af-a0d5-acec47307e72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0d64fc7-5f75-4e29-85e5-db18c94d2c02",
                    "LayerId": "439c4bdc-1a9c-4e6b-a414-01d4a41d73a2"
                },
                {
                    "id": "28581fbc-7ca8-41a4-9a80-3b6bd67abfa9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0d64fc7-5f75-4e29-85e5-db18c94d2c02",
                    "LayerId": "733b765c-366b-4bb0-8e46-f266377b05a4"
                },
                {
                    "id": "16baa8e2-91b3-41fc-8007-2421347b6b69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0d64fc7-5f75-4e29-85e5-db18c94d2c02",
                    "LayerId": "0aba58c8-b973-487d-97c9-27a2ec109188"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 600,
    "layers": [
        {
            "id": "01f11ab9-4238-4f87-8d64-ab802f71052a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed722079-6ead-4bf5-a499-cd11cde12908",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "8dec033e-b053-44bc-a9fc-32c643fab3f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed722079-6ead-4bf5-a499-cd11cde12908",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "bcc97abd-4a70-44b5-807c-9da165c48c8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed722079-6ead-4bf5-a499-cd11cde12908",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 80,
            "visible": false
        },
        {
            "id": "439c4bdc-1a9c-4e6b-a414-01d4a41d73a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed722079-6ead-4bf5-a499-cd11cde12908",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 4 (2)",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "733b765c-366b-4bb0-8e46-f266377b05a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed722079-6ead-4bf5-a499-cd11cde12908",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 4",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "0aba58c8-b973-487d-97c9-27a2ec109188",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed722079-6ead-4bf5-a499-cd11cde12908",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}