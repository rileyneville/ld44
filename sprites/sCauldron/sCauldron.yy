{
    "id": "d871312b-8f70-45fb-9f4e-2197c2da988c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCauldron",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 9,
    "bbox_right": 22,
    "bbox_top": 14,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dc299253-8c38-43d9-a529-d42d2eeae38c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d871312b-8f70-45fb-9f4e-2197c2da988c",
            "compositeImage": {
                "id": "20bb36fe-2d13-4fb4-8e21-eb8368075154",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc299253-8c38-43d9-a529-d42d2eeae38c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4eff961-a38c-4975-8421-560dc09ed879",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc299253-8c38-43d9-a529-d42d2eeae38c",
                    "LayerId": "29bc8152-5061-4be9-8ad9-8c51739c7a2b"
                },
                {
                    "id": "76e0626f-8ad2-4c8c-ab96-0967e56f3cc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc299253-8c38-43d9-a529-d42d2eeae38c",
                    "LayerId": "189c3a35-b48a-4ffa-8811-e0253e554bf8"
                },
                {
                    "id": "87218b6b-55de-4161-9985-d4c78461ee90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc299253-8c38-43d9-a529-d42d2eeae38c",
                    "LayerId": "c12aa8d7-6d44-4df6-8d01-12061aff0fa1"
                }
            ]
        },
        {
            "id": "d102f9e3-c805-4ce3-aa0b-a5a269551f8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d871312b-8f70-45fb-9f4e-2197c2da988c",
            "compositeImage": {
                "id": "2fe13685-9897-4190-8956-1ac2824d3277",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d102f9e3-c805-4ce3-aa0b-a5a269551f8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b8acc60-a288-426b-841e-378109856d9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d102f9e3-c805-4ce3-aa0b-a5a269551f8e",
                    "LayerId": "189c3a35-b48a-4ffa-8811-e0253e554bf8"
                },
                {
                    "id": "dcc8e7c9-fce5-45db-a92b-133dcfafb393",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d102f9e3-c805-4ce3-aa0b-a5a269551f8e",
                    "LayerId": "29bc8152-5061-4be9-8ad9-8c51739c7a2b"
                },
                {
                    "id": "70313576-d8cd-4be3-b3fc-77bb097e9c3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d102f9e3-c805-4ce3-aa0b-a5a269551f8e",
                    "LayerId": "c12aa8d7-6d44-4df6-8d01-12061aff0fa1"
                }
            ]
        },
        {
            "id": "e430c2a6-740c-40e9-aed6-ccb21c940dad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d871312b-8f70-45fb-9f4e-2197c2da988c",
            "compositeImage": {
                "id": "050ea56a-aad3-469a-8709-6b35903b7365",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e430c2a6-740c-40e9-aed6-ccb21c940dad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7faca09-9eda-4a10-86f1-339d2999d927",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e430c2a6-740c-40e9-aed6-ccb21c940dad",
                    "LayerId": "189c3a35-b48a-4ffa-8811-e0253e554bf8"
                },
                {
                    "id": "789435ab-e3eb-4c59-9b7a-2d850f785ca6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e430c2a6-740c-40e9-aed6-ccb21c940dad",
                    "LayerId": "29bc8152-5061-4be9-8ad9-8c51739c7a2b"
                },
                {
                    "id": "18ced1c0-b021-4a14-9d92-6715e267df62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e430c2a6-740c-40e9-aed6-ccb21c940dad",
                    "LayerId": "c12aa8d7-6d44-4df6-8d01-12061aff0fa1"
                }
            ]
        },
        {
            "id": "12bd1078-a50f-4fc5-84c6-070236f522eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d871312b-8f70-45fb-9f4e-2197c2da988c",
            "compositeImage": {
                "id": "03931133-cfdd-4760-9a0b-57c2f8fc7bcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12bd1078-a50f-4fc5-84c6-070236f522eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a49326b-8645-41b8-9470-96ba8d4c1ef9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12bd1078-a50f-4fc5-84c6-070236f522eb",
                    "LayerId": "189c3a35-b48a-4ffa-8811-e0253e554bf8"
                },
                {
                    "id": "376014d7-8a16-4e60-8ce4-c6d00b11074d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12bd1078-a50f-4fc5-84c6-070236f522eb",
                    "LayerId": "29bc8152-5061-4be9-8ad9-8c51739c7a2b"
                },
                {
                    "id": "02795002-0393-4e6c-88ae-0e62973bd297",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12bd1078-a50f-4fc5-84c6-070236f522eb",
                    "LayerId": "c12aa8d7-6d44-4df6-8d01-12061aff0fa1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c12aa8d7-6d44-4df6-8d01-12061aff0fa1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d871312b-8f70-45fb-9f4e-2197c2da988c",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1 (2)",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "189c3a35-b48a-4ffa-8811-e0253e554bf8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d871312b-8f70-45fb-9f4e-2197c2da988c",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "29bc8152-5061-4be9-8ad9-8c51739c7a2b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d871312b-8f70-45fb-9f4e-2197c2da988c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}