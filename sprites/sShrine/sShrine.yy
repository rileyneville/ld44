{
    "id": "308b3727-8279-4c3e-98ba-9c0d0237f648",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShrine",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 15,
    "bbox_right": 55,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2ba75d2-a79c-4f54-a8f4-3fd26a5cd68d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "308b3727-8279-4c3e-98ba-9c0d0237f648",
            "compositeImage": {
                "id": "0060d91c-6d49-4cb3-9e62-396b3fef1a47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2ba75d2-a79c-4f54-a8f4-3fd26a5cd68d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59a0cca7-1dc9-4452-823e-45a13c407385",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2ba75d2-a79c-4f54-a8f4-3fd26a5cd68d",
                    "LayerId": "345826e4-89ec-459a-bf86-f32e19f61bb3"
                },
                {
                    "id": "b3469899-56e5-4573-a218-7820c8baa7a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2ba75d2-a79c-4f54-a8f4-3fd26a5cd68d",
                    "LayerId": "68ecd8cb-fb88-49a9-bbda-333c83ecd95d"
                }
            ]
        },
        {
            "id": "37326cb6-6df0-4521-b0af-03180fe6c7b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "308b3727-8279-4c3e-98ba-9c0d0237f648",
            "compositeImage": {
                "id": "0a3667e0-e629-474e-8666-3aaaef909e5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37326cb6-6df0-4521-b0af-03180fe6c7b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "683fd62c-4d46-408d-abbc-59cc94440def",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37326cb6-6df0-4521-b0af-03180fe6c7b6",
                    "LayerId": "68ecd8cb-fb88-49a9-bbda-333c83ecd95d"
                },
                {
                    "id": "9c0be5bf-0f6c-476e-b301-668b327cd228",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37326cb6-6df0-4521-b0af-03180fe6c7b6",
                    "LayerId": "345826e4-89ec-459a-bf86-f32e19f61bb3"
                }
            ]
        },
        {
            "id": "481ff2fc-8105-431c-9383-ed0187186124",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "308b3727-8279-4c3e-98ba-9c0d0237f648",
            "compositeImage": {
                "id": "7735d26c-c2e3-4546-aa9b-cace50f9c776",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "481ff2fc-8105-431c-9383-ed0187186124",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a073abb-2e0f-4992-a697-f3e6f85ae6ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "481ff2fc-8105-431c-9383-ed0187186124",
                    "LayerId": "68ecd8cb-fb88-49a9-bbda-333c83ecd95d"
                },
                {
                    "id": "36d58d72-765f-4229-b3aa-75740a9dddf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "481ff2fc-8105-431c-9383-ed0187186124",
                    "LayerId": "345826e4-89ec-459a-bf86-f32e19f61bb3"
                }
            ]
        },
        {
            "id": "f8a58d39-e798-4c20-bbf2-cbe3e7cd5884",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "308b3727-8279-4c3e-98ba-9c0d0237f648",
            "compositeImage": {
                "id": "356ab818-0bad-4b96-a576-89d9a9313e80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8a58d39-e798-4c20-bbf2-cbe3e7cd5884",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23796115-d6a4-4a3c-ad5b-cf9d678fe01d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8a58d39-e798-4c20-bbf2-cbe3e7cd5884",
                    "LayerId": "68ecd8cb-fb88-49a9-bbda-333c83ecd95d"
                },
                {
                    "id": "72710bad-9994-473a-9242-ff4dccaa948d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8a58d39-e798-4c20-bbf2-cbe3e7cd5884",
                    "LayerId": "345826e4-89ec-459a-bf86-f32e19f61bb3"
                }
            ]
        },
        {
            "id": "92d6246a-eac3-48cc-afa9-de67b9b1301d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "308b3727-8279-4c3e-98ba-9c0d0237f648",
            "compositeImage": {
                "id": "bc333078-1873-490f-bd73-b1691f3c435a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92d6246a-eac3-48cc-afa9-de67b9b1301d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c03b327-6e44-4a9e-8007-27d1eb777d8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92d6246a-eac3-48cc-afa9-de67b9b1301d",
                    "LayerId": "68ecd8cb-fb88-49a9-bbda-333c83ecd95d"
                },
                {
                    "id": "561565bb-d2fb-4f67-8d31-eed77524dd2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92d6246a-eac3-48cc-afa9-de67b9b1301d",
                    "LayerId": "345826e4-89ec-459a-bf86-f32e19f61bb3"
                }
            ]
        },
        {
            "id": "5b100104-ce9d-464b-9020-6333e6167602",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "308b3727-8279-4c3e-98ba-9c0d0237f648",
            "compositeImage": {
                "id": "a5d2dca0-37e5-4648-b979-47c1c8b3d554",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b100104-ce9d-464b-9020-6333e6167602",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcd54130-5767-41c7-89b6-ca812bc5f386",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b100104-ce9d-464b-9020-6333e6167602",
                    "LayerId": "68ecd8cb-fb88-49a9-bbda-333c83ecd95d"
                },
                {
                    "id": "86c8b56d-1e8b-4ed7-8bbc-e914fff6dc7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b100104-ce9d-464b-9020-6333e6167602",
                    "LayerId": "345826e4-89ec-459a-bf86-f32e19f61bb3"
                }
            ]
        },
        {
            "id": "a4d0e93e-5a11-40b8-b1e4-47f1af13e55a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "308b3727-8279-4c3e-98ba-9c0d0237f648",
            "compositeImage": {
                "id": "4e454a9f-349f-4ddc-a53f-847117072efc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4d0e93e-5a11-40b8-b1e4-47f1af13e55a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f92e2ebc-8daf-498c-a617-5255f167efc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4d0e93e-5a11-40b8-b1e4-47f1af13e55a",
                    "LayerId": "68ecd8cb-fb88-49a9-bbda-333c83ecd95d"
                },
                {
                    "id": "f6eb4539-01eb-4e3c-b1b5-53483e935ccc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4d0e93e-5a11-40b8-b1e4-47f1af13e55a",
                    "LayerId": "345826e4-89ec-459a-bf86-f32e19f61bb3"
                }
            ]
        },
        {
            "id": "13064b16-5be8-4ac2-85dd-536316c67266",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "308b3727-8279-4c3e-98ba-9c0d0237f648",
            "compositeImage": {
                "id": "2358fb1c-e0d3-4338-9ca5-147b578ca861",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13064b16-5be8-4ac2-85dd-536316c67266",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "446abcdc-ae27-48d4-a4d8-57efda813cea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13064b16-5be8-4ac2-85dd-536316c67266",
                    "LayerId": "68ecd8cb-fb88-49a9-bbda-333c83ecd95d"
                },
                {
                    "id": "d2b3eca9-da95-4ef3-b327-ebbb0963ccb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13064b16-5be8-4ac2-85dd-536316c67266",
                    "LayerId": "345826e4-89ec-459a-bf86-f32e19f61bb3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "345826e4-89ec-459a-bf86-f32e19f61bb3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "308b3727-8279-4c3e-98ba-9c0d0237f648",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "68ecd8cb-fb88-49a9-bbda-333c83ecd95d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "308b3727-8279-4c3e-98ba-9c0d0237f648",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 70,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}