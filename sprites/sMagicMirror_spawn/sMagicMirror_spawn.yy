{
    "id": "8a4ea579-8e25-408e-9d29-54c283e1ec9f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMagicMirror_spawn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 7,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3588328b-972f-4110-b993-61bfa09eb68d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a4ea579-8e25-408e-9d29-54c283e1ec9f",
            "compositeImage": {
                "id": "24bd90e6-1602-4c52-95fd-1b417d387add",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3588328b-972f-4110-b993-61bfa09eb68d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd8ead84-5ac2-4e5b-85f6-86c552dfb8d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3588328b-972f-4110-b993-61bfa09eb68d",
                    "LayerId": "7e59e6dc-5718-479c-9a45-a9e93a97544e"
                },
                {
                    "id": "6e5d71c6-2186-4c29-a3e2-32eedb9c510f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3588328b-972f-4110-b993-61bfa09eb68d",
                    "LayerId": "fb89f276-6b84-4d2e-ac93-45d770f9506b"
                }
            ]
        },
        {
            "id": "8025a180-cd66-491e-8946-deedfb674eff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a4ea579-8e25-408e-9d29-54c283e1ec9f",
            "compositeImage": {
                "id": "003891ae-0f2f-4a5e-ac9a-2cfb68174c8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8025a180-cd66-491e-8946-deedfb674eff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52b6acc6-73a5-4b19-bc10-1e7ac6f6eb6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8025a180-cd66-491e-8946-deedfb674eff",
                    "LayerId": "7e59e6dc-5718-479c-9a45-a9e93a97544e"
                },
                {
                    "id": "42148b54-97da-455f-a047-a08340034167",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8025a180-cd66-491e-8946-deedfb674eff",
                    "LayerId": "fb89f276-6b84-4d2e-ac93-45d770f9506b"
                }
            ]
        },
        {
            "id": "5b85fa79-a335-4142-9257-a58b3f23e50e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a4ea579-8e25-408e-9d29-54c283e1ec9f",
            "compositeImage": {
                "id": "a5ae6718-4f0e-4d67-a64a-d1b2bb413e64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b85fa79-a335-4142-9257-a58b3f23e50e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51705779-20c0-41bb-9354-e3bd68278503",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b85fa79-a335-4142-9257-a58b3f23e50e",
                    "LayerId": "7e59e6dc-5718-479c-9a45-a9e93a97544e"
                },
                {
                    "id": "f4f2a4b0-137c-4368-a6ac-3bb31c330392",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b85fa79-a335-4142-9257-a58b3f23e50e",
                    "LayerId": "fb89f276-6b84-4d2e-ac93-45d770f9506b"
                }
            ]
        },
        {
            "id": "c16fe45f-39c7-4e67-85f1-00ee90029708",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a4ea579-8e25-408e-9d29-54c283e1ec9f",
            "compositeImage": {
                "id": "c68f4c2e-1c81-49a5-b8b1-012923cebc57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c16fe45f-39c7-4e67-85f1-00ee90029708",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36da1125-f74e-4545-bb40-87aab2771419",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c16fe45f-39c7-4e67-85f1-00ee90029708",
                    "LayerId": "7e59e6dc-5718-479c-9a45-a9e93a97544e"
                },
                {
                    "id": "7b27bc43-21d9-41b7-a880-ae0c5c7f9c7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c16fe45f-39c7-4e67-85f1-00ee90029708",
                    "LayerId": "fb89f276-6b84-4d2e-ac93-45d770f9506b"
                }
            ]
        },
        {
            "id": "98d8f309-8d09-4e16-a367-52f11d3d7a66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a4ea579-8e25-408e-9d29-54c283e1ec9f",
            "compositeImage": {
                "id": "daa22fe9-c5e9-4c81-b147-8a84b58adb0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98d8f309-8d09-4e16-a367-52f11d3d7a66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36a873f9-a212-467a-a1de-474824b0a813",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98d8f309-8d09-4e16-a367-52f11d3d7a66",
                    "LayerId": "7e59e6dc-5718-479c-9a45-a9e93a97544e"
                },
                {
                    "id": "24349e08-a051-443c-b0df-fa1a1a2d0c5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98d8f309-8d09-4e16-a367-52f11d3d7a66",
                    "LayerId": "fb89f276-6b84-4d2e-ac93-45d770f9506b"
                }
            ]
        },
        {
            "id": "136403ad-fa5c-47ed-93d3-3885de29a960",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a4ea579-8e25-408e-9d29-54c283e1ec9f",
            "compositeImage": {
                "id": "8d735c2e-ab99-4f70-a331-5feff4652849",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "136403ad-fa5c-47ed-93d3-3885de29a960",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "189bb382-c00c-4ef1-80f9-786572cf53d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "136403ad-fa5c-47ed-93d3-3885de29a960",
                    "LayerId": "7e59e6dc-5718-479c-9a45-a9e93a97544e"
                },
                {
                    "id": "212563e2-77dc-462e-a8ca-9e1ab440611b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "136403ad-fa5c-47ed-93d3-3885de29a960",
                    "LayerId": "fb89f276-6b84-4d2e-ac93-45d770f9506b"
                }
            ]
        },
        {
            "id": "fa8af8f8-f970-413e-8d69-7d49f734c1d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a4ea579-8e25-408e-9d29-54c283e1ec9f",
            "compositeImage": {
                "id": "93533af9-194d-4207-a4bd-c8c03535fe3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa8af8f8-f970-413e-8d69-7d49f734c1d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed3ec4b6-57b3-4738-819c-2ef2f1939613",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa8af8f8-f970-413e-8d69-7d49f734c1d2",
                    "LayerId": "7e59e6dc-5718-479c-9a45-a9e93a97544e"
                },
                {
                    "id": "8025199d-3bdb-487b-8145-c9532666bd83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa8af8f8-f970-413e-8d69-7d49f734c1d2",
                    "LayerId": "fb89f276-6b84-4d2e-ac93-45d770f9506b"
                }
            ]
        },
        {
            "id": "0de26006-1773-4dad-974b-2851a773c88c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a4ea579-8e25-408e-9d29-54c283e1ec9f",
            "compositeImage": {
                "id": "3bbe25cd-a04e-41cc-aab0-ff0dcd9f8d28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0de26006-1773-4dad-974b-2851a773c88c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21ac44a5-026b-46cd-8c61-18082bdf14c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0de26006-1773-4dad-974b-2851a773c88c",
                    "LayerId": "7e59e6dc-5718-479c-9a45-a9e93a97544e"
                },
                {
                    "id": "e709baa9-3cde-49a6-9359-6b31e5c2a1dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0de26006-1773-4dad-974b-2851a773c88c",
                    "LayerId": "fb89f276-6b84-4d2e-ac93-45d770f9506b"
                }
            ]
        },
        {
            "id": "ba88a311-7280-4de8-8562-06a9e2cdc92f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a4ea579-8e25-408e-9d29-54c283e1ec9f",
            "compositeImage": {
                "id": "ed177d19-4f3b-43bb-89cb-ae2991363386",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba88a311-7280-4de8-8562-06a9e2cdc92f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cba5db7-c6e2-453b-9e3c-0cddaf00a1c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba88a311-7280-4de8-8562-06a9e2cdc92f",
                    "LayerId": "7e59e6dc-5718-479c-9a45-a9e93a97544e"
                },
                {
                    "id": "61452dec-7a52-4f8d-ba10-106981812faf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba88a311-7280-4de8-8562-06a9e2cdc92f",
                    "LayerId": "fb89f276-6b84-4d2e-ac93-45d770f9506b"
                }
            ]
        },
        {
            "id": "b3a3f8fe-16a4-482b-9682-59a9b8e6955a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a4ea579-8e25-408e-9d29-54c283e1ec9f",
            "compositeImage": {
                "id": "a13c1283-eaf8-4d2e-890e-951abeafb7ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3a3f8fe-16a4-482b-9682-59a9b8e6955a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2953aaab-d297-419c-acb1-26d6fb8fb8a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3a3f8fe-16a4-482b-9682-59a9b8e6955a",
                    "LayerId": "7e59e6dc-5718-479c-9a45-a9e93a97544e"
                },
                {
                    "id": "3460b55a-2ca6-42f3-9ea7-be0d895099d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3a3f8fe-16a4-482b-9682-59a9b8e6955a",
                    "LayerId": "fb89f276-6b84-4d2e-ac93-45d770f9506b"
                }
            ]
        },
        {
            "id": "6d714689-2289-49a1-b950-5e1c312ccd80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a4ea579-8e25-408e-9d29-54c283e1ec9f",
            "compositeImage": {
                "id": "5103aa5d-a5f0-463c-90cb-65519f11b217",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d714689-2289-49a1-b950-5e1c312ccd80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91309264-5c49-4df1-8a14-570bab2868cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d714689-2289-49a1-b950-5e1c312ccd80",
                    "LayerId": "7e59e6dc-5718-479c-9a45-a9e93a97544e"
                },
                {
                    "id": "fe62e7b6-ad1d-4450-841a-3b8d1920ea73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d714689-2289-49a1-b950-5e1c312ccd80",
                    "LayerId": "fb89f276-6b84-4d2e-ac93-45d770f9506b"
                }
            ]
        },
        {
            "id": "523f929d-afc2-4c33-b4ff-6f78a095d08a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a4ea579-8e25-408e-9d29-54c283e1ec9f",
            "compositeImage": {
                "id": "9ef774a6-e7cc-4ebc-aca1-b2b6fdb291b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "523f929d-afc2-4c33-b4ff-6f78a095d08a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c5c09fb-93c8-4d9d-81d5-eb382a015e7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "523f929d-afc2-4c33-b4ff-6f78a095d08a",
                    "LayerId": "7e59e6dc-5718-479c-9a45-a9e93a97544e"
                },
                {
                    "id": "92d65e44-1ff4-4ae1-9548-94042115ff02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "523f929d-afc2-4c33-b4ff-6f78a095d08a",
                    "LayerId": "fb89f276-6b84-4d2e-ac93-45d770f9506b"
                }
            ]
        },
        {
            "id": "4514c389-935d-4723-8d44-650896bd504e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a4ea579-8e25-408e-9d29-54c283e1ec9f",
            "compositeImage": {
                "id": "6eafb259-e10f-4310-93af-7c5dc9ab21a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4514c389-935d-4723-8d44-650896bd504e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bbbf011-f24f-447b-8d91-733f82b0a65d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4514c389-935d-4723-8d44-650896bd504e",
                    "LayerId": "7e59e6dc-5718-479c-9a45-a9e93a97544e"
                },
                {
                    "id": "64aa899f-c5b8-4b78-bb76-550f07336952",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4514c389-935d-4723-8d44-650896bd504e",
                    "LayerId": "fb89f276-6b84-4d2e-ac93-45d770f9506b"
                }
            ]
        },
        {
            "id": "df5adc41-fa3c-420c-aa4d-79b0e80c579d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a4ea579-8e25-408e-9d29-54c283e1ec9f",
            "compositeImage": {
                "id": "ba135661-b7d6-4a2d-a438-c804d4c40325",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df5adc41-fa3c-420c-aa4d-79b0e80c579d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2edb35f-6aad-4c45-861d-1a3aea551b2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df5adc41-fa3c-420c-aa4d-79b0e80c579d",
                    "LayerId": "7e59e6dc-5718-479c-9a45-a9e93a97544e"
                },
                {
                    "id": "2a9e91db-7a4c-4f76-a828-f994af0cd21c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df5adc41-fa3c-420c-aa4d-79b0e80c579d",
                    "LayerId": "fb89f276-6b84-4d2e-ac93-45d770f9506b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7e59e6dc-5718-479c-9a45-a9e93a97544e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a4ea579-8e25-408e-9d29-54c283e1ec9f",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "fb89f276-6b84-4d2e-ac93-45d770f9506b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a4ea579-8e25-408e-9d29-54c283e1ec9f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}