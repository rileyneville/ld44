{
    "id": "d3c5eb4d-2920-4e68-875a-5fff8bc57f81",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 18,
    "bbox_right": 54,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1893ed57-3af9-49fb-bfce-7cc8411a9889",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3c5eb4d-2920-4e68-875a-5fff8bc57f81",
            "compositeImage": {
                "id": "9a07bba8-198e-4cfc-ab5d-d19375be631e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1893ed57-3af9-49fb-bfce-7cc8411a9889",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5628ba7d-475d-4f49-b39e-e399fbe8dc5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1893ed57-3af9-49fb-bfce-7cc8411a9889",
                    "LayerId": "452b779a-6b7a-4fa2-a2e3-bb17d2491597"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "452b779a-6b7a-4fa2-a2e3-bb17d2491597",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d3c5eb4d-2920-4e68-875a-5fff8bc57f81",
            "blendMode": 0,
            "isLocked": false,
            "name": "default (2)",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 20,
    "yorig": 14
}