{
    "id": "909f8d1d-6bf2-423a-98af-64bb8e37a4d8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBackground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 309,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb1efe01-6315-4ea0-a823-8057d8891f0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "909f8d1d-6bf2-423a-98af-64bb8e37a4d8",
            "compositeImage": {
                "id": "600194c9-ee8f-416e-a8db-6aa013ed66e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb1efe01-6315-4ea0-a823-8057d8891f0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a346bdef-bc7a-4ec3-956e-8538d4b6c66d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb1efe01-6315-4ea0-a823-8057d8891f0e",
                    "LayerId": "ae0dc56d-dc04-4ee3-b02b-67c904ab494b"
                },
                {
                    "id": "7235cfff-c2a2-49f8-a3ff-5ba27a84546c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb1efe01-6315-4ea0-a823-8057d8891f0e",
                    "LayerId": "12bcd665-49eb-48c6-a028-562c1c1c1c2b"
                },
                {
                    "id": "6a3c808f-a45a-493c-aaf9-f3e22d0213d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb1efe01-6315-4ea0-a823-8057d8891f0e",
                    "LayerId": "d190a5ac-7eea-47ad-85d6-7194037948d0"
                },
                {
                    "id": "159c57e1-dbda-45b2-a80c-462d7222b3ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb1efe01-6315-4ea0-a823-8057d8891f0e",
                    "LayerId": "95af00ec-fed0-4fb3-b662-829f7c6236b1"
                },
                {
                    "id": "b67c67d4-ce3c-495a-8204-1cf45114ae4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb1efe01-6315-4ea0-a823-8057d8891f0e",
                    "LayerId": "45624519-7099-4d76-801c-f6031057596f"
                },
                {
                    "id": "9a83792c-72f5-4a4f-8489-538341b1015e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb1efe01-6315-4ea0-a823-8057d8891f0e",
                    "LayerId": "3876ccfd-f3f3-441b-9f1c-ffa9d6852045"
                },
                {
                    "id": "5bc454d8-f496-4a9c-9803-578fe0dd4e88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb1efe01-6315-4ea0-a823-8057d8891f0e",
                    "LayerId": "c782563b-1b37-44e3-9178-f0b8137cf815"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 600,
    "layers": [
        {
            "id": "ae0dc56d-dc04-4ee3-b02b-67c904ab494b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "909f8d1d-6bf2-423a-98af-64bb8e37a4d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "d190a5ac-7eea-47ad-85d6-7194037948d0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "909f8d1d-6bf2-423a-98af-64bb8e37a4d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "12bcd665-49eb-48c6-a028-562c1c1c1c2b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "909f8d1d-6bf2-423a-98af-64bb8e37a4d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "c782563b-1b37-44e3-9178-f0b8137cf815",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "909f8d1d-6bf2-423a-98af-64bb8e37a4d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 5",
            "opacity": 50,
            "visible": true
        },
        {
            "id": "3876ccfd-f3f3-441b-9f1c-ffa9d6852045",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "909f8d1d-6bf2-423a-98af-64bb8e37a4d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 4 (2)",
            "opacity": 75,
            "visible": false
        },
        {
            "id": "45624519-7099-4d76-801c-f6031057596f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "909f8d1d-6bf2-423a-98af-64bb8e37a4d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 4",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "95af00ec-fed0-4fb3-b662-829f7c6236b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "909f8d1d-6bf2-423a-98af-64bb8e37a4d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}